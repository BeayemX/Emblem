# Emblem
### History is written by the vectors
This program is a simple vector graphics editor using blender controls (for now) running completely inside your browser. 
Everything is running on the client side.
Drawings are saved as SVG files and can be exported as PNGs.

You can try it out here:

http://emblem.pixelpackers.com/ (stable release)

http://beayemx.gitlab.io/Emblem/ (development branch)


## Features
 - Grid snapping for different grid types:
    - Rectangle grid
    - Triangle grid
    - Polar grid
 - Line cutting
 - Blender inspired features
    - Blender-like keyboard shortcuts
    - 2D-Cursor as pivot for operations
 - Layers (hide and don't-render properties)
 - Quality settings to adjust performance
 - Load saved SVG-files by drag and drop 
 - Prefabs / Instances

## Use cases
 - Fast prototyping

## Coming soon
 - Shapes(Rectangles, Circles, Images)
 - Annotations
 - Filled polygons
 - Undo / Redo functionality
 - More grid types
 - Non-Blender usability (toolbar like in any other drawing software)

## More information
For more information check out the wiki!
https://gitlab.com/BeayemX/Emblem/wikis/home
