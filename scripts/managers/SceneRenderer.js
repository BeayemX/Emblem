﻿class SceneRenderer extends Renderer {
    constructor(view) {
        super(view);
        
        this.outlineSize = 3;
        this.handleSizeFactor = 5;

        this.showCursor = true;

        this._currentQualitySetting = "quality";
    }

    get currentQualitySetting() {
        if (!this._currentQualitySetting)
            return Preferences.qualitySetting;
        else
            return this._currentQualitySetting
    }
  
    setQualitySetting(setting) {
        this._currentQualitySetting = setting;
        GUI.qualitySelection.value = this.currentQualitySetting;
    }

    _redraw() {
        if (this.view.isRenderPreviewing) {
            this.drawRenderBG();
        } else {
            this.drawArtbounds();
            this._drawGrid();

            if (this.view.logic.currentState instanceof BoxSelectionState) {
                // TODO if next line is removed, axis gets drawn in orange? somewhere this.view.context.stroke missing or sth like that? but i cant find it...
                this.drawCrosshair(true);
                this.drawBorderSelection();
            }
        }

        if (this.currentQualitySetting === "quality")
            this.redrawQuality();
        else if (this.currentQualitySetting === "speed")
            this.redrawSpeed();
        else if (this.currentQualitySetting === "mono")
            this.redrawMono();
        else
            console.log("QualitySetting '" + this.currentQualitySetting + "' does not exist!");


        if (this.view.isRenderPreviewing)
            this.cutRenderPreview();
        else {
            this.drawPivot();

            if (Application.logic.currentState instanceof PrefabBrushState)
                this.drawPrefabPreview();
        }

        if (this.view.logic.currentState instanceof MeasureState) {
            if (!this.view.isRenderPreviewing || Preferences.renderGuideLayers)
                if (this.view.logic.currentState.startPosRMB)
                    this.drawMeasure(this.view.logic.currentState.startPosRMB, this.view.logic.currentState.currentPosRMB);
        }
    }

    cutRenderPreview() {
        let o = Preferences.artBounds.origin;
        let s = Preferences.artBounds.size;

        o = this.view.camera.canvasSpaceToScreenSpace(o);
        s = s.multiply(this.view.camera.zoom);

        let os = o.addVector(s);

        this.view.context.clearRect(0, 0, o.x, this.view.canvas.height); // left
        this.view.context.clearRect(os.x, 0, this.view.canvas.width - os.x, this.view.canvas.height); // right
        this.view.context.clearRect(o.x, 0, s.x, o.y); // top
        this.view.context.clearRect(o.x, os.y, s.x, this.view.canvas.height - os.y); // bottom
    }

    redrawMono() {
        this.view.context.lineCap = "butt";

        for (let layer of Application.file.layers)
        {
            if (!layer.visible || layer == Application.file.currentLayer)
                continue;

            if (layer.hideInRenderPreview)
                this.view.context.setLineDash([(Settings.guideDashSize) * this.view.camera.zoom, (Settings.guideDashSize) * this.view.camera.zoom]);

            for (let line of layer.data.lines)
                this.batchLine(new Line(line.start.position, line.end.position), false);
            this.renderBatchedLines(1, 'grey', false, false);

            for (let instance of layer.data.instances)
                for (let line of instance.getAsLines())
                    this.batchLine(new Line(line.start.position, line.end.position), false);
            this.renderBatchedLines(1, 'darkgreen', false, false);


            if (layer.hideInRenderPreview)
                this.view.context.setLineDash([]);
        }
        // current layer
        if (Application.file.currentLayer.hideInRenderPreview)
            this.view.context.setLineDash([(Settings.guideDashSize) * this.view.camera.zoom, (Settings.guideDashSize) * this.view.camera.zoom]);

        for (let line of Application.file.currentLayer.data.lines)
            this.batchLine(new Line(line.start.position, line.end.position), false);
        this.renderBatchedLines(1, 'black', false, false);


        for (let instance of Application.file.currentLayer.data.instances)
            for (let line of instance.getAsLines())
                this.batchLine(new Line(line.start.position, line.end.position), false);
        this.renderBatchedLines(3, 'green', false, false);


        // selection
        for (let line of this.view.selection.data.lines)
            this.batchLine(new Line(line.start.position, line.end.position), false);
        this.renderBatchedLines(1, Settings.selectionColor, false, false);

        for (let line of this.view.selection.data.partialLines)
            this.batchLine(new Line(line.start.position, line.end.position), false);
        this.renderBatchedLines(1, 'yellow', false, false);


        for (let instance of this.view.selection.data.instances)
            for (let line of instance.getAsLines())
                this.batchLine(new Line(line.start.position, line.end.position), false);
        this.renderBatchedLines(3, 'lightgreen', false, false);


        this.drawPreviewLine();

        if (!(this.view.logic.currentState instanceof DrawingState)
            && this.showCursor == true
            )
            this.drawCursor();

        if (this.view.logic.currentState instanceof MoveLinesState)
            this.drawMoveLinesPreview();

        if (this.view.logic.currentState instanceof RotateState)
            this.drawRotateLinesPreview();

        if (this.view.logic.currentState instanceof ScaleState)
            this.drawScaleLinesPreview();


        if (Application.file.currentLayer.hideInRenderPreview)
            this.view.context.setLineDash([]);

    }

    redrawSpeed() {
        this.view.context.lineCap = "butt";
        this.renderLayers();
    }

    redrawQuality() {
        this.view.context.lineCap = "round";
        this.renderLayers();
    }

    drawRenderBG() {
        let origin = Preferences.artBounds.origin;
        let size = Preferences.artBounds.size;

        origin = this.view.camera.canvasSpaceToScreenSpace(origin);
        size = size.multiply(this.view.camera.zoom);

        // TODO not culled
        this.view.context.fillStyle = 'white';
        this.view.context.fillRect(origin.x, origin.y, size.x, size.y);
    }

    drawArtbounds() {
        let origin = Preferences.artBounds.origin;
        let size = Preferences.artBounds.size;

        origin = this.view.camera.canvasSpaceToScreenSpace(origin);
        size = size.multiply(this.view.camera.zoom);

        // TODO not culled
        this.view.context.lineWidth = Settings.artBoundsLineWidth;
        this.view.context.strokeStyle = Settings.artBoundsStroke;
        this.view.context.strokeRect(origin.x, origin.y, size.x, size.y);

        this.view.context.fillStyle = Settings.artBoundsFill;
        this.view.context.fillRect(origin.x, origin.y, size.x, size.y);
    }

   
    renderLayers() {
        // layers below current layer
        for (let layer of Application.file.layers) {
            if (layer == Application.file.currentLayer)
                break;

            if (!(layer.visible))
                continue;

            if (layer.hideInRenderPreview == true)
                this.drawRenderHiddenLayer(layer);
            else
                this.drawLayer(layer);

        }

        // current Layer
        if (Globals.mirrorX || Globals.mirrorY)
            this.drawMirroredLines();

        // draw green outline if in prefab edit mode

        if (Application.file.currentLayer.hideInRenderPreview == true)
            this.drawRenderHiddenLayer(Application.file.currentLayer);
        else
            this.drawLayer(Application.file.currentLayer);

        // draw dotted original lines 
        if (!this.view.isRenderPreviewing)
            this.drawDottedOriginals();

        // draw selection in correct sorting order if previewing
        if (this.view.isRenderPreviewing) {
            if (!(this.view.logic.currentState instanceof InputRecorderState)) {
                this.drawSelectionOutline(new Vector2(0, 0));
                this.drawSelection();
            }
        }

        this.drawPreviewLine();

        if (!(this.view.logic.currentState instanceof DrawingState)
            && this.showCursor == true
            )
            this.drawCursor();

        if (this.view.logic.currentState instanceof MoveLinesState)
            this.drawMoveLinesPreview();
        else if (this.view.logic.currentState instanceof RotateState)
            this.drawRotateLinesPreview();
        else if (this.view.logic.currentState instanceof ScaleState)
            this.drawScaleLinesPreview();






        // layers above current layer
        let above = false;
        for (let layer of Application.file.layers) {
            if (layer == Application.file.currentLayer) {
                above = true;
                continue;
            }
            if (!above)
                continue;

            if (!layer.visible)
                continue;

            if (layer.hideInRenderPreview == true)
                this.drawRenderHiddenLayer(layer);
            else
                this.drawLayer(layer);
        }

        /*/ draw outline and selected lines if not previewing
            if (!(Application.currentView.logic.currentState instanceof InputRecorderState))
                this.drawSelectionOutline(new Vector2(0, 0));
            if (!this.view.isRenderPreviewing && !(Application.currentView.logic.currentState instanceof InputRecorderState))
                this.drawSelection();
                // */

        // draw selection on top of everything if not previewing
        if (!this.view.isRenderPreviewing) {
            if (!(this.view.logic.currentState instanceof InputRecorderState)) {
                this.drawSelectionOutline(new Vector2(0, 0));
                this.drawSelection();
            }
        }
    }

    

    // TODO use prefab like stuff?
    drawMirroredLines() {
        // draw mirrored lines
        for (let line of Application.file.currentLayer.data.lines.concat(this.view.selection.data.lines.concat(this.view.selection.data.partialLines)))
        {
            let thickness = line.thickness;
            let color = line.color.copy();

            if (!this.view.isRenderPreviewing)
                color.a = 0.3;

            if (Globals.mirrorX)
                this.drawLineFromTo(line.start.position.mirrorX(), line.end.position.mirrorX(), thickness, color);
            if (Globals.mirrorY)
                this.drawLineFromTo(line.start.position.mirrorY(), line.end.position.mirrorY(), thickness, color);
            if (Globals.mirrorX && Globals.mirrorY)
                this.drawLineFromTo(line.start.position.flipped(), line.end.position.flipped(), thickness, color);
        }
    }

    drawLayer(layer) {
        for (let line of layer.data.lines)
        {
            let thickness = line.thickness;
            let color = line.color.copy();

            if (!this.view.isRenderPreviewing && layer != Application.file.currentLayer) {
                //thickness *= 0.3;
                color.r = Math.floor((color.r + 127) / 2.0);
                color.g = Math.floor((color.g + 127) / 2.0);
                color.b = Math.floor((color.b + 127) / 2.0);
            }

            this.drawLineFromTo(line.start.position, line.end.position, thickness, color);

            if (Preferences.showLineHandles) {
                let radius = (layer == Application.file.currentLayer && !this.view.isRenderPreviewing) ? line.thickness * 0.5 + this.handleSizeFactor / this.view.camera.zoom : line.thickness * 0.5;
                this.drawRealCircle(line.start.position, radius, 0, color, false, false, true)
                this.drawRealCircle(line.end.position, radius, 0, color, false, false, true)
            }
        }

        let color = Application.file.currentLayer == layer ? Settings.prefabColor : null;
        this.drawInstances(layer.data.instances, color);
    }

    drawRenderHiddenLayer(layer) {
        if (this.view.isRenderPreviewing && Preferences.renderGuideLayers == false)
            return;

        this.view.context.setLineDash([Settings.guideDashSize * this.view.camera.zoom, Settings.guideDashSize * this.view.camera.zoom]);

        for (let line of layer.data.lines)
            this.batchLine(line, false);
        this.renderBatchedLines(1, 'black', false, false);

        if (Preferences.showLineHandles) {
            let radius = this.handleSizeFactor / this.view.camera.zoom;
            for (let p of Utilities.linesToLineEndings(layer.data.lines))
                this.drawRealCircle(p.position, radius, 0, p.line.color, false, false, true); // TODO batch circles?
        }

        this.view.context.setLineDash([]);
    }
}