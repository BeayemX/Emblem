﻿class ZoomState extends State {
    enter() {
        this.startZoom = Application.currentView.camera.zoom;
        this.startPosScreenSpace = Application.currentView.camera.canvasSpaceToScreenSpace(Application.cursor.position);
    }

    exit() {
    }

    update(newPosScreenSpace) {
        // TODO ctrl+mmb zoom is MAGIC
        //Application.currentView.camera.setZoom(this.startZoom + (newPosScreenSpace.y - this.zoomInitScreenPos.y) / canvas.height * 4, true);
        Application.currentView.camera.zoomBy(
            (newPosScreenSpace.y - this.startPosScreenSpace.y)
            / Application.currentView.canvas.height
            * Application.currentView.camera.zoom
            * 4
            , true);
        this.startPosScreenSpace = newPosScreenSpace;
    }

    toString() {
        return "ZoomState";
    }
}