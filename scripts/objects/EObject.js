﻿class EObject {
    constructor(position, rotation, scale) {
        this.transform = new Transform(position, rotation, scale);
        // line --> getter which calculates mean of start and end
    }
    
    objectSpaceForPosition(pos) {
        return this.transform.transformSpaceForPosition(pos);
    }

    getAsLines() { throw ("getAsLines() not implemented"); }

    equals() { throw ("equals() not implemented"); }
    copy() { throw ("copy() not implemented"); }
    

    selectionHit() { throw ("selectionHit() not implemented"); }
    boxSelectionHit(bounds) { throw ("boxSelectionHit() not implemented"); }

    executeSelection(bounds) { throw ("executeSelection() not implemented"); }
    executeBoxSelection(bounds) { throw ("executeBoxsSelection() not implemented"); }

    render() { throw ("render() not implemented"); }
}