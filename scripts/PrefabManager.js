﻿class PrefabManager {
    static init() {
        console.log("PrefabManager created.");
        this.prefabs = [];
        GUI.buildPrefabUI();
    }

    static addPrefab(prefab) {
        this.prefabs.push(prefab);
    }

    static createNewPrefab() {
        let newPrefab = new Prefab();
        //Application.file.currentLayer.data.addInstance(new PrefabInstance(newPrefab, Vector2.zero));
        Application.file.updateStats();

        this.addPrefab(newPrefab);
        this.editPrefab(this.prefabs.length - 1);


        GUI.buildPrefabUI();
        //Application.currentView.camera.resetView();
    }

    static createNewPrefabFromSelection() {
        let newPrefab = new Prefab();
        newPrefab.data.lines = Application.sceneView.selection.data.lines.concat(Application.sceneView.selection.data.partialLines);
        newPrefab.data.instances = Application.sceneView.selection.data.instances;

        let instance = new PrefabInstance(newPrefab, Vector2.zero);

        Application.sceneView.selection.data = new SelectionData(Application.sceneView.selection);
        Application.sceneView.selection.data.addInstance(instance);

        this.addPrefab(newPrefab);

        Application.file.updateStats();
        GUI.buildPrefabUI();
        Application.redrawAllViews();
    }

    static editPrefab(id) {
        let prefab = this.prefabs[id];
        if (Application.prefabEditorView.prefab) {
            Application._currentView = Application.prefabEditorView;
            Application.prefabEditorView.selection.clear();
        }

        if (Application.prefabEditorView.prefab == prefab) {
            Application.prefabEditorView.setPrefab(null);
        } else {
            Application.prefabEditorView.setPrefab(prefab);
        }

        GUI.buildPrefabUI();
        Application.redrawAllViews();
    }

    static selectPrefabWithID(id) {
        if (Application.logic.currentState instanceof PrefabBrushState
            && Application.logic.currentState.currentPrefab == this.prefabs[id]
            ) {
            Application.logic.currentState.cancel();
            Application.logic.setState(new IdleState());
            //Application.prefabEditorView.setPrefab(null);
        }
        else {
            Application.logic.setState(new PrefabBrushState(this.prefabs[id]));
            //Application.prefabEditorView.setPrefab(this.prefabs[id]);
        }

        GUI.buildPrefabUI();
    }

    static changeNameForPrefabWithID(id, name) {
        this.prefabs[id].name = name;
    }

    static convertPrefabWithIDToLines(id) {
        if (Application.prefabEditorView.prefab &&
            Application.prefabEditorView.prefab.includesNested(this.prefabs[id])) {
            Application.prefabEditorView.selection.clear();

            if (Application.prefabEditorView.prefab == this.prefabs[id]) 
                Application.prefabEditorView.setPrefab(null);
        }

        // convert instances to lines
        // TODO what happens to nested prefabs?

        Application.sceneView.selection.clear();

        // all but current layer. 
        // convert instances to lines and put into corresponding layer
        for (let layer of Application.file.layers) {
            if (layer == Application.file.currentLayer)
                continue;

            for (let i = layer.data.instances.length - 1; i >= 0; --i) {
                let instance = layer.data.instances[i];
                if (instance.reference == this.prefabs[id]) {
                    // TODO use instance.dissolve() instead of instance.getAsLines()
                    for (let line of instance.getAsLines()) {
                        layer.data.addLine(line);
                    }
                    layer.data.removeInstance(instance);
                }
            }
        }

        // current layer
        // convert instances to lines and put them into the selection
        for (let i = Application.file.currentLayer.data.instances.length - 1; i >= 0; --i) {
            let instance = Application.file.currentLayer.data.instances[i];
            if (instance.reference == this.prefabs[id]) {
                // TODO use instance.dissolve() instead of instance.getAsLines()
                // TODO use addLinesUnsave()?
                for (let line of instance.getAsLines())
                    Application.sceneView.selection.data.addLine(line);

                Application.sceneView.data.removeInstance(instance);
            }
        }

        // other prefabs if nested
        for (let prefab of this.prefabs) {
            let instance;
            for (let i = prefab.data.instances.length - 1; i >= 0; --i) {
                instance = prefab.data.instances[i];
                if (instance.reference == this.prefabs[id]) {
                    // TODO use instance.dissolve() instead of instance.getAsLines()
                    for (let line of instance.getAsLines()) {
                        prefab.data.addLine(line);
                    }
                    prefab.data.removeInstance(instance);
                }
            }
        }
        
        Utilities.deleteArrayEntry(this.prefabs, this.prefabs[id]);
        GUI.buildPrefabUI();
        Application.redrawAllViews();
    }

    static deletePrefabWithID(id) {
        if (Application.prefabEditorView.prefab &&
            Application.prefabEditorView.prefab.includesNested(this.prefabs[id])) {
            Application.prefabEditorView.selection.clear();

            if (Application.prefabEditorView.prefab == this.prefabs[id])
                Application.prefabEditorView.setPrefab(null);
        }

        Application.sceneView.selection.clear();

        for (let layer of Application.file.layers) {
            for (let i = layer.data.instances.length - 1; i >= 0; --i) {
                let instance = layer.data.instances[i];
                if (instance.reference == this.prefabs[id]) {
                    layer.data.removeInstance(instance);
                }
            }
        }

        // other prefabs if nested
        for (let prefab of this.prefabs) {
            let instance;
            for (let i = prefab.data.instances.length - 1; i >= 0; --i) {
                instance = prefab.data.instances[i];
                if (instance.reference == this.prefabs[id]) {
                    prefab.data.removeInstance(instance);
                }
            }
        }

        Utilities.deleteArrayEntry(this.prefabs, this.prefabs[id]);

        GUI.buildPrefabUI();
        Application.file.updateStats();
        Application.redrawAllViews();
    }

    static convertSelectedInstancesToLines() {
        let lines = [];
        let instances = Application.currentView.selection.data.instances;

        for (let instance of instances)
            lines = lines.concat(instance.getAsLines());


        Application.sceneView.selection.data.instances = [];
        Application.sceneView.selection.clear();

        Application.sceneView.selection.data.lines = lines;
        Application.redrawAllViews();
    }

    static generateIDsForPrefabs() {
        for (let i = 0; i < this.prefabs.length; ++i)
            this.prefabs[i].id = i;
    }
}