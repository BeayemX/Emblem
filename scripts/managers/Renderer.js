﻿class Renderer {
    constructor(view) {
        this.view = view;

        // batching
        this.batchedLines = [];
        this.batchedCircles = [];

        // for every frame
        this.screenBounds = null;
        this.drawnLinesCounter = 0;
        this.culledLinesCounter = 0;
        this.culledCirclesCounter = 0;
        this.batchedCirclesCounter = 0;
        this.copiedCirclesCounter = 0;
        this.drawnCirclesCounter = 0;

        // v-sync
        this.oldStep = 0;
        this.requestRedraw = false;
        this.fps = 0;
    }


    // // // // // // // // // // // // // 
    // // // ABSTRACT METHODS BEGINN // // 
    // // // // // // // // // // // // // 

    _redraw() { // gets called automatically
        console.log("_redraw() not implemented!");
    }

    // // // // // // // // // // // // // 
    // // // ABSTRACT METHODS END // // // 
    // // // // // // // // // // // // // 


    generateGradient(start, end, toColor) {
        start = this.view.camera.canvasSpaceToScreenSpace(start);
        end = this.view.camera.canvasSpaceToScreenSpace(end);

        let gradient = this.view.context.createLinearGradient(start.x, start.y, end.x, end.y);
        gradient.addColorStop(0, Settings.selectionColorFill);
        gradient.addColorStop(1, toColor.toString());
        return gradient;
    }

    redraw(step) {
        // TODO not sure if check is necessary
        if (!this.view.context)
            return;

            // v-sync
        if (arguments.length == 0) {
            if (!this.requestRedraw) {
                this.requestRedraw = true;
                window.requestAnimationFrame(step => this.redraw(step));
            }
            return;
        }
        else {
            this.requestRedraw = false;
        }

        // rendering
        this._preRender();
        this._redraw();
        this._postRender(step);
    }

    _preRender() {
        this.screenBounds = this.view.camera.getVisibleBounds();

        this.drawnLinesCounter = 0;
        this.culledLinesCounter = 0;
        this.culledCirclesCounter = 0;
        this.batchedCirclesCounter = 0;
        this.copiedCirclesCounter = 0;
        this.drawnCirclesCounter = 0;

        this._clear();
    }

    _postRender(step) {
        // v-sync
        this.fps = 1000 / (step - this.oldStep);
        this.oldStep = step;

        // write displayed stats
        GUI.writeToStats("FPS", this.fps.toFixed(2));
        GUI.writeToStats("Lines drawn", this.drawnLinesCounter);
        GUI.writeToStats("Culled lines", this.culledLinesCounter);
        GUI.writeToStats("Culled circles", this.culledCirclesCounter);
        GUI.writeToStats("Circles batched", this.batchedCirclesCounter);
        GUI.writeToStats("Circles copied", this.copiedCirclesCounter);
        GUI.writeToStats("Circles drawn", this.drawnCirclesCounter);
    }

    drawRealCircle(center, radius, thickness, color, screenSpace, screenSpaceThickness, filled) {

        if (!this.screenBounds.shrink(-(radius + thickness * 0.5)).contains(center)) {
            ++this.culledCirclesCounter;
            return;
        }

        center = center.copy();
        if (!screenSpace) {
            center.x += this.view.camera.position.x;
            center.y += this.view.camera.position.y;

            center.x *= this.view.camera.zoom;
            center.y *= this.view.camera.zoom;

            radius *= this.view.camera.zoom;
        }
        if (!screenSpaceThickness) {
            thickness *= this.view.camera.zoom;
        }

        this.view.context.beginPath();
        this.view.context.lineWidth = thickness;

        this.view.context.arc(center.x, center.y, radius, 0, 2 * Math.PI);

        if (filled) {
            this.view.context.fillStyle = color;
            this.view.context.fill();
        } else {
            this.view.context.strokeStyle = color;
            this.view.context.stroke();
        }

        ++this.drawnCirclesCounter;
    }

    drawLineFromTo(p1, p2, thickness, color, screenSpace, screenSpaceThickness) {
        if (p1 instanceof LineEnding)
            p1 = p1.position;
        if (p2 instanceof LineEnding)
            p2 = p2.position;

        if (!screenSpace) {
            if (!this.screenBounds.containsLine(p1, p2, thickness)) {
                ++this.culledLinesCounter;
                return;
            }
        } else {
            let p1_ = this.view.camera.screenSpaceToCanvasSpace(p1);
            let p2_ = this.view.camera.screenSpaceToCanvasSpace(p2);
            if (!this.screenBounds.containsLine(p1_, p2_, thickness)) {
                ++this.culledLinesCounter;
                return;
            }
        }

        p1 = p1.copy();
        p2 = p2.copy();

        if (!screenSpace) {
            p1.x += this.view.camera.position.x;
            p1.y += this.view.camera.position.y;
            p2.x += this.view.camera.position.x;
            p2.y += this.view.camera.position.y;

            p1.x *= this.view.camera.zoom;
            p1.y *= this.view.camera.zoom;
            p2.x *= this.view.camera.zoom;
            p2.y *= this.view.camera.zoom;

        }

        if (!screenSpaceThickness)
            thickness *= this.view.camera.zoom;

        this.view.context.beginPath();
        this.view.context.lineWidth = thickness;
        this.view.context.strokeStyle = color;
        this.view.context.moveTo(p1.x, p1.y);
        this.view.context.lineTo(p2.x, p2.y);

        this.view.context.stroke();

        ++this.drawnLinesCounter;
    }

    batchLine(line, ignoreCulling) { // TODO remove me if lines outside of screen rect get drawn
        if (ignoreCulling || this.screenBounds.contains(line))
            this.batchedLines.push(line);
        else
            ++this.culledLinesCounter;

    }

    batchCircle(circle) {
        if (this.screenBounds.contains(circle)) {
            if (this.batchedCircles[circle.toString()] == undefined)
                this.batchedCircles[circle.toString()] = circle;
            else
                ++this.batchedCirclesCounter;
        }
        else
            ++this.culledCirclesCounter;
    }

    renderBatchedLines(thickness, color, screenSpace, screenSpaceThickness) {
        if (this.batchedLines.length == 0)
            return;

        if (color instanceof Color)
            color = color.toHexString();

        this.view.context.beginPath();

        if (!screenSpaceThickness)
            thickness *= this.view.camera.zoom;


        this.view.context.lineWidth = thickness;
        this.view.context.strokeStyle = color;

        let p1;
        let p2;

        for (let line of this.batchedLines) {
            p1 = line.start.position.copy();
            p2 = line.end.position.copy();

            if (!screenSpace) {
                p1.x += this.view.camera.position.x;
                p1.y += this.view.camera.position.y;
                p2.x += this.view.camera.position.x;
                p2.y += this.view.camera.position.y;

                p1.x *= this.view.camera.zoom;
                p1.y *= this.view.camera.zoom;
                p2.x *= this.view.camera.zoom;
                p2.y *= this.view.camera.zoom;
            }

            this.view.context.moveTo(p1.x, p1.y);
            this.view.context.lineTo(p2.x, p2.y);
        }

        this.drawnLinesCounter += Object.keys(this.batchedLines).length;

        this.view.context.stroke();
        this.batchedLines = [];
    }

    renderBatchedCircles(radius, thickness, color, screenSpace, screenSpaceSize, filled) {

        if (Object.keys(this.batchedCircles).length == 0)
            return;

        if (!screenSpaceSize) {
            radius *= this.view.camera.zoom;
            thickness *= this.view.camera.zoom;
        }

        // TODO margin doesnt help much. circles sometimes still seem chopped off
        let margin = 10;
        let doubleRadius = radius * 2 + thickness * 2 + margin;
        offscreenCanvas.width = doubleRadius;
        offscreenCanvas.height = doubleRadius;
        offscreenCanvas.style.left = -doubleRadius;
        offscreenCanvas.style.top = -doubleRadius;

        offscreenContext.beginPath();
        offscreenContext.strokeStyle = color;
        offscreenContext.fillStyle = color;
        offscreenContext.lineWidth = thickness;
        offscreenContext.arc(radius + thickness + margin, radius + thickness + margin, radius, 0, 2 * Math.PI);

        ++this.drawnCirclesCounter;

        if (filled)
            offscreenContext.fill();
        else
            offscreenContext.stroke();

        let center = new Vector2(0, 0);
        let circle = null;
        for (let key in this.batchedCircles) {
            if (this.batchedCircles.hasOwnProperty(key)) {
                circle = this.batchedCircles[key];
                center.setValues(circle.x, circle.y);
                if (!screenSpace) {
                    center.x += this.view.camera.position.x;
                    center.y += this.view.camera.position.y;

                    center.x *= this.view.camera.zoom;
                    center.y *= this.view.camera.zoom;
                }
                this.view.context.drawImage(offscreenCanvas, center.x - (radius + thickness + margin), center.y - (radius + thickness + margin));
                ++this.copiedCirclesCounter;
            }
        }

        this.batchedCircles = [];
    }

    _clear() {
        this.view.context.clearRect(0, 0, this.view.canvas.width, this.view.canvas.height);
    }

    drawBorderSelection() {
        if (!this.view.logic.currentState.startPos || !this.view.logic.currentState.currentPos)
            return;

        this.view.context.strokeStyle = Settings.selectionColor;
        this.view.context.fillStyle = Settings.boxSelectionColor;

        let leftTop = this.view.camera.canvasSpaceToScreenSpace(this.view.logic.currentState.startPos);
        let sizeCanvasSpace = this.view.logic.currentState.currentPos.subtractVector(this.view.logic.currentState.startPos);
        let size = sizeCanvasSpace.multiply(this.view.camera.zoom);

        if (this.view.logic.currentState.artBounds) {
            leftTop = this.view.camera.canvasSpaceToScreenSpace(this.view.logic.currentState.startPos.rounded());
            sizeCanvasSpace = this.view.logic.currentState.currentPos.subtractVector(this.view.logic.currentState.startPos).rounded();
            size = sizeCanvasSpace.multiply(this.view.camera.zoom);
        }

        this.view.context.rect(leftTop.x, leftTop.y, size.x, size.y);

        if (!this.view.logic.currentState.artBounds)
            this.view.context.fillRect(leftTop.x, leftTop.y, size.x, size.y);
        this.view.context.stroke();
    }

    // TODO unify settings spread across Settings und GridManager...
    _drawGrid() {
        if (!Preferences.showGrid)
            return;

        for (let line of GridManager.grid.getLines(this.view))
            this.drawLineFromTo(line.start.position, line.end.position, line.thickness, line.color.toString());
        //this.__batchLine(line);
        return;
    }

    drawMeasure(p1, p2) {
        // measure
        let size = p2.subtractVector(p1);
        let dashFactor = 1;

        /* // shadow outline
        this.view.context.shadowColor = "white";
        this.view.context.shadowOffsetX = 0;
        this.view.context.shadowOffsetY = 0;
        this.view.context.shadowBlur = 1;
        // */

        // x
        let dashSize = GridManager.grid.width * this.view.camera.zoom * dashFactor;
        this.view.context.setLineDash([dashSize, dashSize]);
        this.drawLineFromTo(p1, p1.addVector(new Vector2(size.x, 0)), 2, 'black', false, true);

        // y
        dashSize = GridManager.grid.height * this.view.camera.zoom * dashFactor;
        this.view.context.setLineDash([dashSize, dashSize]);
        this.drawLineFromTo(p1.addVector(new Vector2(size.x, 0)), p2, 2, 'black', false, true);
        this.view.context.setLineDash([]);

        // xy-diagonal
        dashSize = Math.sqrt(GridManager.grid.width * GridManager.grid.width + GridManager.grid.height * GridManager.grid.height) * this.view.camera.zoom * dashFactor;
        this.view.context.setLineDash([dashSize, dashSize]);
        this.drawLineFromTo(p1, p2, 2, 'black', false, true);
        this.view.context.setLineDash([]);

        // texts
        // setup
        this.view.context.font = "16pt Oswald";
        this.view.context.fillStyle = 'black';
        this.view.context.textAlign = "center";
        this.view.context.textBaseline = "middle";
        let offset = new Vector2(25, 20);
        // place information
        // x
        let pos = p1.copy();
        pos.x += size.x * 0.5;
        pos = this.view.camera.canvasSpaceToScreenSpace(pos);
        pos.y = (size.y > 0) ? pos.y - offset.y : pos.y + offset.y;

        this.view.context.fillText(+((size.x / GridManager.grid.width).toFixed(2)), pos.x, pos.y);
        this.view.context.fillText(+((size.x / GridManager.grid.width).toFixed(2)), pos.x, pos.y);

        // y
        pos = p2.copy();
        pos.y -= size.y * 0.5;
        pos = this.view.camera.canvasSpaceToScreenSpace(pos);
        pos.x = (size.x > 0) ? pos.x + offset.x : pos.x - offset.x;
        this.view.context.fillText(+((size.y / GridManager.grid.height).toFixed(2)), pos.x, pos.y);

        // xy
        pos = p1.addVector(p2).divide(2);
        let off = LineManipulator.rotatePositionByAngle(size.normalized().multiply(30), Vector2.zero, -90);
        off = off.divide(this.view.camera.zoom);
        off = (size.y > 0) ? off.flipped() : off;
        pos = (size.x > 0) ? pos.addVector(off) : pos.subtractVector(off);
        pos = this.view.camera.canvasSpaceToScreenSpace(pos);

        let x = size.x / GridManager.grid.width;
        let y = size.y / GridManager.grid.height;
        this.view.context.fillText(+(Math.sqrt(x * x + y * y).toFixed(2)), pos.x, pos.y);

        // angle
        pos = p1.addVector(size.normalized().multiply(33 / this.view.camera.zoom));
        pos = LineManipulator.rotatePositionByAngle(pos, p1, 180);
        pos = this.view.camera.canvasSpaceToScreenSpace(pos);
        let num = +(Math.atan2(size.y, size.x) * (180 / Math.PI)).toFixed(2);
        this.view.context.fillText(num + "°", pos.x, pos.y);

        // crosshair line end
        let s = 10 / this.view.camera.zoom;
        let crossSize = new Vector2(s, s);
        this.drawLineFromTo(p2.addVector(crossSize), p2.subtractVector(crossSize), 1, 'black', false, true);
        crossSize.x = -crossSize.x;
        this.drawLineFromTo(p2.subtractVector(crossSize), p2.addVector(crossSize), 1, 'black', false, true);

        // restore
        /* // shadow outline
        this.view.context.shadowColor = 'black';
        this.view.context.shadowOffsetX = 0;
        this.view.context.shadowOffsetY = 0;
        this.view.context.shadowBlur = 0;
        // */
    }

    drawCrosshair(dontSnap) {
        let screenpos;

        if (dontSnap)
            screenpos = this.view.camera.canvasSpaceToScreenSpace(Application.cursor.position.copy());
        else
            screenpos = this.view.camera.canvasSpaceToScreenSpace(Application.cursor.currentPosition);

        if (this.view.logic.currentState.artBounds)
            screenpos = this.view.camera.canvasSpaceToScreenSpace(Application.cursor.position.copy().rounded());

        this.drawLineFromTo(new Vector2(0, screenpos.y), new Vector2(this.view.canvas.width, screenpos.y), Settings.crossHairLineWidth, Settings.crossHairColor, true, true);
        this.drawLineFromTo(new Vector2(screenpos.x, 0), new Vector2(screenpos.x, this.view.canvas.height), Settings.crossHairLineWidth, Settings.crossHairColor, true, true);
    }

    drawInstances(instances, outlinecolor, deltaPos, deltaRot, deltaScale, center) {
        // draw prefab instances
        for (let instance of instances) {
            
        // draw center point
            let pos = instance.transform.position;

        // normal transform operation with delta stuff
            if (deltaPos)
                pos = pos.addVector(deltaPos);
            if (deltaRot)
                pos = pos.rotateAround(center, deltaRot);
            if (deltaScale)
                pos = pos.scaleFrom(center, deltaScale);

            if (!this.view.isRenderPreviewing)
                this.batchCircle(pos);


        // instances as lines
            let instanceAsLines = this.instanceCrap(instance.getAsLines(), pos, deltaPos, deltaRot, deltaScale, center);

        // light green outline
            if (outlinecolor)
                if (!this.view.isRenderPreviewing)
                    for (let line of instanceAsLines)
                        this.drawLineFromTo(line.start, line.end, line.thickness + Settings.prefabOutlineSize, outlinecolor);

        // lines in instance space
            for (let line of instanceAsLines)
                this.drawLineFromTo(line.start, line.end, line.thickness, line.color);
        }

        // show instance center
        if (!this.view.isRenderPreviewing)
            this.renderBatchedCircles(2, 0, outlinecolor, false, true, true);
    }

    instanceCrap(lines, pos, deltaPos, deltaRot, deltaScale, center) {
        for (let line of lines)
        {
            if (deltaPos)
                line.move(deltaPos);

            if (deltaRot) {
                line.rotateAround(center, deltaRot);

                if (Preferences.onlyTransformPrefabInstancePosition)
                    line.rotateAround(pos, -deltaRot);
            }

            if (deltaScale) {
                line.scaleFrom(center, deltaScale);

                if (Preferences.onlyTransformPrefabInstancePosition)
                    line.scaleFrom(pos, deltaScale.oneDividedByThisVector());
            }
        }

        return lines;
    }

    // TODO maybe move parameter somewhere else? not sure if it should be a parameter...
    // same as with prefabs... method which transforms the given selection
    drawSelectionOutline(delta) {
        if (this.view.isRenderPreviewing)
            return;

        if (Application.file.currentLayer.hideInRenderPreview)
            this.view.context.setLineDash([Settings.guideDashSize * this.view.camera.zoom, Settings.guideDashSize * this.view.camera.zoom]);

        for (let line of this.view.selection.data.lines)
            this.drawLineFromTo(line.start.position.addVector(delta), line.end.position.addVector(delta), line.thickness + this.outlineSize / this.view.camera.zoom, Settings.selectionColor);

        for (let p of this.view.selection.data.points) {
            let color = this.generateGradient(p.position.addVector(delta), p.opposite.position, Color.transparent());
            this.drawLineFromTo(p.position.addVector(delta), p.opposite.position, p.line.thickness + this.outlineSize / this.view.camera.zoom, color, false, false);
        }

        // selected points
        for (let p of this.view.selection.getAllSelectedPoints())
        {
            let radius = (Preferences.showLineHandles) ? p.line.thickness * 0.5 + this.handleSizeFactor / this.view.camera.zoom : p.line.thickness * 0.5;
        // TODO use batching
            this.drawRealCircle(p.position.addVector(delta), radius + this.outlineSize * 0.5 / this.view.camera.zoom, this.outlineSize / this.view.camera.zoom, Settings.selectionColor, false, false, true)
        }

        if (Application.file.currentLayer.hideInRenderPreview)
            this.view.context.setLineDash([]);
    }





























    drawSelection() {
        if (Application.file.currentLayer.hideInRenderPreview) {
            if (this.view.isRenderPreviewing && !Preferences.renderGuideLayers)
                return;
            else
                this.view.context.setLineDash([Settings.guideDashSize * this.view.camera.zoom, Settings.guideDashSize * this.view.camera.zoom]);
        }

        for (let line of this.view.selection.data.lines)
            this.drawLineFromTo(line.start.position, line.end.position, line.thickness, line.color);

        for (let p of this.view.selection.data.points)
            this.drawLineFromTo(p.position, p.opposite.position, p.line.thickness, p.line.color);

        if (Preferences.showLineHandles && !this.view.isRenderPreviewing)
            for (let line of this.view.selection.data.lines.concat(this.view.selection.data.partialLines)) {
                this.drawRealCircle(line.start.position, line.thickness * 0.5 + this.handleSizeFactor / this.view.camera.zoom, 0, line.color, false, false, true);
                this.drawRealCircle(line.end.position, line.thickness * 0.5 + this.handleSizeFactor / this.view.camera.zoom, 0, line.color, false, false, true);
            }

        this.drawInstances(this.view.selection.data.instances, Settings.prefabSelectionColor);

        if (Application.file.currentLayer.hideInRenderPreview)
            this.view.context.setLineDash([]);
    }

    drawDottedOriginals() {
        if (!(this.view.isRenderPreviewing && Application.file.currentLayer.hideInRenderPreview)) {
            // selected lines. dotted if origin while manipulating lines
            let drawDotted = this.view.logic.currentState instanceof InputRecorderState;

            // draw dotted originals
            if (!this.view.isRenderPreviewing) {
                for (let line of this.view.selection.data.lines.concat(this.view.selection.data.partialLines)) {
                    if (drawDotted)
                        this.view.context.setLineDash([line.thickness * 6 * this.view.camera.zoom, line.thickness * 4 * this.view.camera.zoom]);
                    if (Application.file.currentLayer.hideInRenderPreview)
                        this.view.context.setLineDash([Settings.guideDashSize * this.view.camera.zoom, Settings.guideDashSize * this.view.camera.zoom]);

                    let thickness = drawDotted ? line.thickness * 0.5 : line.thickness;
                    let color = line.color.copy();

                    this.drawLineFromTo(line.start.position, line.end.position, thickness, line.color);

                    if (drawDotted || Application.file.currentLayer.hideInRenderPreview)
                        this.view.context.setLineDash([]);
                }
            }
        }
    }


    drawPreviewLine() {
        if (this.view.logic.currentState instanceof DrawingState) {
            let start = this.view.logic.currentState.startPos;
            let end = Application.cursor.currentPosition;
            this.drawLineFromTo(start, end, Application.currentBrush.lineThickness, Application.currentBrush.lineColor, false);
        } else if (this.view.logic.currentState instanceof MeasureState) {
            if (this.view.logic.currentState.startPosLMB && !this.view.logic.currentState.stoppedLMB) {
                let start = this.view.logic.currentState.startPosLMB;
                let end = this.view.logic.currentState.currentPosLMB;
                this.view.context.setLineDash([Settings.guideDashSize * this.view.camera.zoom, Settings.guideDashSize * this.view.camera.zoom]);
                this.drawLineFromTo(start, end, Application.currentBrush.lineThickness, 'black', false);
                this.view.context.setLineDash([]);
            }
        }
    }

    drawCursor() {
        if (Application.currentView != this.view)
            return;

        if (this.view.logic.currentState instanceof MeasureState) {
            let size = Application.cursor.range * Math.PI * 2 / 6 * this.view.camera.zoom;
            this.view.context.setLineDash([size, size]);
        }


        this.drawRealCircle(Application.cursor.currentPosition, Application.currentBrush.lineThickness * 0.5, 1, Application.currentBrush.lineColor.toString(), false, true, true);
        this.drawRealCircle(Application.cursor.position, Application.cursor.range, 2, Settings.selectionColor, false, true);

        if (this.view.logic.currentState instanceof BrushSelectionState)
            this.drawRealCircle(Application.cursor.position, Application.cursor.range, 2, Settings.boxSelectionColor, false, true, true);
        else if (this.view.logic.currentState instanceof MeasureState)
            this.view.context.setLineDash([]);

    }

    drawMoveLinesPreview() {
        let delta = this.view.logic.currentState.getDelta();
        this.drawSelectionOutline(delta);

        let other;

        // selected lines
        for (let line of this.view.selection.data.lines)
        {
            let thickness = line.thickness;
            let color = line.color.copy();

            this.drawLineFromTo(line.start.position.addVector(delta), line.end.position.addVector(delta), thickness, color);
        }

        // partially selected lines
        for (let point of this.view.selection.data.points) {
            let p = point.position.addVector(delta);

            let thickness = point.line.thickness;
            let color = point.line.color.copy();

            this.drawLineFromTo(p, point.opposite.position, thickness, color, false);
        }

        // selected points
        for (let p of Utilities.linesToLineEndings(this.view.selection.data.lines).concat(this.view.selection.data.points)) {
            let radius = (!this.view.isRenderPreviewing && Preferences.showLineHandles) ? p.line.thickness * 0.5 + this.handleSizeFactor / this.view.camera.zoom : p.line.thickness * 0.5;
            this.drawRealCircle(p.position.addVector(delta), radius, 0, p.line.color, false, false, true);
        }

        // prefab instances 
        this.drawInstances(this.view.selection.data.instances, Settings.prefabSelectionColor, delta);
    }


    drawRotateLinesPreview() {
        let angleDegree = this.view.logic.currentState.getAngle();
        let center;

        if (Preferences.usePivot)
            center = Globals.pivot;
        else
            center = Utilities.calculateCenterOfMass(this.view.selection.data.getAllPositions());

        // selected lines
        for (let line of this.view.selection.data.lines)
            this.drawLineFromTo(
                LineManipulator.rotatePositionByAngle(line.start.position, center, angleDegree),
                LineManipulator.rotatePositionByAngle(line.end.position, center, angleDegree),
                line.thickness,
                line.color
            );

        // partial selected lines
        for (let ending of this.view.selection.data.points)
            this.drawLineFromTo(
                LineManipulator.rotatePositionByAngle(ending.position, center, angleDegree),
                ending.opposite.position,
                ending.line.thickness,
                ending.line.color
            );

        this.drawInstances(this.view.selection.data.instances, Settings.prefabSelectionColor, null, angleDegree, null, center);
    }

    drawScaleLinesPreview() {
        let scaleVector = this.view.logic.currentState.getScaleVector();

        let center;
        if (Preferences.usePivot)
            center = Globals.pivot;
        else
            center = Utilities.calculateBBCenter(this.view.selection.data.getAllPositions());

        // selected lines
        for (let line of this.view.selection.data.lines)
            this.drawLineFromTo(
                LineManipulator.scalePoint(line.start.position, center, scaleVector),
                LineManipulator.scalePoint(line.end.position, center, scaleVector),
                line.thickness,
                line.color
            );


        for (let ending of this.view.selection.data.points)
            this.drawLineFromTo(
                LineManipulator.scalePoint(ending.position, center, scaleVector),
                ending.opposite.position,
                ending.line.thickness,
                ending.line.color
            );

        this.drawInstances(this.view.selection.data.instances, Settings.prefabSelectionColor, null, null, scaleVector, center);
    }

    drawPivot() {
        if (Preferences.usePivot) {
            let radius = 10 / this.view.camera.zoom;
            let color = Color.black();

            this.drawRealCircle(Globals.pivot, radius, 1, color, false, true, false);
            this.batchLine(new Line(Globals.pivot.add(-radius), Globals.pivot.add(radius)), true);
            this.batchLine(new Line(Globals.pivot.addVector(new Vector2(-radius, radius)), Globals.pivot.addVector(new Vector2(radius, -radius))), true);
            this.renderBatchedLines(1, color, false, true);
        }
    }

    drawPrefabPreview() {
        if (Application.currentView != this.view)
            return;

        let prefab = Application.logic.currentState.currentPrefab;
        let lines = prefab.getAsLines();

        if (Application.prefabEditorView.prefab == prefab) 
            for (let line of Application.prefabEditorView.getSelectionAsLines())
                    lines.push(line);

        let color = Settings.prefabColor;

        if (Application.currentView == Application.prefabEditorView
            && prefab.includesNested(Application.prefabEditorView.prefab))
        {
            color = 'red';
            // TODO use 
            // GUI.notify("Creating endless recursions isn't allowed!", color);
        }

        for (let line of lines)
            this.drawLineFromTo(
                line.start.position.addVector(Application.cursor.currentPosition),
                line.end.position.addVector(Application.cursor.currentPosition),
                line.thickness + Settings.prefabOutlineSize,
                color
            );
    }
}

