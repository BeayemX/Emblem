﻿class MeasureState extends State {
    constructor() {
        super();
    }

    enter() {
    }

    exit() {
    }

    mouseDown(button) {
        if (button == 0) {
            this.startPosLMB = Application.cursor.currentPosition;
            this.currentPosLMB = Application.cursor.currentPosition;
            this.stoppedLMB = false;
        } else if (button == 1) {
            this.startPosMMB = Application.cursor.currentPosition;
            this.currentPosMMB = Application.cursor.currentPosition;
            this.stoppedMMB = false;
        } else if (button == 2) {
            this.startPosRMB = Application.cursor.currentPosition;
            this.currentPosRMB = Application.cursor.currentPosition;
            this.stoppedRMB = false;
        }
    }

    mouseUp(button) {
        if (button == 0) {
            this.stoppedLMB = true;
            Application.file.addGuideLine(new Line(this.startPosLMB, this.currentPosLMB), true);
        } else if (button == 1) {
            this.stoppedMMB = true;
        } else if (button == 2) {
            this.stoppedRMB = true;
        }
    }

    top() {
        this.stopped = true;
        if (this.initWithLMB)

        this.locked = false;
    }
    update() {
        if (this.startPosLMB && !this.stoppedLMB)
            this.currentPosLMB = Application.cursor.currentPosition;
        else if (this.startPosMMB && !this.stoppedMMB)
            this.currentPosMMB = Application.cursor.currentPosition;
        else if (this.startPosRMB && !this.stoppedRMB)
            this.currentPosRMB = Application.cursor.currentPosition;
    }

    keyDown(keycode) {
    }

    keyUp(keycode) {
    }

    toString() {
        return "Measure State";
    }
}