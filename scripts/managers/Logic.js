var offscreenCanvas;
var offscreenContext;

let toolbar;

let waitingForStart = [];

class Logic {
    constructor() {
        this.currentState = new IdleState();
        this.previousState = new IdleState();
        this.waitingState = null;
    }

    setState(state) {
        if (this.currentState == state)
            return;

        this.currentState.exit();
        this.previousState = this.currentState;
        this.currentState = state;
        this.currentState.enter();

        Application.redrawAllViews();
        if (Preferences.developerMode) {
            ;//console.log(this.previousState + " --> " + this.currentState);
            GUI.writeToStats("currentState", state.toString());
        }

    }

    setWaitingState(state) {
        this.waitingState = state;
        let text = "null";
        if (state)
            text = state.toString();
        GUI.writeToStats("waitingState", text);
        Application.redrawAllViews();
    }

    useWaitingState() {
        this.setState(this.waitingState);
    }

    isPreviewing() {
        return Application.currentView.isRenderPreviewing;
    }

    _clearStates() {
        // TODO HACK
        if (this.currentState instanceof ContinuousDrawingState)
            return;

        if (this.currentState.cancel)
            this.currentState.cancel();

        this.previousState = new IdleState();
        this.setState(new IdleState());
        this.setWaitingState(null);

        console.log("States cleared.");
        // TODO also reset ctrlDown and so on?
    }
    
    
}