Exporter = new class {
    constructor() {
        this.renderResultWindow = null;
        this.showRenderResultOnCheckerBackground = false;
    }

    renderPNG() // img
    {
        let view = new FinalRenderView();
        let renderCanvas = view.canvas;
        let renderContext = view.context;

        renderCanvas.id = "rendercanvas";
        renderCanvas.width = Preferences.artBounds.size.x * Preferences.renderScaleFactor;
        renderCanvas.height = Preferences.artBounds.size.y * Preferences.renderScaleFactor;

        if (this.renderResultWindow != null)
            this.renderResultWindow.close();

        this.renderResultWindow = window.open("", "Render",
            'width=' + renderCanvas.width
            + ', height=' + renderCanvas.height
             + ', menubar=no, status=no, titlebar=no'

            );

        let doc = this.renderResultWindow.document;

        let colorStart = 127;
        let collDiffOffset = 7;
        let color1 = 'rgb(' + (colorStart + collDiffOffset) + ', ' + (colorStart + collDiffOffset) + ', ' + (colorStart + collDiffOffset) + ')';
        let color2 = 'rgb(' + (colorStart - collDiffOffset) + ', ' + (colorStart - collDiffOffset) + ', ' + (colorStart - collDiffOffset) + ')';
        let patternSize = 30;
        let docText = "";
        docText += '<html><head><title>Render Result</title><style>';
        docText += '*{margin:0px; padding: 0px;}';
        docText += 'input{position:absolute; top: 5px; right: 5px; padding: 10px; font-size:10pt;}';

        docText += 'img{position:absolute; top:0px;left:0px;}';
        docText += 'div{position:absolute; top:0px;left:0px;width: ' + renderCanvas.width + 'px; height: ' + renderCanvas.height + 'px;'
            + 'background: ' + color1 + ';'
            + 'background: '
                + 'linear-gradient(135deg, transparent 75%, ' + color2 + ' 0%) 0 0,'
                + 'linear-gradient(-45deg, transparent 75%, ' + color2 + ' 0%) ' + patternSize * 0.5 + 'px ' + patternSize * 0.5 + 'px,'
                + 'linear-gradient(135deg, transparent 75%, ' + color2 + ' 0%) ' + patternSize * 0.5 + 'px ' + patternSize * 0.5 + 'px,'
                + 'linear-gradient(-45deg, transparent 75%, ' + color2 + ' 0%) 0 0,'
                + color1 + ';'
            + 'background-size: ' + patternSize + 'px ' + patternSize + 'px;'
            + '}'

        docText += '</style></head>';
        doc.write(docText);

        // preparation
        view.isRenderPreviewing = true;

        let offset = new Vector2(0, 0);
        offset = offset.addVector(Preferences.artBounds.size.multiply(0.5));
        offset = offset.subtractVector(Preferences.artBounds.origin);
        offset = offset.subtractVector((new Vector2(renderCanvas.width * 0.5, renderCanvas.height * 0.5)).divide(Preferences.renderScaleFactor));
        view.camera.position = offset;
        view.camera.zoom = Preferences.renderScaleFactor;
        //view.renderer.setContext(renderContext);
        // TODO needed?
        view.renderer.showCursor = false;

        // render
        view.renderer.setQualitySetting("quality");
        view.renderer.redraw(0);
        docText = "";
        docText += "<body>";
        docText += "</body>";
        docText += "</html>";
        doc.write(docText);

        // add bg
        let checkerBG = doc.createElement('div');
        checkerBG.setAttribute("id", "checkerBG");
        doc.body.appendChild(checkerBG);

        if (this.showRenderResultOnCheckerBackground)
            checkerBG.style.visibility = "visible";
        else
            checkerBG.style.visibility = "hidden";
        // add img
        let image = doc.createElement('img');
        image.setAttribute("src", renderCanvas.toDataURL("image/png"));
        image.setAttribute("alt", "Rendered image");
        doc.body.appendChild(image);

        // add toggle BG button
        let button = doc.createElement('input');
        button.setAttribute("type", "button");
        button.setAttribute("onclick", "checkerBG.style.visibility = (checkerBG.style.visibility == 'visible') ? 'hidden' : 'visible';");
        button.value = "Toggle background";
        doc.body.appendChild(button);

        // finalize
        GUI.notify("Picture saved!");
    }

    exportAsSVG() {
        var name = prompt("Save Logo as: ");

        if (name) {
            // FIXME should just also use lines in selection...
            Application.clearAllSelections();

            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:application/svg+xml;charset=utf-8,' + encodeURIComponent(this.generateSVGString()));
            pom.setAttribute('download', name + ".svg");

            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            }
            else {
                pom.click();
            }

            return true;
        }

        return false;
    }

    generateSVGString() {
        PrefabManager.generateIDsForPrefabs();

        let b = Preferences.artBounds;
        let svgData = '<';
        svgData += 'svg xmlns="http://www.w3.org/2000/svg" version="1.1" ' +
        'width="' + b.width + '" ' +
        'height="' + b.height + '" ' +
        'viewBox="' + b.left + " " + b.top + " " + b.width + " " + b.height + '"' +
        '>\n';

        let layers = Application.file.layers;

        // layers
        for (let layer of layers) {
            svgData += '\t<g ';
            svgData += 'name="' + layer.name + '" ';
            svgData += 'visible="' + layer.visible + '" ';
            svgData += 'hideInRenderPreview="' + layer.hideInRenderPreview + '" ';

            // just for displaying svg
            if (layer.visible == false || (layer.hideInRenderPreview && !Preferences.renderGuideLayers))
                svgData += 'display="none" ';

            svgData += '>\n';

            svgData += this.generateSVGStringForLines(layer.data.lines, 2, (layer.hideInRenderPreview && Preferences.renderGuideLayers));
            svgData += this.generateSVGStringForInstances(layer.data.instances, 2, (layer.hideInRenderPreview && Preferences.renderGuideLayers));
            svgData += '\t</g>\n';

        }

        // prefabs
        svgData += this.generateSVGStringForPrefabs(1);

        svgData += '</svg>';

        return svgData;
    }

    generateSVGStringForLines(lines, indentations, dashedLines) {
        let text = "";
        let indent = "";
        for (var i = 0; i < indentations; i++) {
            indent += "\t"
        }
        for (let line of lines) {
            text +=
                indent +
                '<line ' +
                'x1="' + line.start.position.x + '" ' +
                'y1="' + line.start.position.y + '" ' +
                'x2="' + line.end.position.x + '" ' +
                'y2="' + line.end.position.y + '" ' +

                'stroke="' + line.color.toHexString() + '" ' +
                'stroke-opacity="' + (line.color.a) + '" ' +
                'stroke-width="' + line.thickness + '" ' +
                'stroke-linecap="round"';

            if (dashedLines) {
                text += " ";
                text += 'style="stroke-dasharray:' + Settings.guideDashSize + ',' + Settings.guideDashSize + '"';
            }

            text += '/> \n'
        }
        return text;
    }

    generateSVGStringForInstances(instances, indentations, dashedLines) {
        let text = "";
        let indent = "";
        for (var i = 0; i < indentations; i++) {
            indent += "\t"
        }
        for (let instance of instances) {
            // emblem related to reconstruct prefabs and instances
            text += indent;
            text += "<g prefabReferenceID='" + instance.reference.id + "'";
            text += " positionX='" + instance.transform.position.x + "'";
            text += " positionY='" + instance.transform.position.y + "'";
            text += " rotation='" + instance.transform.rotation + "'";
            text += " scaleX='" + instance.transform.scale.x + "'";
            text += " scaleY='" + instance.transform.scale.y + "'";
            text += "> \n";

            // just for svg displaying and usage in other software
            for (let line of instance.getAsLines()) {
                text +=
                indent + "\t" + 
                '<line ' +
                'x1="' + line.start.position.x + '" ' +
                'y1="' + line.start.position.y + '" ' +
                'x2="' + line.end.position.x + '" ' +
                'y2="' + line.end.position.y + '" ' +

                'stroke="' + line.color.toHexString() + '" ' +
                'stroke-opacity="' + (line.color.a) + '" ' +
                'stroke-width="' + line.thickness + '" ' +
                'stroke-linecap="round"';

                if (dashedLines) {
                    text += " ";
                    text += 'style="stroke-dasharray:' + Settings.guideDashSize + ',' + Settings.guideDashSize + '"';
                }

                text += '/> \n'
            }

            text += indent + "</g> \n";
        }
        return text;
    }

    generateSVGStringForPrefabs(indentations) {
        let text = "";
        let indent = "";
        for (var i = 0; i < indentations; i++) {
            indent += "\t"
        }

        for (let prefab of PrefabManager.prefabs)
        {
            text += indent;
            text += "<g prefabID='" + prefab.id + "' " + 'name="' + prefab.name + '" display="none"> \n';

            for (let line of prefab.data.lines) {
                text +=
                    indent + "\t" + // additional indent for lines
                    '<line ' +
                    'x1="' + line.start.position.x + '" ' +
                    'y1="' + line.start.position.y + '" ' +
                    'x2="' + line.end.position.x + '" ' +
                    'y2="' + line.end.position.y + '" ' +

                    'stroke="' + line.color.toHexString() + '" ' +
                    'stroke-opacity="' + (line.color.a) + '" ' +
                    'stroke-width="' + line.thickness + '" ' +
                    'stroke-linecap="round"';

                text += '/> \n'
            }
            for (let instance of prefab.data.instances) {
                text += indent + "\t"; // additional indent for instances
                text += "<g prefabReferenceID='" + instance.reference.id + "'";
                text += " positionX='" + instance.transform.position.x + "'";
                text += " positionY='" + instance.transform.position.y + "'";
                text += " rotation='" + instance.transform.rotation + "'";
                text += " scaleX='" + instance.transform.scale.x + "'";
                text += " scaleY='" + instance.transform.scale.y + "'";
                text += "></g> \n";                
            }

            text += indent + "</g> \n";
        }

        return text;
    }
}