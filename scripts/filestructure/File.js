﻿class File {
    constructor(createFirstLayer) {
        this.layers = [];

        if (createFirstLayer){
            let layer = new Layer();
            this.layers.push(layer);
            this.currentLayer = layer;
            this.currentLayerChanged();
        }
    }

    createNewLayer(selectNewLayer, name, addToBottom) {
        let layer = new Layer();
        if (name)
            layer.name = name;
        if (addToBottom)
            this.layers = [layer].concat(this.layers);
        else
            this.layers.push(layer);

        if (selectNewLayer) {
            this.selectLayerWithID(this.layers.length - 1);
            this.currentLayerChanged();
        }
        GUI.objectHierarchyChanged();

        return layer;
    }

    moveLayerDown(id) {
        if (id <= 0)
            return;

        let layer1 = this.layers[id-1];
        let layer2 = this.layers[id];

        this.layers[id - 1] = layer2;
        this.layers[id] = layer1;

        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    moveLayerUp(id) {
        if (id >= this.layers.length-1)
            return;

        let layer1 = this.layers[id + 1];
        let layer2 = this.layers[id];

        this.layers[id + 1] = layer2;
        this.layers[id] = layer1;

        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    deleteLayerWithID(id) {
        if (id < 0 || id >= this.layers.length)
            return;

        if (this.currentLayer)
            Application.sceneView.selection.clear();

        let layer = this.layers[id];

        this.selectNextVisibleLayer(id);
        Utilities.deleteArrayEntry(this.layers, layer);

        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    selectLayerWithID(id) {
        if (id < 0 || id >= this.layers.length)
            return;

        if (this.currentLayer)
            Application.sceneView.selection.clear();

        this.currentLayer = this.layers[id];
        this.layers[id].visible = true;

        this.currentLayerChanged();
        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    toggleVisibilityOfLayerWithID(id) {
        if (this.currentLayer) 
            Application.sceneView.selection.clear();

        this.layers[id].visible = !this.layers[id].visible;

        if (!this.layers[id].visible) {
            this.selectNextVisibleLayer(id);
        }

        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    toggleRenderableOfLayerWithID(id) {
        this.layers[id].hideInRenderPreview= !this.layers[id].hideInRenderPreview;
        GUI.objectHierarchyChanged();
        Application.redrawAllViews();
    }

    selectNextVisibleLayer(id) {
        if (this.currentLayer == this.layers[id]) {
            if (this.layers.length == 1) {
                this.createNewLayer(true);
                GUI.notify("No layer available. New layer has been created.");
            }
            else {
                let i = 1;
                let upperOut = false;
                let lowerOut = false;
                while (i <= this.layers.length) {

                    if (id + i < this.layers.length) {
                        if (this.layers[id + i].visible) {
                            this.currentLayer = this.layers[id + i];
                            break;
                        }

                    }
                    else
                        upperOut = true;

                    if (id - i >= 0) {
                        if (this.layers[id - i].visible) {
                            this.currentLayer = this.layers[id - i];
                            break;
                        }
                    }
                    else
                        lowerOut = true;


                    if (upperOut && lowerOut) {
                        this.createNewLayer(true);
                        GUI.notify("No layer available. New layer has been created.");
                        break;
                    }
                    ++i;
                }
            }
        }

        this.currentLayerChanged();
    }

    renameLayerWithID(id) {
        let name = prompt("New name for layer: ");

        if (name) {
            this.layers[id].name = name;
        }
        GUI.objectHierarchyChanged();
    }

    changeNameForLayerWithID(id, name) {
        this.layers[id].name = name;
    }

    // TODO called too often? no bugs, just curious
    updateStats() {
        let amountLines = 0;
        for (let layer of this.layers)
            amountLines += layer.data.lines.length;

        amountLines += Application.sceneView.selection.data.lines.length;
        amountLines += Application.sceneView.selection.data.partialLines.length;

        GUI.writeToStats("Lines in File", amountLines);


        let amountOfInstances = 0;
        for (let layer of this.layers)
            amountOfInstances += layer.data.instances.length;

        amountOfInstances += Application.sceneView.selection.data.instances.length;

        GUI.writeToStats("Instances in File", amountOfInstances);
    }

    addGuideLine(line) {
        let done = false;

        for (let layer of Application.file.layers) {
            if (layer.hideInRenderPreview) {
                layer.data.addLine(line);
                done = true;
                GUI.notify("Line added to available guide layer.");
                break;
            }
        }
        if (!done) {
            let layer = Application.file.createNewLayer(false, "Guides", true);
            layer.hideInRenderPreview = true;
            GUI.objectHierarchyChanged();

            layer.data.addLine(line);
            GUI.notify("Guide layer added.");
        }

        Application.file.changed();
    }

    getBounds(padding) {

        let min = new Vector2(Infinity, Infinity);
        let max = new Vector2(-Infinity, -Infinity);
        let maxLineWidth = 0;

        for (let layer of this.layers) {
            for (let line of layer.data.lines) {
                min.x = Math.min(min.x, line.start.position.x);
                min.y = Math.min(min.y, line.start.position.y);
                max.x = Math.max(max.x, line.start.position.x);
                max.y = Math.max(max.y, line.start.position.y);

                min.x = Math.min(min.x, line.end.position.x);
                min.y = Math.min(min.y, line.end.position.y);
                max.x = Math.max(max.x, line.end.position.x);
                max.y = Math.max(max.y, line.end.position.y);

                maxLineWidth = Math.max(maxLineWidth, line.thickness);
            }
        }
        for (let line of Application.sceneView.selection.data.lines) {
            min.x = Math.min(min.x, line.start.position.x);
            min.y = Math.min(min.y, line.start.position.y);
            max.x = Math.max(max.x, line.start.position.x);
            max.y = Math.max(max.y, line.start.position.y);

            min.x = Math.min(min.x, line.end.position.x);
            min.y = Math.min(min.y, line.end.position.y);
            max.x = Math.max(max.x, line.end.position.x);
            max.y = Math.max(max.y, line.end.position.y);

            maxLineWidth = Math.max(maxLineWidth, line.thickness);
        }
        for (let line of Application.sceneView.selection.data.partialLines) {
            min.x = Math.min(min.x, line.start.position.x);
            min.y = Math.min(min.y, line.start.position.y);
            max.x = Math.max(max.x, line.start.position.x);
            max.y = Math.max(max.y, line.start.position.y);

            min.x = Math.min(min.x, line.end.position.x);
            min.y = Math.min(min.y, line.end.position.y);
            max.x = Math.max(max.x, line.end.position.x);
            max.y = Math.max(max.y, line.end.position.y);

            maxLineWidth = Math.max(maxLineWidth, line.thickness);
        }
        maxLineWidth *= 0.5;

        if (padding != undefined)
            maxLineWidth = padding;

        return new Bounds(min.subtract(maxLineWidth), max.add(maxLineWidth));
    }

    currentLayerChanged() {
        Application.sceneView.data = this.currentLayer.data;
    }

    changed() {
        this.updateStats();
    }
}