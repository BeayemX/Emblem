﻿class Selection {
    constructor(view) {
        this.data = new SelectionData(this);
        this.view = view;
    }

    clear(dontTriggerChangedEvent) {
        this.view.data.lines = this.view.data.lines.concat(this.data.lines).concat(this.data.partialLines);

        for (let instance of this.data.instances)
            this.view.data.addInstance(instance);

        this.data.points = [];
        this.data.partialLines = [];
        this.data.lines = [];
        this.data.instances = [];

        if (!dontTriggerChangedEvent) { // when selecting everything this gets called later
            this.data._instanceSelectionChanged();
            this.view.data.changed();
            this.data.changed();
        }
    }

    selectEverything() {
        this.clear(true);
        this.data.lines = this.view.data.lines;
        this.data.instances = this.view.data.instances;

        this.view.data.lines = [];
        this.view.data.instances = [];

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    isEmpty() {
        return this.data.points.length == 0 && this.data.partialLines.length == 0 && this.data.lines.length == 0 && this.data.instances.length == 0;
    }

    invert() {
        let tmp = this.data.lines;
        this.data.lines = this.view.data.lines;
        this.view.data.lines = tmp;

        for (var i = 0; i < this.data.points.length; i++) {
            this.data.points[i] = this.data.points[i].opposite;
        }

        tmp = this.data.instances;
        this.data.instances = this.view.data.instances;
        this.view.data.instances = tmp;

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    deleteSelection() {
        this.data.lines = [];
        this.data.partialLines = [];

        // TODO PERFORMANCE maybe use slice here, becaus 'deleteArrayEntry iterates over whole array for every delete...
        for (let point of this.data.points)
            Utilities.deleteArrayEntry(this.view.data.lines, point.line);

        this.data.points = [];
        this.data.instances = [];

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    getAllSelectedPoints() {
        return Utilities.linesToLineEndings(this.data.lines).concat(this.data.points);
    }

    isPointSelected(point) {
        for (let p of this.data.points) {
            if (p == point) {
                return true;
            }
        }
        for (let l of this.data.lines) {
            if (point == l.start || point === l.end) {
                return true;
            }
        }

        return false;
    }

    changeSelectionForPoints(points) {
        for (let p of points) {
            if (this.isPointSelected(p))
                this.data.removePoint(p);
            else
                this.data.addPoint(p);
        }

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    selectPoints(points) {
        for (let point of points)
            this.data.addPoint(point);

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    deselectPoints(points) {
        for (let point of points)
            this.data.removePoint(point);

        this.data._instanceSelectionChanged();
        this.view.data.changed();
        this.data.changed();
    }

    getUnselectedPointsOfPartialLines() {
        let points = [];
        for (let p of this.data.points)
            points.push(p.opposite);

        return points;
    }

    duplicate() {
        let newSelData = this.data.copy();
        this.clear(true);
        this.data = newSelData;

        this.view.data.changed();

        return newSelData;
    }

    selectAllToggle() {
        if (this.isEmpty())
            this.selectEverything();
        else
            this.clear();
    }

    selectionHit() {
        for (let line of this.data.lines)
            if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range)
                return true;

        for (let p of this.data.points)
            if (Application.cursor.intersect(p))
                return true;

        for (let instance of this.data.instances)
            for (let line of instance.getAsLines())
                if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range)
            return true;

        return false;
    }

    snapToGrid() {
        Utilities.snapPointsToGrid(this.data.getAllPositions());
        Application.redrawAllViews();
    }

    merge() {
        Utilities.mergePoints(this.data.getAllPositions())
        Application.redrawAllViews();
    }

    moveBy(delta) {        
        let points = this.data.getAllPositions();
        
        if (delta.x != 0 || delta.y != 0) {
            ActionHistory.pushAction(new MoveAction(points, delta));
        }
    }

    rotate() {

    }


}