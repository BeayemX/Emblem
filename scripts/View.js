﻿class View {
    constructor(container) {
        // canvas
        this.canvas = document.createElement('canvas');
        this.container = container;

        if (container)
            container.appendChild(this.canvas);

        // canvas
        this.canvas.contentEditable = true; // to make canvas.focus() working

        this.canvas.addEventListener("mousemove", evt => this.mouseMove(evt));
        this.canvas.addEventListener("mouseup", evt => this.mouseUp(evt));
        this.canvas.addEventListener("mousedown", evt => this.mouseDown(evt));
        this.canvas.addEventListener("wheel", evt => this.mouseScroll(evt));

        this.canvas.addEventListener("mouseenter", evt => this.mouseEnter(evt));
        this.canvas.addEventListener("mouseleave", evt => this.mouseExit(evt));

        this.canvas.style.background = Settings.canvasColor;

        this.context = this.canvas.getContext('2d');
        this.camera = new Camera(this);

        // mouse
        this.oldPos = new Vector2(-1, -1);
        this.oldPosScreenSpace = Vector2.zero;
        this.processingWaitingState = false;
        this.mouseMovedAfterMouseDown = false;

        // settings
        this.isRenderPreviewing = false;

        // file and data stuff
        this.logic = new Logic();
        this.data = []; // currentlayer.data for sceneView. prefab for editView
        this.selection = new Selection(this);
    }

    mouseMove(e) {
        // TODO better way than this? maybe into Input-class and just call corresponding view?
        if (Application.currentView != this)
            return;

        let newPosScreenSpace = this._getMousePos(e);
        this.mouseMovedAfterMouseDown = true;
        let screenPosDelta = newPosScreenSpace.subtractVector(this.oldPosScreenSpace);

        if (this.logic.currentState instanceof ZoomState) {
            this.logic.currentState.update(newPosScreenSpace);
        }
        else if (this.logic.currentState instanceof PanState) {
            this.logic.currentState.update(screenPosDelta);
        } else {
            // calculate cursor position and delta and stuff
            Application.cursor.position = this.camera.screenSpaceToCanvasSpace(newPosScreenSpace.copy());

            this.oldPos = Application.cursor.currentPosition;

            // handle individual states
            if (this.logic.currentState instanceof ContinuousDrawingState) {
                this.logic.currentState.update();
            } else if (this.logic.currentState instanceof InputRecorderState) {
                this.logic.currentState.currentPos = Application.cursor.position.copy();
                this.logic.currentState.updateValue();
            }
            else if (this.logic.currentState instanceof BoxSelectionState) {
                if (this.logic.currentState.startPos) {
                    this.logic.currentState.currentPos = Application.cursor.position.copy();
                }
            } else if (this.logic.currentState instanceof BrushSelectionState) {
                this.logic.currentState.update();
            } else if (this.logic.currentState instanceof MeasureState) {
                this.logic.currentState.update();
            }

            if (this.logic.currentState instanceof IdleState ||
                this.logic.currentState instanceof DrawingState ||
                this.logic.currentState instanceof ContinuousDrawingState ||
                this.logic.currentState instanceof MeasureState) {
                GUI.writeToStatusbarLeft(Application.cursor.currentPosition.toString());
            }
        }

        //GUI.writeToStats("Application.currentView.camera.position", Application.currentView.camera.position.toString());
        this.oldPosScreenSpace = newPosScreenSpace;
        Application.redrawAllViews();
    }

    mouseUp(e) {
        if (Application.currentView != this)
            return;

        this._adjustMouseDownButtonBools(e, false);

        if (e.button == 0) // LMB
        {
            // process waiting state
            if (this.processingWaitingState) {
                this.processingWaitingState = false;

                this.logic.setWaitingState(null);

                if (this.logic.currentState instanceof InputRecorderState
                    || this.logic.currentState instanceof BoxSelectionState)
                    this.logic.currentState.execute();

                // TODO better way in javascript?
                // should just enter new State of waitingState-class
                if (this.logic.currentState instanceof MoveLinesState)
                    this.logic.setWaitingState(new MoveLinesState());
                else if (this.logic.currentState instanceof RotateState)
                    this.logic.setWaitingState(new RotateState());
                else if (this.logic.currentState instanceof ScaleState)
                    this.logic.setWaitingState(new ScaleState());
                else if (this.logic.currentState instanceof BoxSelectionState) {
                    this.logic.setWaitingState(new BoxSelectionState());
                }

                this.logic.setState(new IdleState());

            } else if (this.logic.currentState instanceof DrawingState) {
                if (Globals.drawPolyLine)
                    this.logic.setState(new DrawingState());
                else {
                    if (this.mouseMovedAfterMouseDown == false) {
                        if (Input.isKeyDown(Key.Alt))
                            Globals.pivot = new Vector2(0, 0);
                        else
                            Globals.pivot = Application.cursor.currentPosition;

                        if (Preferences.gridType == 2)
                            if (GridManager.grid.placePolarGridCenterAt2DCursor == true)
                                GridManager.grid.positionOffset = Globals.pivot.copy();

                    }
                    this.logic.setState(new IdleState());
                }
            }
            else if (this.logic.currentState instanceof BoxSelectionState) {
                this.logic.currentState.execute();
                this.logic.setState(new IdleState());
            }
            else if (this.logic.currentState instanceof ContinuousDrawingState) {
                this.logic.currentState.oldPos = undefined;
            } else if (this.logic.currentState instanceof MeasureState) {
                this.logic.currentState.mouseUp(0);
            }
        }

        else if (e.button == 1) // MMB
        {
            if (this.logic.currentState instanceof BoxSelectionState) {
                this.logic.currentState.execute();
                this.logic.setState(new IdleState());
            }
            else if (this.logic.currentState instanceof ZoomState)
                this.logic.setState(new IdleState());
            else if (this.logic.currentState instanceof PanState)
                this.logic.setState(new IdleState());
        }
        else if (e.button == 2) // RMB
        {
            if (this.logic.currentState instanceof MoveLinesState) // cancel grab
            {
                if (this.logic.currentState.initializedWithRMBDown)
                {
                    this.logic.currentState.execute();

                    if (this.mouseMovedAfterMouseDown == false) {
                        if (!Input.isKeyDown(Key.Shift)) {
                            this.selection.clear();
                            this._executeSelctionAtMousePosition();
                        }

                    }
                }

                this.logic.setState(new IdleState());
                Application.redrawAllViews();
            }
            else if (this.logic.currentState instanceof BoxSelectionState) // end box selection initialized with RMB
            {
                if (this.logic.currentState.initializedWithRMBDown) {
                    this.logic.currentState.execute();
                    this.logic.setState(new IdleState());
                }
            } else if (this.logic.currentState instanceof BrushSelectionState) // end brush selection initialized with RMB
            {
                if (this.logic.currentState.initializedWithRMBDown) {
                    this.logic.currentState.cancel();
                }
            } else if (this.logic.currentState instanceof MeasureState) {
                this.logic.currentState.mouseUp(2);
            }
        }
    }

    // abstract
    _preMouseDown(e) {

    }
    mouseDown(e) {
        if (Application.currentView != this)
            return;

        this._preMouseDown(e);

        this._adjustMouseDownButtonBools(e, true);
        this.mouseMovedAfterMouseDown = false;

        if (e.detail == 1) {
            if (e.button == 0) // LMB
            {
                // enter waiting state
                if (this.logic.currentState instanceof IdleState && this.logic.waitingState) {
                    this.processingWaitingState = true;
                    this.logic.useWaitingState();

                    if (this.logic.currentState instanceof BoxSelectionState)
                        this.logic.currentState.start(true)
                }
                    // application states
                else if (Application.logic.currentState instanceof PrefabBrushState) {
                    Application.logic.currentState.place(Input.isKeyDown(Key.Shift));
                }
                    // view states
                else if (this.logic.currentState instanceof IdleState) {
                    this.logic.setState(new DrawingState());
                }
                else if (this.logic.currentState instanceof InputRecorderState) {
                    this.logic.currentState.execute();
                    this.logic.setState(new IdleState());
                }
                else if (this.logic.currentState instanceof BoxSelectionState) {
                    this.logic.currentState.start(true);
                }
                else if (this.logic.currentState instanceof BrushSelectionState) {
                    this.logic.currentState.update();
                    Application.redrawAllViews();
                }
                else if (this.logic.currentState instanceof ContinuousDrawingState) {
                    this.logic.currentState.oldPos = Application.shouldSnap() ? Application.cursor.currentPosition : Application.cursor.position.copy();
                }
                else if (this.logic.currentState instanceof MeasureState) {
                    this.logic.currentState.mouseDown(0);
                }
            }
            else if (e.button == 2) // RMB
            {
                // application states

                if (Application.logic.currentState instanceof PrefabBrushState) {
                    Application.logic.currentState.cancel();
                }
                    // view states
                else if (this.logic.currentState instanceof IdleState) {
                    let clickedOnAlreadySelected = this.selection.selectionHit();

                    if (!Input.isKeyDown(Key.Shift) && !clickedOnAlreadySelected)
                        this.selection.clear();

                    if (Input.isKeyDown(Key.Shift) || !clickedOnAlreadySelected) {
                        this._executeSelctionAtMousePosition();
                    }
                    else {
                        // clicked and dragged on already selected, without shift pressed
                        this.logic.setState(new MoveLinesState());
                        this.logic.currentState.initializedWithRMBDown = true;
                    }

                }
                else if (this.logic.currentState instanceof BoxSelectionState) {
                    this.logic.currentState.cancel();
                }
                else if (this.logic.currentState instanceof DrawingState) {
                    this.logic.currentState.cancel();
                }
                else if (this.logic.currentState instanceof InputRecorderState) {
                    this.logic.currentState.cancel();
                }
                else if (this.logic.currentState instanceof BrushSelectionState) {
                    this.logic.currentState.cancel();
                }
                else if (this.logic.currentState instanceof MeasureState) {
                    this.logic.currentState.mouseDown(2);
                }

                Application.redrawAllViews();
            }
            else if (e.button == 1) // MMB
            {
                if (this.logic.currentState instanceof BoxSelectionState) {
                    this.logic.currentState.start(false);
                }
                else if (this.logic.currentState instanceof BrushSelectionState) {
                    this.logic.currentState.update();
                    Application.redrawAllViews();
                }
                else if (e.ctrlKey) {
                    this.logic.setState(new ZoomState());
                } else {
                    this.logic.setState(new PanState());
                }
            }
            else {
                console.log("Mouse button: \n"
                    + "button: " + e.button + "\n"
                    + "ctrlKey: " + e.ctrlKey + "\n"
                    + "altKey: " + e.altKey + "\n"
                    + "shiftKey: " + e.shiftKey + "\n"
                    );
            }
        }
        else if (e.detail == 2) {
            if (e.button == 0) { // LMB

            }
            else if (e.button == 1) {// MMB

            }
            else if (e.button == 2) {// RMB
                LineManipulator.growSelection(true);
            }

        }
        else if (e.detail == 3) {

            if (e.button == 2) {// RMB
                LineManipulator.selectLinked();
            }
        }

        e.preventDefault();
    }

    mouseScroll(e) {
        if (Application.currentView != this)
            return;

        if (e.ctrlKey) {
            let step = 1;
            let newThickness = Application.currentBrush.lineThickness;
            if (e.deltaY < 0)
                newThickness += step;
            else if (e.deltaY > 0)
                newThickness = Math.max(Application.currentBrush.lineThickness - step, 1);

            if (e.deltaY != 0)
                Application.increaseCurrentLineThickness(e.deltaY < 0 ? step : -step);
            else if (e.deltaX != 0)
                Application.increaseCurrentLineThickness(e.deltaX < 0 ? step : -step);
        }

        if (e.shiftKey) {
            let step = 1;
            if (e.deltaY < 0)
                Application.cursor.range += step;
            else if (e.deltaY > 0)
                Application.cursor.range = Math.max(Application.cursor.range - step, 1);
        }
        if (this.logic.currentState instanceof BrushSelectionState) {
            let step = 0.2;
            if (e.deltaY < 0)
                Application.cursor.range *= 1 + step;
            else if (e.deltaY > 0)
                Application.cursor.range *= 1 - step;
            //Application.cursor.range = Math.max(Application.cursor.range - step, 1);
        }
        else if (!e.shiftKey && !e.ctrlKey) {
            if (e.deltaY < 0) // upscroll
                this.camera.multiplyZoomBy(1.1, true);
            else if (e.deltaY > 0)
                this.camera.multiplyZoomBy(0.9, true);

            this.mouseMove(e);
        }

        Application.redrawAllViews();

        e.preventDefault();
    }


    mouseEnter(e) {
        //console.log(this._getMousePos(e));
        Application.setView(this);
        Application.focus(this);

    }

    mouseExit(e) {
        //console.log(this._getMousePos(e));
        Application.setView(null);
        Application.focus(null);
    }

    _getMousePos(e) {
        let rect = this.canvas.getBoundingClientRect();
        return new Vector2(
		    e.clientX - rect.left,
		    e.clientY - rect.top
	    );
    }

    activate() {
    }

    deactivate() {
        if (!(this.logic.currentState instanceof IdleState)) {
            if (this.logic.currentState.cancel)
                this.logic.currentState.cancel()
            this.logic.currentState.exit()
        }
    }

    _adjustMouseDownButtonBools(e, state) {
        if (e.button == 0)
            this.LMBDown = state;
        if (e.button == 1)
            this.MMBDown = state;
        if (e.button == 2)
            this.RMBDown = state;

        GUI.writeToStats("LMB down", this.LMBDown);
        GUI.writeToStats("MMB down", this.MMBDown);
        GUI.writeToStats("RMB down", this.RMBDown);
    }

    keyDown(keyCode) {
        switch (keyCode) {
            case Key.Space:
                if (this.logic.currentState instanceof IdleState) {
                    this.logic.setState(new MeasureState());
                    Application.redrawAllViews();
                }
                break;
            case Key.G:
                if (!this.selection.isEmpty()) {
                    if (Input.isKeyDown(Key.Alt))
                        LineManipulator.resetPosition();
                    else
                        this.logic.setState(new MoveLinesState());
                    Application.redrawAllViews();
                }
                break;
            case Key.R:
                if (!this.selection.isEmpty()) {
                    if (Input.isKeyDown(Key.Shift))
                        LineManipulator.rotateSelectionBy(90);
                    else if (Input.isKeyDown(Key.Control))
                        LineManipulator.rotateSelectionBy(-90);
                    else if (Input.isKeyDown(Key.Alt))
                        LineManipulator.resetRotation();
                    else
                        this.logic.setState(new RotateState());
                    Application.redrawAllViews();
                }
                break;

            case Key.S:
                if (!Input.isKeyDown(Key.Control) && !Input.isKeyDown(Key.Shift))
                    if (!this.selection.isEmpty()) {
                        if (Input.isKeyDown(Key.Alt))
                            LineManipulator.resetScale();
                        else
                            this.logic.setState(new ScaleState());
                        Application.redrawAllViews();
                    }
                break;

            case Key.D:
                if (Input.isKeyDown(Key.Shift)) {
                    if (this.logic.currentState instanceof IdleState || this.logic.currentState instanceof MoveLinesState) {
                        if (!this.selection.isEmpty()) {
                            this.selection.duplicate();
                            this.logic.setState(new MoveLinesState());
                        }
                    }
                }
                else {
                    if (this.logic.currentState instanceof IdleState) {
                        this.logic.setState(new ContinuousDrawingState());
                    }
                }
                break;

            case Key.Tab:
                if (!this.logic.isPreviewing()) {
                    this.isRenderPreviewing = true;
                    this.renderer.setQualitySetting("quality");
                    this.renderer.redraw();
                }
                break;

            case Key.K:
                if (this.logic.currentState instanceof IdleState)
                    Utilities.cutWithSelection();
                break;

            case Key.Escape:
                this.logic._clearStates();
                break;

            case Key.Control:
                Application.redrawAllViews();
                break;
            case Key.E:
                Utilities.extrudeSelection();
                break;
        }
    }

    keyUp(keyCode) {
        switch (keyCode) {
            case Key.Space:
                if (this.logic.currentState instanceof MeasureState)
                    this.logic.setState(new IdleState());
                break;

            case Key.Tab:
                if (this.logic.isPreviewing()) {
                    this.isRenderPreviewing = false;
                    this.renderer.setQualitySetting(Preferences.qualitySetting);
                    this.renderer.redraw();
                }
                break;

            case Key.Shift:
                if (this.logic.currentState instanceof DrawingState) {
                    if (Globals.drawPolyLine) {
                        this.logic.currentState.cancel();
                        Application.redrawAllViews();
                    }
                }
                Globals.drawPolyLine = false;
                break;

            case Key.D:
                if (this.logic.currentState instanceof ContinuousDrawingState) {
                    if (!this.logic.currentState.used && !this.selection.isEmpty()) {
                        this.selection.duplicate();
                        this.logic.setState(new MoveLinesState());
                    }
                    else
                        this.logic.setState(this.logic.previousState);
                }
                break;

            case Key.Control:
                Application.redrawAllViews();
                break;
        }
    }

    zoomToBounds(margin) {
        let size = Preferences.artBounds.size;
        let x = this.canvas.width / size.x;
        let y = this.canvas.height / size.y;

        this.camera.setZoom(Math.min(x, y), false);

        // margin
        if (margin)
            this.camera.zoomBy(margin * this.camera.zoom, false);

        // TODO maybe better way to do it? with less calls?
        this.camera.position = (new Vector2(this.canvas.width * 0.5, this.canvas.height * 0.5)).divide(this.camera.zoom).subtractVector(Preferences.artBounds.origin.addVector(Preferences.artBounds.size.multiply(0.5)));
        this.renderer.redraw();
    }

    _fitCanvasToContainer() {
        // make canvas visually fill the positioned parent
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';
        // set the internal size to match
        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;
    }

    toString() {
        console.log("toString() not implemented for view!")
    }

    growSelection(redraw) {
        let selectedPoints = this.selection.getAllSelectedPoints();
        let allSelectedPoints = [];

        for (let i = 0; i < selectedPoints.length; ++i)
            allSelectedPoints = allSelectedPoints.concat(this.getAllLineEndingsAt(selectedPoints[i].position));

        for (let i = 0; i < allSelectedPoints.length; ++i)
            this.selection.data.addPoint(allSelectedPoints[i]);

        for (let i = 0; i < allSelectedPoints.length; ++i) {
            let p = allSelectedPoints[i].opposite;
            let pArray = this.getAllLineEndingsAt(p.position);

            for (let j = 0; j < pArray.length; ++j)
                this.selection.data.addPoint(pArray[j]);
        }

        if (redraw)
            Application.redrawAllViews();
    }

    // TODO improve. use sth like a visited-list like a* or sth...
    // to avoid max depth reached
    selectLinked() {
        let selPointsNumOld = 0;
        let maxIterations = 30;

        for (var i = 0; i < maxIterations; i++) {
            this.growSelection(false);

            let selPointsNum = this.selection.getAllSelectedPoints().length;

            if (selPointsNumOld == selPointsNum)
                break;
            else
                selPointsNumOld = selPointsNum;
        }
        if (i == maxIterations)
            GUI.notify("Max Iteration Depth reached!");

        Application.redrawAllViews();
    }


    // TODO extract one part into data class? and other into selection-data?
    getAllLineEndingsAt(position) {
        let points = [];
        let lines = this.data.lines.concat(this.selection.data.partialLines);

        for (let i = 0; i < lines.length; ++i) {
            if (lines[i].start.position.equals(position))
                points.push(lines[i].start);
            if (lines[i].end.position.equals(position))
                points.push(lines[i].end);
        }
        return points;
    }

    _executeSelctionAtMousePosition() {
        let lines = this.data.lines.concat(this.selection.data.lines).concat(this.selection.data.partialLines);
        let pointsToChangeSelection = [];
        let instancesToChangeSelection = [];

        if (Preferences.selectVertices && Preferences.selectEdges) {
            let s, e, m, _m;
            for (let line of lines) {
                s = Application.cursor.intersect(line.start);
                e = Application.cursor.intersect(line.end);
                _m = line.start.position.addVector(line.end.position);
                _m = _m.divide(2);
                _m = new LineEnding(_m.x, _m.y, line);
                m = Application.cursor.intersect(_m);

                if (s && !m && !e)
                    pointsToChangeSelection.push(line.start);
                else if (!s && !m && e)
                    pointsToChangeSelection.push(line.end);
                else if (m) { // if one line ending and the middle of the line is within cursor range select whole line. otherwise weird selection happens when whole line is inside cursor range
                    pointsToChangeSelection.push(line.start);
                    pointsToChangeSelection.push(line.end);
                } else if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range + (line.thickness * 0.5)) {
                    if (Application.cursor.withinTimesRange(line.start, 3))
                        pointsToChangeSelection.push(line.start);
                    else if (Application.cursor.withinTimesRange(line.end, 3))
                        pointsToChangeSelection.push(line.end);
                    else {
                        pointsToChangeSelection.push(line.start);
                        pointsToChangeSelection.push(line.end);
                    }
                }
            }
            // check prefab instances
            // TODO unify with code above?
            //*
            for (let instance of this.data.instances) {
                if (instance.selectionHit())
                    instancesToChangeSelection.push(instance);
            }
            for (let instance of this.selection.data.instances) {
                if (instance.selectionHit())
                    instancesToChangeSelection.push(instance);
            }
            // */
        } else if (Preferences.selectEdges) {
            for (let i = 0; i < lines.length; i++) {
                if (lines[i].distanceToPoint(Application.cursor.position) <= Application.cursor.range) {
                    pointsToChangeSelection.push(lines[i].start);
                    pointsToChangeSelection.push(lines[i].end);
                }
            }
        } else if (Preferences.selectVertices) {
            for (let p of Utilities.linesToLineEndings(lines)) {
                if (Application.cursor.intersect(p))
                    pointsToChangeSelection.push(p);
            }
        }

        // actually change selection
        this.selection.changeSelectionForPoints(pointsToChangeSelection);

        for (let instance of instancesToChangeSelection) {
            if (this.selection.data.instances.includes(instance)) {
                this.data.addInstance(instance);
                this.selection.data.removeInstance(instance);
            } else {
                this.selection.data.addInstance(instance);
                this.data.removeInstance(instance);
            }
        }


        if (pointsToChangeSelection.length > 0 || instancesToChangeSelection.length > 0) {
            this.logic.setState(new MoveLinesState());
            this.logic.currentState.initializedWithRMBDown = true;
        }
        else {
            if (Preferences.rmbSelectionType == "box") {
                this.logic.setState(new BoxSelectionState());
                this.logic.currentState.initializedWithRMBDown = true;
                this.logic.currentState.start(true);
            } else if (Preferences.rmbSelectionType == "brush") {
                this.logic.setState(new BrushSelectionState());
                this.logic.currentState.initializedWithRMBDown = true;
            }
        }

    }
}
