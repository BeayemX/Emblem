﻿Globals = new class {
    constructor() {
        // TODO pivot for each view its own pivot?
        // TODO would pivot-class make sense (placing history or sth?)
        this.pivot = Vector2.zero;
        this.drawPolyLine = false;

        this.mirrorX = false;
        this.mirrorY = false;
    }
}