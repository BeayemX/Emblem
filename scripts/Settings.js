// TODO check which variables are not used and remove them
Settings = new class {
    constructor() {
        this.emblemRed = new Color(124, 0, 0, 1);//'#7c0000';
        
        this.artBoundsLineWidth = 2;
        this.artBoundsStroke = '#000';
        this.artBoundsFill = '#777';

        this.canvasColor = "#666";

        this.crossHairColor = "#f90";
        this.crossHairLineWidth = 1;
        

        this.selectionColor = "#f90"; 
        this.selectionColorFill = 'rgba(255, 127, 0, 1)'; // gradient
        this.boxSelectionColor = 'rgba(255, 127, 0, 0.5)';

        this.guideDashSize = 10;

        this.prefabColor = 'green';
        this.prefabSelectionColor = 'lightgreen';
        this.prefabOutlineSize = 3;

        this.measureDashSize = 20;

        this.notificationTimeout = 4000;
    }
}
