﻿class TriangleGrid {
    constructor() {
        this.width = 32;
        this.height = 32;
        this.uniform = true;
        this.swapXAndY = false;

        this.thickness = 0.33;
        this.color = '#444';
        this.bigGridColor = '#222';
        this.bigGridSteps = 4;

        if (this.uniform)
            this.uniformHeight();
    }

    uniformHeight() {
        this.height = this.width / Math.cos(30 * (Math.PI / 180));
    }

    getLines(view) {
        let bounds = view.camera.getVisibleBounds();
        let min = new Vector2(bounds.left, bounds.top);
        let max = new Vector2(bounds.right, bounds.bottom);
        let minMax = new Vector2(bounds.left, bounds.bottom);
        let maxMin = new Vector2(bounds.right, bounds.top);
        let snappedMin = this.getNearestPointFor(min);
        let snappedMax = this.getNearestPointFor(max);
        let snappedMinMax = this.getNearestPointFor(minMax);
        let snappedMaxMin = this.getNearestPointFor(maxMin);
        let k;
        let startD;
        let endD;

        let lines = [];

        if (this.swapXAndY) {
            k = 2 * this.width / this.height;
            startD = snappedMaxMin.y - k * snappedMaxMin.x;
            endD = snappedMinMax.y - k * snappedMinMax.x;
            for (let d = startD; d <= endD; d += 2 * this.width) {
                let ly = k * min.x + d;
                let ry = k * max.x + d;
                if (ly < max.y && ry > min.y) {
                    let start = (ly < min.y) ? new Vector2((min.y - d) / k, min.y) : new Vector2(min.x, ly);
                    let end = (ry > max.y) ? new Vector2((max.y - d) / k, max.y) : new Vector2(max.x, ry);
                    lines.push(new Line(start, end, this.color, this.thickness / view.camera.zoom));
                }
            }
            k = -k;
            startD = snappedMin.y - k * snappedMin.x;
            endD = snappedMax.y - k * snappedMax.x;
            for (let d = startD; d <= endD; d += 2 * this.width) {
                let ly = k * min.x + d;
                let ry = k * max.x + d;
                if (ly > min.y && ry < max.y) {
                    let start = (ly > max.y) ? new Vector2((max.y - d) / k, max.y) : new Vector2(min.x, ly);
                    let end = (ry < min.y) ? new Vector2((min.y - d) / k, min.y) : new Vector2(max.x, ry);
                    lines.push(new Line(start, end, this.color, this.thickness / view.camera.zoom));
                }
            }
            for (let y = snappedMin.y < min.y ? snappedMin.y + this.width : snappedMin.y; y <= max.y; y += this.width)
                lines.push(new Line(new Vector2(min.x, y), new Vector2(max.x, y), this.color, this.thickness / view.camera.zoom));
        }
        else {
            k = 0.5 * this.height / this.width;
            startD = snappedMaxMin.y - k * snappedMaxMin.x;
            endD = snappedMinMax.y - k * snappedMinMax.x;
            for (let d = startD; d <= endD; d += this.height) {
                let ly = k * min.x + d;
                let ry = k * max.x + d;
                if (ly < max.y && ry > min.y) {
                    let start = (ly < min.y) ? new Vector2((min.y - d) / k, min.y) : new Vector2(min.x, ly);
                    let end = (ry > max.y) ? new Vector2((max.y - d) / k, max.y) : new Vector2(max.x, ry);
                    lines.push(new Line(start, end, this.color, this.thickness / view.camera.zoom));
                }
            }
            k = -k;
            startD = snappedMin.y - k * snappedMin.x;
            endD = snappedMax.y - k * snappedMax.x;
            for (let d = startD; d <= endD; d += this.height) {
                let ly = k * min.x + d;
                let ry = k * max.x + d;
                if (ly > min.y && ry < max.y) {
                    let start = (ly > max.y) ? new Vector2((max.y - d) / k, max.y) : new Vector2(min.x, ly);
                    let end = (ry < min.y) ? new Vector2((min.y - d) / k, min.y) : new Vector2(max.x, ry);
                    lines.push(new Line(start, end, this.color, this.thickness / view.camera.zoom));
                }
            }
            for (let x = snappedMin.x < min.x ? snappedMin.x + this.width : snappedMin.x; x <= max.x; x += this.width)
                lines.push(new Line(new Vector2(x, min.y), new Vector2(x, max.y), this.color, this.thickness / view.camera.zoom));
        }

        return lines;
    }

    getNearestPointFor(position) {
        if (this.swapXAndY)
            position = new Vector2(position.y, position.x);
        let intX = Math.round(position.x / this.width);
        let snapX = intX * this.width;
        let snapY = intX % 2 == 0 ?
            Math.round(position.y / this.height) * this.height :
            (Math.round(position.y / this.height + 0.5) - 0.5) * this.height;

        let firstCandidate = new Vector2(snapX, snapY);
        let secondCandidate = new Vector2(snapX + this.width * (snapX < position.x ? 1 : -1), snapY + this.height * (snapY < position.y ? 0.5 : -0.5));
        let firstDistance = (firstCandidate.subtractVector(position)).sqrMagnitude();
        let secondDistance = (secondCandidate.subtractVector(position)).sqrMagnitude();
        let chosenOne = firstDistance < secondDistance ? firstCandidate : secondCandidate;
        if (this.swapXAndY)
            chosenOne = new Vector2(chosenOne.y, chosenOne.x);
        return chosenOne;
    }
}