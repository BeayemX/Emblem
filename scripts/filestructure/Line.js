﻿class Line {
    constructor(x1, y1, x2, y2, color, _thickness) {
        if (x1 instanceof Vector2) {
            this.start = new LineEnding(x1.x, x1.y, this);
            this.end = new LineEnding(y1.x, y1.y, this);

            this.color = (x2) ? x2: Application.currentBrush.lineColor.copy();
            this._thickness = (y2) ? y2: Application.currentBrush.lineThickness;
        }
        else {
            this.start = new LineEnding(x1, y1, this);
            this.end = new LineEnding(x2, y2, this);

            this.color = (color) ? color : Application.currentBrush.lineColor.copy();
            this._thickness = (_thickness) ? _thickness : Application.currentBrush.lineThickness;
        }
    }

    get thickness() {
        return this._thickness;
    }

    set thickness(value) {
        this._thickness = Math.max(1, value);
    }

    static overlapping(line1, line2) {
        return line1.start.position.equals(line2.start.position)
        && line1.end.position.equals(line2.end.position)
        ||
        line1.start.position.equals(line2.end.position)
        && line1.end.position.equals(line2.start.position);
    }

    distanceToPoint(point) {
        let se = this.end.position.subtractVector(this.start.position);
        let sp = point.subtractVector(this.start.position);
        let ep = point.subtractVector(this.end.position);

        if (Vector2.dot(se, sp) <= 0)
            return sp.magnitude();
        if (Vector2.dot(se.flipped(), ep) <= 0)
            return ep.magnitude();
        return Math.abs((se.x * sp.y - se.y * sp.x) / se.magnitude());
    }

    equals(other)
    {
        return Line.overlapping(this, other) && this.color.equals(other.color) && this._thickness == other._thickness;
    }

    setEnd(p) {
        this.end = new LineEnding(p.x, p.y, this);
    }

    opposite(point) {
        if (point == this.start)
            return this.end;
        else
            return this.start;
    }
    copy()
    {
        return new Line(this.start.position.x, this.start.position.y, this.end.position.x, this.end.position.y, this.color, this._thickness);
    }

    move(delta)
    {
        this.start.position = this.start.position.addVector(delta);
        this.end.position = this.end.position.addVector(delta);
        return this;
    }
    rotateAround(center, degree) {
        this.start.position = this.start.position.rotateAround(center, degree);
        this.end.position = this.end.position.rotateAround(center, degree);
        return this;
    }

    scaleFrom(center, scaleVector) {
        this.start.position = this.start.position.scaleFrom(center, scaleVector);
        this.end.position = this.end.position.scaleFrom(center, scaleVector);
        return this;
    }

    toString() {
        return "Line from " + this.start.position + " to " + this.end.position;
    }
}