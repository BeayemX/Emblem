﻿class Data {
    constructor() {
        this._lines = [];
        this._instances = [];
    }

    get lines() {
        return this._lines;
    }

    set lines(lines) {
        this._lines = lines;
    }

    get instances() {
        return this._instances;
    }

    set instances(instances) {
        this._instances = instances;
    }

    addLine(line) {
        if (Application.tmpCutLines()) {
            Application.currentView.selection.clear(true);
            Utilities.cutLines(line, this._lines, false);
        }
        else if (Preferences.cutLines) {
            Application.currentView.selection.clear(true);
            Utilities.cutLines(line, this._lines, true);
        }
        else {
            this._lines.push(line);
        }
    }

    addLines(lines) {
        this._lines = this._lines.concat(lines);
    }

    addInstance(instance) {
        this._instances.push(instance);
    }

    removeInstance(instance) {
        for (var i = 0; i < this._instances.length; ++i) {
            if (this._instances[i] == instance) {
                this._instances.splice(i, 1);
                return;
            }
        }
    }

    removeLine(line) {
        for (var i = 0; i < this._lines.length; ++i) {
            if (this._lines[i] == line) {
                this._lines.splice(i, 1);
                return;
            }
        }
    }

    copy() {
        let dataCopy = new Data();

        for (let line of this._lines)
            dataCopy._lines.push(line.copy());

        for (let instance of this._instances)
            dataCopy._instances.push(instance.copy());

        return dataCopy();
    }

    changed() {
        this.cleanUp();
        Application.file.updateStats();
    }

    cleanUp() {
        let deletedLinesCounter = 0;
        let deletedInstanceCounter = 0;

        // lines with length 0
        for (let i = this._lines.length - 1; i >= 0; --i) {
            if (this._lines[i].start.position.x == this._lines[i].end.position.x
                && this._lines[i].start.position.y == this._lines[i].end.position.y) {
                this.removeLine(this._lines[i]);
                ++deletedLinesCounter;
            }
        }

        // overlapping lines
        for (let i = this._lines.length - 1; i >= 0; --i) {
            for (let j = this._lines.length - 1; j > i; --j) {
                if (Line.overlapping(this._lines[i], this._lines[j])) {
                    this.removeLine(this._lines[j]);
                    ++deletedLinesCounter;
                    continue;
                }
            }
        }

        // instances
        for (let i = this._instances.length - 1; i >= 0; --i) {
            for (let j = this._instances.length - 1; j > i; --j) {
                if (this._instances[i].equals(this._instances[j])) {
                    this.removeInstance(this._instances[j])
                    ++deletedInstanceCounter;
                }
            }
        }

        // if sth has been cleaned up
        if (deletedLinesCounter > 0 || deletedInstanceCounter > 0)
        {
            let text = "Cleaned up ";

            if (deletedLinesCounter == 1)
                text += "1 line";
            else if (deletedLinesCounter > 0)
                text += deletedLinesCounter + " lines";

            if (deletedInstanceCounter > 0){
                if (deletedLinesCounter > 0)
                    text += " and ";

                if (deletedInstanceCounter == 1)
                    text += "1 instance";
                else if (deletedInstanceCounter > 0)
                    text += deletedInstanceCounter + " instances";

            }

            text += ".";

            GUI.notify(text);
        }

    }
}