﻿class ScaleState extends InputRecorderState {
    constructor() {
        super();
    }

    enter() {
        this.startPos = Application.cursor.position.copy();
        this.currentPos = Application.cursor.position.copy();
        //this.updateValue();
    }

    exit() {
    }

    updateValue(number, axisLock) {
        let scale = this.getScaleVector();

        let text = "Scaling: " + (scale.toString());
        if (Application.currentView.logic.currentState.axisLock)
            text += " | Lock: " + Application.currentView.logic.currentState.axisLock;
        GUI.writeToStatusbarLeft(text);

        Application.redrawAllViews();
    }

    execute() {
        LineManipulator.scaleSelection(this.getScaleVector());
    }

    cancel() {
        Application.currentView.logic.setState(new IdleState());
        Application.redrawAllViews();
    }

    getScaleFromMousePosition() {
        // TODO PERFORMANCE calculate center is calculated on every mouse update. maybe save somewhere?
        let center;
        if (Preferences.usePivot)
            center = Globals.pivot;
        else
            center = Utilities.calculateBBCenter(Application.currentView.selection.data.getAllPositions());

        let p1 = Application.currentView.logic.currentState.startPos.subtractVector(center);
        let p2 = Application.currentView.logic.currentState.currentPos.subtractVector(center);

        let value =
            (p2.sqrMagnitude() /  // this
            p1.sqrMagnitude());  // this


        if (Input.isKeyDown(Key.Shift)) {
            let snapFactor = 0.2;
            value = (Math.floor(value / snapFactor)) * snapFactor;
        } else if (Input.isKeyDown(Key.Control)) {
            let snapFactor = 1;
            value = (Math.floor(value / snapFactor)) * snapFactor;
        }

        return value;
    }

    getScaleVector() {
        let scale = (isNaN(this.number)) ?
            Application.currentView.logic.currentState.getScaleFromMousePosition()
            :
            this.number;

        let delta = new Vector2(scale, scale);

        if (this.axisLock == "X")
            delta.y = 1;
        else if (this.axisLock == "Y")
            delta.x = 1;

        return delta;
    }

    keyDown(keycode) {
        super.record(keycode);

        switch (keycode) {
            case Key.D:
                LineManipulator.scaleSelection(this.getScaleVector());
                Application.currentView.selection.duplicate();
                LineManipulator.scaleSelection(this.getScaleVector().oneDividedByThisVector());
                break;
            case Key.K:
                LineManipulator.scaleSelection(this.getScaleVector());
                Utilities.cutWithSelection();
                LineManipulator.scaleSelection(this.getScaleVector().oneDividedByThisVector());
                break;
        }
    }

    keyUp(keycode) {

    }

    toString() {
        return "ScaleState";
    }
}