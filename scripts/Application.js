﻿class App {
    constructor() {
        console.log("Application.constructor()");
        this.currentState; // Drawing, EditingPrefab
        this.currentFocus;
        this._lastView;
        this._currentView;

        this.cursor = new Cursor();
        this.currentBrush = new Brush();

        // logic
        this.logic = new Logic();

        // views
        this.sceneView;
        this.prefabEditorView;
        this.renderPreview;

        this.clipBoard;
    }

    start() {
        this.sceneView = new SceneView(sceneView);
        this._lastView = this.sceneView;
        this.prefabEditorView = new PrefabEditorView(prefabEditor);
        this.renderPreview = new FinalRenderView(renderPreview);

        this.initializeClasses();

        this.layoutGUI();

        window.addEventListener("keydown", evt => Input.keyDown(evt), false);
        window.addEventListener("keyup", evt => Input.keyUp(evt), false);

        window.addEventListener('resize', evt => this.layoutGUI(evt), false);


        this.start2();


        // center cameras of views
        this.sceneView.camera.resetView();
        this.prefabEditorView.camera.resetView();

        this.redrawAllViews();
    }

    start2() {
        window.addEventListener("contextmenu", function (e) { e.preventDefault(); return false; });
        window.addEventListener('focus', function () {
            Input.clearKeys();
            if (Preferences.developerMode)
                GUI.notify("received focus");
        }, false);
        window.onbeforeunload = function () {
            if (Input.isKeyDown(Key.Control))
                localStorage.removeItem(Saver.autosaveFileName);
            else
                Saver.autoSave();
            Preferences.savePreferences();
        };

        window.onerror = function () {
            if (Preferences.developerMode == true)
                GUI.notify("<font color='red'>An error occured! Press F12 to see what went wrong.</font>");
            //Application.currentView.logic._clearStates();
        };

        offscreenCanvas = document.getElementById('offscreenCanvas');
        offscreenContext = offscreenCanvas.getContext('2d');

        toolbar = document.getElementById('toolbar');

        // Setup the dnd listeners.
        let dropZone = document.body;
        dropZone.addEventListener('dragover', Saver.handleDragOver, false);
        dropZone.addEventListener('drop', Saver.handleFileSelect, false);

        Saver.loadAutoSave();

        this.file.updateStats();

        for (var i = 0; i < waitingForStart.length; i++) {
            waitingForStart[i].start();
        }

        //ActionHistory.writeHistory();

        $(".foldingHeader").each(function () {
            $(this).mousedown(function () {
                $(this).next().slideToggle();
            })
        });
    }

    adjustRenderPreview() {
        let max = Math.max(Preferences.artBounds.width, Preferences.artBounds.height);
        renderPreview.style.width = Preferences.artBounds.width / max * 300 + "px";
        renderPreview.style.height = Preferences.artBounds.height / max * 300 + "px";
        this.renderPreview._fitCanvasToContainer();
        this.renderPreview.zoomToBounds();
    }

    initializeClasses() {
        GridManager.init();
        GUI.init();
        Preferences.init();

        Input.init();
        Saver.init();

        ActionHistory.init();
        PrefabManager.init();

    }

    layoutGUI() {
        let topLeft = Vector2.zero;
        let size = Vector2.zero;

        size.x = window.innerWidth - leftarea.offsetWidth - rightarea.offsetWidth -prefabarea.offsetWidth;
        size.y = window.innerHeight - GUI.menubar.offsetHeight - GUI.statusbar.offsetHeight;

        topLeft.x = leftarea.offsetWidth + prefabarea.offsetWidth

        this.sceneView.canvas.style.left = topLeft.x;
        this.sceneView.canvas.width = size.x;
        this.sceneView.canvas.height = size.y;

        this.prefabEditorView._fitCanvasToContainer();
        this.renderPreview._fitCanvasToContainer();

        /*
        toolbar.style.left = canvas.style.left;
        toolbar.style.width = canvas.width;
        toolbar.style.top = GUI.menubar.offsetHeight;
        */

        // TODO magic
        GUI.stats.style.right = 280;
        GUI.stats.style.top = GUI.menubar.offsetHeight + 25;

        this.redrawAllViews();
    }

    focus(focusable) {
        this.currentFocus = focusable;
        if (this.currentFocus)
            GUI.writeToStats("currentFocus", this.currentFocus.toString());
        else 
            GUI.writeToStats("currentFocus", "null");
    }

    setView(view) {
        // deactivate current view if there is one
        if (this._currentView)
            this._currentView.deactivate();

        // if no current view, store last one
        if (view == null) {
            this._lastView = this._currentView;
            this._currentView = null;
            return;
        }

        // set and activate new view
        this._currentView = view;
        this._currentView.activate();
        this._currentView.canvas.focus();

        GUI.writeToStats("currentView", this._currentView.toString());
    }

    get currentView() {
        let view;
        if (this._currentView)
            view = this._currentView;
        else
            view = this._lastView;

        /*
        if (view == this.prefabEditorView && !this.prefabEditorView.prefab)
            view = this.sceneView;
        */
        return view;
    }

    redrawAllViews() {
        this.sceneView.renderer.redraw();
        this.prefabEditorView.renderer.redraw();
        this.renderPreview.renderer.redraw();
    }

    setQualitySetting(setting) {
        this.sceneView.renderer.setQualitySetting(setting);
        this.prefabEditorView.renderer.setQualitySetting(setting);
    }

    shouldSnap() {
        return (Preferences.snapToGrid && !this.tmpSwitchSnapToGrid()) || (!Preferences.snapToGrid && this.tmpSwitchSnapToGrid());
    }

    tmpCutLines() {
        return Input.isKeyDown(Key.Alt);
    }

    tmpSwitchSnapToGrid() {
        return Input.isKeyDown(Key.Control);
    }

    toggleGridVisiblity(senderButton) {
        Preferences.showGrid = !Preferences.showGrid;
        senderButton.innerHTML = Preferences.showGrid ? "Hide grid" : "Show grid";
        Application.redrawAllViews();
    }

    // TODO move to Brush class?
    setCurrentLineAlpha(alpha) {
        Application.currentBrush.lineColor.a = alpha;
        this.useCurrentColorForSelection();
    }

    // TODO move to Brush class?
    setCurrentLineColor(color) {
        let alpha = Application.currentBrush.lineColor.a;
        Application.currentBrush.lineColor = color;
        Application.currentBrush.lineColor.a = alpha;

        GUI.lineColor.value = color.toHexString();
        this.useCurrentColorForSelection();
    }

    selectionChanged(selection) {
        let color;
        let thickness;
        let alpha;

        let notInitialized = true;

        for (let line of selection.data.lines.concat(selection.data.partialLines)) {

            if (notInitialized == true) {
                color = line.color.copy();
                thickness = line.thickness;
                alpha = line.color.a;

                notInitialized = false;
            }
            else {
                if (color != undefined) {
                    if (!(color.equals(line.color)))
                        color = undefined;
                }
                if (thickness != line.thickness)
                    thickness = undefined;
                if (alpha != line.color.a)
                    alpha = undefined;
            }
        }


        if (color != undefined)
            Application.currentBrush.lineColor = color.copy();
        GUI.lineColor.value = Application.currentBrush.lineColor.toHexString();

        if (thickness != undefined)
            Application.currentBrush.lineThickness = thickness;

        GUI.lineThickness.value = Application.currentBrush.lineThickness;

        if (alpha != undefined)
            Application.currentBrush.lineColor.a = alpha;

        GUI.lineAlpha.value = Application.currentBrush.lineColor.a;
    }

    increaseCurrentLineThickness(delta) {
        for (let line of this.currentView.selection.data.lines.concat(this.currentView.selection.data.partialLines))
            line.thickness += delta;

        Application.currentBrush.lineThickness = Math.max(1, Application.currentBrush.lineThickness + delta);

        GUI.lineThickness.value = Application.currentBrush.lineThickness;
        Application.redrawAllViews();
    }

    setCurrentLineThickness(thickness) {
        Application.currentBrush.lineThickness = thickness;

        for (let line of this.currentView.selection.data.lines.concat(this.currentView.selection.data.partialLines))
            line.thickness = thickness;

        GUI.lineThickness.value = thickness;
        Application.redrawAllViews();
    }

    useCurrentColorForSelection() {
        for (let line of this.currentView.selection.data.lines.concat(this.currentView.selection.data.partialLines))
            line.color = Application.currentBrush.lineColor.copy();

        Application.redrawAllViews();
    }

    clearAllSelections() {
        this.sceneView.selection.clear();
        this.prefabEditorView.selection.clear();
    }
}