﻿class RectangleGrid {
    constructor() {
        this.width = 32;
        this.height = 32;

        this.thickness = 0.33;
        this.gridLineColor = '#444';

        this.bigGridLineColor = '#333';
        this.bigGridNumber = 4;

        this.axisThickness = 1;
    }

    getNearestPointFor(p) {
        return new Vector2(
            Math.round(p.x / this.width) * this.width,
            Math.round(p.y / this.height) * this.height);
    }

    getLines(view) {
        let bounds = view.camera.getVisibleBounds();
        let min = new Vector2(bounds.left, bounds.top);
        let max = new Vector2(bounds.right, bounds.bottom);
        let sMin = this.getNearestPointFor(min);
        let sMax = this.getNearestPointFor(max);
        let startX = sMin.x < min.x ? sMin.x + this.width : sMin.x;
        let startY = sMin.y < min.y ? sMin.y + this.height : sMin.y;

        let axisLines = [];
        let gridLines = [];
        let bigGridLines = [];

        for (let x = startX; x <= max.x; x += this.width)
            if (x == 0)
                axisLines.push(new Line(new Vector2(x, min.y), new Vector2(x, max.y), Settings.emblemRed, this.axisThickness / view.camera.zoom));
            else if (x % (this.width * this.bigGridNumber) != 0)
                gridLines.push(new Line(new Vector2(x, min.y), new Vector2(x, max.y), this.gridLineColor, this.thickness / view.camera.zoom));
            else
                bigGridLines.push(new Line(new Vector2(x, min.y), new Vector2(x, max.y), this.bigGridLineColor, this.thickness * 2 / view.camera.zoom));

        for (let y = startY; y <= max.y; y += this.height)
            if (y == 0)
                axisLines.push(new Line(new Vector2(min.x, y), new Vector2(max.x, y), Settings.emblemRed, this.axisThickness / view.camera.zoom));
            else if (y % (this.height * this.bigGridNumber) != 0)
                gridLines.push(new Line(new Vector2(min.x, y), new Vector2(max.x, y), this.gridLineColor, this.thickness / view.camera.zoom));
            else
                bigGridLines.push(new Line(new Vector2(min.x, y), new Vector2(max.x, y), this.bigGridLineColor, this.thickness * 2 / view.camera.zoom));

        return axisLines.concat(gridLines).concat(bigGridLines);
    }
}