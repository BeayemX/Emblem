﻿class FinalRenderView extends View {
    constructor(container)
    {
        super(container);
        this.renderer = new FinalRenderRenderer(this);

        if (container) {
            this.isRenderPreviewing = true;
        }
    }

    // HACK? becaus if sth is selected in scene view it cant be rendered because its a different selection
    // this is more or less okay because these views are only used to display things...
    // maybe this hack can be avoided if FinalRenderRenderer wouldn't extend SceneRenderer...
    get selection() {
        return Application.sceneView.selection;
    }
    set selection(value){}

}

// TODO maybe don't extend SceneRenderer, because there are many unnecessary methods eg drawGrid, drawStates etc
// maybe maybe SceneRenderer and EditorRenderer should extend some class which itself extends Renderer...
class FinalRenderRenderer extends SceneRenderer {
    constructor(view) {
        super(view);
    }

    // overwrite methods so they don't get execuded from super
    drawRenderBG() {
        if (this.view.container)
            super.drawRenderBG();
    }

    cutRenderPreview() { }
}
