class PolarGrid {
	constructor() {
		this.width = 32;
		this.height = 32;
		this.radius = 50;
		this.segments = 36;
		this.angleOffset = 90;
		this.placePolarGridCenterAt2DCursor = false;
		this.positionOffset = Vector2.zero;

		this.thickness = 0.33;
        this.color = '#444';
	}

	getNearestPointFor(p) {
		let pos = p.copy();
		let offsetPosition = new Vector2((pos.x - this.positionOffset.x) / this.width, (pos.y - this.positionOffset.y) / this.height);
		let r = Math.round(offsetPosition.magnitude());
		let a = Math.atan2(offsetPosition.y, offsetPosition.x);
		let step = 2 * Math.PI / this.segments;
		a = Math.round((a - this.angleOffset * Math.PI / 180) / step) * step + this.angleOffset * Math.PI / 180;
		pos.x = Math.cos(a) * r;
		pos.y = Math.sin(a) * r;
		return new Vector2(pos.x * this.width, pos.y * this.height).addVector(this.positionOffset);
	}

	getLines(view) {
		let step = 2 * Math.PI / this.segments;
		let angle = this.angleOffset * Math.PI / 180 - step;
		let prevSin = Math.sin(angle);
		let prevCos = Math.cos(angle);

		let lines = [];

		for (let i = 0; i < this.segments; i++) {
			angle = this.angleOffset * Math.PI / 180 + i * step;
			let sin = Math.sin(angle);
			let cos = Math.cos(angle);
			lines.push(new Line(this.positionOffset, this.positionOffset.addVector(new Vector2(cos * this.width * this.radius, sin * this.height * this.radius)), this.color, this.thickness / view.camera.zoom));
			for (let j = 0; j <= this.radius; j++) {
				let from = this.positionOffset.addVector(new Vector2(prevCos * this.width * j, prevSin * this.height * j));
				let to = this.positionOffset.addVector(new Vector2(cos * this.width * j, sin * this.height * j));
				lines.push(new Line(from, to, this.color, this.thickness / view.camera.zoom));
			}
			prevSin = sin;
			prevCos = cos;
		}

		return lines;
	}
}