﻿class Cursor {
    constructor() {
        this.position = Vector2.zero;
        this.range = 10;
    }

    get currentPosition() {
        if (Application.shouldSnap())
            return GridManager.grid.getNearestPointFor(this.position);
        else
            return this.position.copy();
    }

    intersect(obj) {
        if (obj instanceof Line) {
            throw("not defined for Line!")
        } else if (obj instanceof LineEnding) {
            let radius = this.range + (obj.line.thickness * 0.5);
            return Vector2.sqrDistance(obj.position, this.position) <= radius * radius;
        } else if (obj instanceof Vector2) {
            return Vector2.sqrDistance(obj, this.position) <= this.range * this.range;
        }

        return false;
    }

    withinTimesRange(pos, times) {
        if (pos instanceof LineEnding)
            pos = pos.position;

        let radius = this.range * times;
        return Vector2.sqrDistance(this.position, pos) <= radius * radius;
    }
}