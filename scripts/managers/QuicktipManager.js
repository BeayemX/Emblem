﻿class QuicktipManager {

    constructor()
    {
        this.quickTipOverlay = quicktip;
        this.container = quicktipContainer;

        this.localStorageKey = "dontShowTips";

        this.tips = []
        this.tipIndex = localStorage.getItem("tipIndex") ? Number(localStorage.getItem("tipIndex")) : -1;

        this.tips.push("You can change the line thickness by holding the control key while scrolling! <br><br>Selected lines will change their size too!");
        this.tips.push("You can change the selection radius by holding the shift key while scrolling!");
        this.tips.push("Use Control + B to draw the rectangle which will be rendered!");
        this.tips.push("You can quickly move lines when clicking on something selectable, holding down and dragging with the right mouse button!");
        this.tips.push("You can enter numbers with your keyboard in the MoveState (G), RotationState (R) and ScalingState (S)!");
        this.tips.push("You can lock an axis while in the MoveState or ScaleState by pressing X or Y on your keyboard!");
        this.tips.push("Double / tripple click with the right mouse button to grow the selection / select line connected!");
        this.tips.push("You can press E to extrude the selected lines!");
        this.tips.push("You can press F to connect the endings of all selected lines!");
        this.tips.push("Hold the spacebar and use the right mouse button to measure things in grid-units!");
        this.tips.push("When holding down Shift while placing prefabs the instance will instead be placed as lines!");
        this.tips.push("While holding down D you can draw with the continuous drawing tool!")
        this.tips.push("You can toggle grid snapping temporarily by holding down the Control key!")
        this.tips.push("You can selecte / deselect everything by pressing A!")
        this.tips.push("You can use a line only for cutting by drawing a line while holding down Alt!")
        
        let dontShow = localStorage.getItem(this.localStorageKey);

        this.setTip(this.tips[this.tipIndex % this.tips.length])
        if (!dontShow)
            this.nextTipPressed();
    }

    setTip(text) {
        this.container.innerHTML = "Tip " + (this.tipIndex+1) + ":<br> " + text;
    }

    okButtonPressed() {
        this.close();
    }

    nextTipPressed() {
        this.tipIndex = ++this.tipIndex % this.tips.length;
        this.setTip(this.tips[this.tipIndex]);
        this.show();
        localStorage.setItem("tipIndex", this.tipIndex);
    }

    dontShowAgainPressed() {
        localStorage.setItem(this.localStorageKey, 1);
        this.close();
    }

    menuButtonPressed() {
        if (this.quickTipOverlay.style.visibility == "visible")
            this.nextTipPressed();
        else
            this.show();
    }

    show() {
        this.quickTipOverlay.style.visibility = 'visible';
        localStorage.removeItem(this.localStorageKey);
    }

    close() {
        this.quickTipOverlay.style.visibility = 'hidden';
    }
}
