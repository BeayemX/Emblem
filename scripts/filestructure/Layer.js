﻿class Layer {
    constructor() {
        this.name = "New Layer";
        this.visible = true;
        this.hideInRenderPreview = false;
        
        this.data = new Data();
    }

    copy() {
        let layer = new Layer();

        layer.data = this.data.copy();
        layer.name = this.name;
        layer.visible = this.visible;
        layer.hideInRenderPreview = this.hideInRenderPreview;

        return layer;
    }
}
