class InputRecorderState extends State {
    constructor() {
        super();
        this.axisLock = "";
        this.digitsBeforeComma = "";
        this.digitsAfterComma = "";
        this.comma = false;
        this.negative = false;

        // TODO not sure if needed
        this.active = false; // only active after first keystroke, if want to use mouse instead
    }

    updateValue() {
        console.log("Not Implemented Exception");
    }

    execute() {
        console.log("Not Implemented Exception");
    }

    cancel() {
        console.log("Not Implemented Exception");
    }

    getRecordedInputNumberAsString() {
        let number = (this.digitsBeforeComma + "." + this.digitsAfterComma);

        if (this.negative)
            number *= -1;
        return number;
    }

    record(keycode) {
        if (keycode >= Key.Alpha0 && keycode <= Key.Alpha9) {
            if (this.comma)
                this.digitsAfterComma += keycode - Key.Alpha0;
            else
                this.digitsBeforeComma += keycode - Key.Alpha0;
        }
        else if (keycode >= Key.Num0 && keycode <= Key.Num9) {
            if (this.comma)
                this.digitsAfterComma += keycode - Key.Num0;
            else
                this.digitsBeforeComma += keycode - Key.Num0;
        }
        else if (keycode == Key.X) {
            if (this.axisLock == "X")
                this.axisLock = "";
            else
                this.axisLock = "X";
        }
        else if (keycode == Key.Y || keycode == Key.Z) {
            if (this.axisLock == "Y")
                this.axisLock = "";
            else
                this.axisLock = "Y";
        }
        else if (keycode == Key.Comma || keycode == Key.Period || keycode == Key.NumComma) {
            this.comma = true;
        }
        else if (keycode == Key.Enter) {
            this.execute();
            Application.currentView.logic.setState(new IdleState());
            return;
        }
        else if (keycode == Key.Backspace) {
            if (this.digitsAfterComma.length > 0)
                this.digitsAfterComma = this.digitsAfterComma.slice(0, this.digitsAfterComma.length - 1);
            else if (this.comma)
                this.comma = false;
            else if (this.digitsBeforeComma.length > 0)
                this.digitsBeforeComma = this.digitsBeforeComma.slice(0, this.digitsBeforeComma.length - 1);
        }
        else if (keycode == Key.AlphaSubtract || keycode == Key.NumSubtract) {
            this.negative = !this.negative;
        }
        else if (keycode == Key.Escape) {
            this.cancel();
            return;
        }
        else {// unused key
            return;
        }

        this.active = true;

        //this.setData(this.getRecordedInputNumberAsString(), this.axisLock);
        this.updateValue();

        GUI.writeToStatusbarLeft(this.getRecordedInputNumberAsString());
    }

    get number() {
        return +this.getRecordedInputNumberAsString();
    }
}
/*
class InputRecorderData {
    constructor() {
        this.number;
        this.axisLock = "";
    }
}*/