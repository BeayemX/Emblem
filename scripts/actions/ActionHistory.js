﻿/*class ActionHistory {
    static init() {
        console.log("ActionHistory created.");

        this.states = [];
        this.undoneStates = [];
        let layers = [];
        layers.push(new Layer());
        this.currentState = new HistoryState(0, layers, new SelectionData());
    }

    static writeHistory() {
        this.undoneStates = [];

        this.states.push(this.currentState);
        this.currentState = this.getState();
        //this.setState(this.getState());
        GUI.notify("Snapshot created.");

    }

    static getState() {
        let layers = [];
        for (let layer of Application.file.layers) {
            layers.push(layer.copy());
        }
        let selection = Selection.data.copy();

        let state = new HistoryState(Application.file.getCurrentLayerID(), layers, selection);

        return state;
    }

    static setState(state) {
        this.currentState = state;

        Application.file.layers = state.layers;
        Application.file.selectLayerWithID(state.currentLayerID);
        Selection.data = state.selection;
    }

    static undo() {

        let state = this.states.pop();

        if (state) {
            this.undoneStates.push(this.currentState);
            this.setState(state);
            Application.redrawAllViews();
        }
    }

    static redo() {
        let state = this.undoneStates.pop();

        if (state) {
            this.states.push(this.currentState);
            this.setState(state);
            Application.redrawAllViews();
        }
    }
}
class HistoryState {
    constructor(currentLayerID, layers, selection) {
        this.currentLayerID = currentLayerID;
        this.layers = layers;
        this.selection = selection;
    }
}
// */
class ActionHistory {
    static init() {
        console.log("ActionHistory created.");

        this.actions = [];
        this.undoneActions = [];
    }

    static pushAction(action, redoAction) {
        this.actions.push(action)

        // TODO clears redo-actions if new action is performed. maybe use if possible? 
        // e.g. move lines on a new selection?
        if (arguments.length == 1 || !redoAction)
            this.undoneActions = [];

        action.Do();
    }

    static popAction() {
        var action = this.actions.pop();
        if (action) {
            action.undo();
            this.undoneActions.push(action);
        }
    }

    static undo() {
        this.popAction();
        Application.redrawAllViews();
    }
    static redo() {
        var action = this.undoneActions.pop();

        if (action)
            this.pushAction(action, true);
        Application.redrawAllViews();
    }
}