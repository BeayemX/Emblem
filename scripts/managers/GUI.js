﻿class GUI {
    static init() {
        console.log("GUI created.");

        this.menubar = document.getElementById("menubar");
        this.statusbar = document.getElementById("statusbar");
        this.statusbarentryleft = document.getElementById("statusbarentryleft");
        this.statusbarentryright = document.getElementById("statusbarentryright");
        this.leftarea = document.getElementById('leftarea');
        this.rightarea = document.getElementById('rightarea');
        this.rightarea.style.visibility = "visible";

        // Stats
        this.stats = document.getElementById('stats');
        this.statsDict = [];

        // Layers
        this.layersDiv = document.getElementById('layersDiv')

        // Prefab
        this.prefabsDiv = document.getElementById('prefabsDiv')

        // Toggles
        this.lineHandlesToggle = document.getElementById("lineHandlesToggle");
        this.cutLinesToggle = document.getElementById("cutLinesToggle");
        this.gridSnapToggle = document.getElementById("gridSnapToggle");
        this.use2DCursorToggle = document.getElementById("use2DCursorToggle");
        this.selectVerticesToggle = document.getElementById("selectVerticesToggle");
        this.selectEdgesToggle = document.getElementById("selectEdgesToggle");
        this.mirrorXToggle = document.getElementById("mirrorXToggle");
        this.mirrorYToggle = document.getElementById("mirrorYToggle");
        this.devModeToggle = document.getElementById("devModeToggle");
        this.onlyTransformPrefabInstancePositionToggle = document.getElementById("onlyTransformPrefabInstancePositionToggle");
        this.showRenderPreviewToggle = document.getElementById("showRenderPreviewToggle");

        // Gridsettings
        this.gridSettings = document.getElementById("gridSettings");
        this.gridTypeSelection = document.getElementById("gridTypeSelection");

        // Rendering
        this.artBoundsLeft = document.getElementById("artBoundsLeft");
        this.artBoundsTop = document.getElementById("artBoundsTop");
        this.artBoundsWidth = document.getElementById("artBoundsWidth");
        this.artBoundsHeight = document.getElementById("artBoundsHeight");
        this.renderScaleFactor = document.getElementById("renderScaleFactor");
        this.renderResolution = document.getElementById("renderResolution");

        // Line settings
        this.lineColor = document.getElementById("lineColor");
        this.lineAlpha = document.getElementById("lineAlpha");
        this.lineThickness = document.getElementById("lineThickness");

        // Preferences
        this.rmbSelectionType = document.getElementById("rmbSelectionType");
        this.qualitySelection = document.getElementById("qualitySelection");
        this.renderGuideLayers = document.getElementById("renderGuideLayers");

        this.quicktipManager = new QuicktipManager();
        waitingForStart.push(this);
    }

    static start() {
        this.updateArtbounds();

        // toggles
        this.updateLineHandlesToggle();
        this.updateCutLinesToggle();
        this.updateGridSnapToggle();
        this.updateUse2DCursorToggle();
        this.updateSelectVerticesToggle();
        this.updateSelectEdgesToggle();
        this.updateDevModeToggle();
        this.updateRenderGuideLayers();
        this.updateOnlyTransformPrefabInstancePositionToggle();
        this.genereateGridSettings();
        this.updateShowRenderPreviewToggleToggle();

        Application.setQualitySetting(Preferences.qualitySetting);
        this.rmbSelectionType.value = Preferences.rmbSelectionType;
    }

    static genereateGridSettings() {
        while (this.gridSettings.firstChild) {
            this.gridSettings.removeChild(this.gridSettings.firstChild);
        }

        let table = document.createElement("table");
        this.gridSettings.appendChild(table);

        let col = document.createElement("col");
        col.setAttribute("width", "15%");
        table.appendChild(col);
        col = document.createElement("col");
        col.setAttribute("width", "15%");
        table.appendChild(col);



        if (Preferences.gridType == 0) {

            // Width
            let tr = document.createElement("tr");
            table.appendChild(tr);

            let td = document.createElement("td");
            td.innerHTML = "Width";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            let input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("max", "100");
            input.setAttribute("value", GridManager.grid.width);
            input.setAttribute("onchange", "GridManager.grid.width = +value; Application.redrawAllViews();");
            td.appendChild(input);

            // Height
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Height";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("max", "100");
            input.setAttribute("value", GridManager.grid.height);
            input.setAttribute("onchange", "GridManager.grid.height = +value; Application.redrawAllViews();");
            td.appendChild(input);

            // Big grid
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Big Grid";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "0");
            input.setAttribute("value", GridManager.grid.bigGridNumber);
            input.setAttribute("onchange", "GridManager.grid.bigGridNumber= +value; Application.redrawAllViews();");
            td.appendChild(input);
        }
        else if (Preferences.gridType == 1) {

            let tr = document.createElement("tr");
            table.appendChild(tr);

            let td = document.createElement("td");
            td.innerHTML = "Width";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            let input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.width);
            input.setAttribute("onchange", "GridManager.grid.width = +value; Application.redrawAllViews()");
            td.appendChild(input);

            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Height";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.height);
            input.setAttribute("onchange", "GridManager.grid.height = +value; Application.redrawAllViews()");
            td.appendChild(input);



            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.setAttribute("colspan", "2");
            tr.appendChild(td);


            input = document.createElement("input");
            input.setAttribute("type", "button");
            input.setAttribute("onclick", "GridManager.grid.uniformHeight(); GUI.genereateGridSettings(); Application.redrawAllViews()");
            input.value = "Uniform Height";
            td.appendChild(input);

            tr = document.createElement("tr");
            table.appendChild(tr);
            td = document.createElement("td");
            td.setAttribute("colspan", "2");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "button");
            input.setAttribute("onclick", "GridManager.grid.swapXAndY = !GridManager.grid.swapXAndY; Application.redrawAllViews()");
            input.value = "Swap X and Y";
            td.appendChild(input);
        }
        else if (Preferences.gridType == 2) {

            // width 
            let tr = document.createElement("tr");
            table.appendChild(tr);

            let td = document.createElement("td");
            td.innerHTML = "Width";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            let input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.width);
            input.setAttribute("onchange", "GridManager.grid.width = +value; Application.redrawAllViews()");
            td.appendChild(input);

            // height
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Height";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.height);
            input.setAttribute("onchange", "GridManager.grid.height = +value; Application.redrawAllViews()");
            td.appendChild(input);

            // radius
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Radius";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.radius);
            input.setAttribute("onchange", "GridManager.grid.radius= +value; Application.redrawAllViews()");
            td.appendChild(input);

            // segments
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Segments";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.segments);
            input.setAttribute("onchange", "GridManager.grid.segments = +value; Application.redrawAllViews()");
            td.appendChild(input);

            // angleOffset
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Angle Offset";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "number");
            //input.setAttribute("min", "1");
            input.setAttribute("value", GridManager.grid.angleOffset);
            input.setAttribute("onchange", "GridManager.grid.angleOffset= +value; Application.redrawAllViews()");
            td.appendChild(input);


            // placePolarGridCenterAt2DCursor
            tr = document.createElement("tr");
            table.appendChild(tr);

            td = document.createElement("td");
            td.innerHTML = "Use Pivot as center";
            tr.appendChild(td);

            td = document.createElement("td");
            tr.appendChild(td);

            input = document.createElement("input");
            input.setAttribute("type", "checkbox");
            //input.setAttribute("min", "1");
            if (GridManager.grid.placePolarGridCenterAt2DCursor == true)
                input.setAttribute("checked", true);

            input.setAttribute("onchange", "GUI.setPolarGridCenterToPivot(checked)");
            td.appendChild(input);


            if (GridManager.grid.placePolarGridCenterAt2DCursor == false) {
                // posOffsetX
                tr = document.createElement("tr");
                table.appendChild(tr);

                td = document.createElement("td");
                td.innerHTML = "Offset X";
                tr.appendChild(td);

                td = document.createElement("td");
                tr.appendChild(td);

                input = document.createElement("input");
                input.setAttribute("type", "number");
                //input.setAttribute("min", "1");
                input.setAttribute("value", GridManager.grid.positionOffset.x);
                input.setAttribute("onchange", "GridManager.grid.positionOffset.x = +value; Application.redrawAllViews()");
                td.appendChild(input);

                // posOffsetY
                tr = document.createElement("tr");
                table.appendChild(tr);

                td = document.createElement("td");
                td.innerHTML = "Offset Y";
                tr.appendChild(td);

                td = document.createElement("td");
                tr.appendChild(td);

                input = document.createElement("input");
                input.setAttribute("type", "number");
                //input.setAttribute("min", "1");
                input.setAttribute("value", GridManager.grid.positionOffset.y);
                input.setAttribute("onchange", "GridManager.grid.positionOffset.y = +value; Application.redrawAllViews()");
                td.appendChild(input);
            }
        }
    }

    static writeToStatusbarLeft(text) {
        statusbarentryleft.innerHTML = text;
    }

    static writeToStatusbarRight(text) {
        statusbarentryright.innerHTML = text;
    }

    static writeToStats(k, v) {
        this.statsDict[k] = v;
        let text = "<table>";
        for (let key in this.statsDict) {
            if (this.statsDict.hasOwnProperty(key)) {
                text += "<tr>";
                text += "<td>";
                text += key;
                text += "</td>";
                text += "<td>";
                text += "&nbsp;";
                text += "</td>";
                text += "<td align='right'>";
                text += this.statsDict[key];
                text += "</td>";
                text += "</tr>";
            }
        }
        text += "</table>";

        stats.innerHTML = text;
    }

    static removeEntryFromStats(k) {
        delete this.statstDict[k];
    }

    static objectHierarchyChanged() {
        while (this.layersDiv.firstChild) {
            this.layersDiv.removeChild(this.layersDiv.firstChild);
        }

        let layers = Application.file.layers;
        let table = document.createElement("table");
        table.setAttribute("id", "layers");


        // hide layer button
        let col = document.createElement("col");
        col.setAttribute("width", "10%");
        table.appendChild(col);

        // layer render visibility
        col = document.createElement("col");
        col.setAttribute("width", "10%");
        table.appendChild(col);

        // select layer button
        col = document.createElement("col");
        col.setAttribute("width", "100%");
        table.appendChild(col);

        // move layer up button
        col = document.createElement("col");
        col.setAttribute("width", "10%");
        table.appendChild(col);

        // move layer down button
        col = document.createElement("col");
        col.setAttribute("width", "10%");
        table.appendChild(col);

        // delete layer button
        col = document.createElement("col");
        col.setAttribute("width", "10%");
        table.appendChild(col);

        for (let i = layers.length-1; i >= 0; --i) {
            let tr = document.createElement("tr");
            table.appendChild(tr);


            // hide layer button
            let td = document.createElement("td");
            tr.appendChild(td);
            let button = document.createElement("button");
            button.setAttribute("type", "button");
            //button.setAttribute("class", "smallLayerButton");
            button.setAttribute("title", "Hide / unhide layer");
            button.setAttribute("onclick", "Application.file.toggleVisibilityOfLayerWithID(" + i + ")");
            if (layers[i].visible)
                button.innerHTML = "O";
            else
                button.innerHTML = "-";
            td.appendChild(button);

            // hide in render preview button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            //button.setAttribute("class", "smallLayerButton");
            button.setAttribute("title", "Hide / unhide for render preview only");
            button.setAttribute("onclick", "Application.file.toggleRenderableOfLayerWithID(" + i + ")");
            if (layers[i].hideInRenderPreview)
                button.innerHTML = "::";
            else
                button.innerHTML = "#";
            td.appendChild(button);

            // select layer button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("class", "layerbutton");
            button.setAttribute("title", "Rightclick to rename");
            button.setAttribute("onmousedown", "Application.file.selectLayerWithID(" + i + ")");
            button.setAttribute("oncontextmenu", "this.focus(); document.execCommand('selectAll',false,null);");
            button.setAttribute("contenteditable", "true");
            button.setAttribute("oninput", "Application.file.changeNameForLayerWithID(" + i + ", this.innerHTML)");

            if (Application.file.currentLayer == layers[i]) {
                button.setAttribute("id", "selectedButton");
                tr.setAttribute("id", "selectedLayer");
            }
            button.innerHTML = layers[i].name;
            td.appendChild(button);

            // move layer up button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("title", "Move layer up");
            button.setAttribute("onclick", "Application.file.moveLayerUp(" + i + ")");
            button.innerHTML = "&uarr;";
            td.appendChild(button);
            // move layer up button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("title", "Move layer down");
            button.setAttribute("onclick", "Application.file.moveLayerDown(" + i + ")");
            button.innerHTML = "&darr;";
            td.appendChild(button);
            // delete layer button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("title", "Delete layer");
            button.setAttribute("onclick", "Application.file.deleteLayerWithID(" + i + ")");
            button.innerHTML = "<font color='red'>X</font>";
            td.appendChild(button);
        }
        this.layersDiv.appendChild(table);
    }
























    static buildPrefabUI() {
        while (this.prefabsDiv.firstChild) {
            this.prefabsDiv.removeChild(this.prefabsDiv.firstChild);
        }

        let prefabs = PrefabManager.prefabs;

        let table = document.createElement("table");
        table.setAttribute("id", "prefabs");

        // edit prefab button
        let col = document.createElement("col");
        col.setAttribute("width", "20%");
        table.appendChild(col);

        // select prefab button
        col = document.createElement("col");
        col.setAttribute("width", "60%");
        table.appendChild(col);

        // delete prefab button
        col = document.createElement("col");
        col.setAttribute("width", "20%");
        table.appendChild(col);

        for (let i = 0; i < prefabs.length; ++i) {
            let tr = document.createElement("tr");
            table.appendChild(tr);

            // edit prefab button
            let td = document.createElement("td");
            tr.appendChild(td);
            let button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("onclick", "PrefabManager.editPrefab(" + i + ")");
            button.innerHTML = "Edit";

            if (Application.prefabEditorView.prefab == prefabs[i]) {
                button.setAttribute("id", "selectedButton");
                button.setAttribute("title", "Apply changes");
            }
            else {
                button.setAttribute("title", "Edit prefab");
            }


            td.appendChild(button);

            // select prefab button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("class", "prefabbutton");
            button.setAttribute("title", "Leftclick to place instances. Rightclick to rename.");
            button.setAttribute("onmousedown", "PrefabManager.selectPrefabWithID(" + i + ")");
            button.setAttribute("oncontextmenu", "this.focus(); document.execCommand('selectAll',false,null);");
            button.setAttribute("contenteditable", "true");
            button.setAttribute("oninput", "PrefabManager.changeNameForPrefabWithID(" + i + ", this.innerHTML)");

            if (Application.logic.currentState instanceof PrefabBrushState &&
                Application.logic.currentState.currentPrefab == prefabs[i]) {
                button.setAttribute("id", "selectedButton");
                tr.setAttribute("id", "selectedPrefab");
            }

            button.innerHTML = prefabs[i].name;
            td.appendChild(button);

            // convert prefab to lines button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("title", "Delete prefab and convert all of it's instances to lines");
            button.setAttribute("onclick", "PrefabManager.convertPrefabWithIDToLines(" + i + ")");
            button.innerHTML = "L";
            td.appendChild(button);

            // delete prefab button
            td = document.createElement("td");
            tr.appendChild(td);
            button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("title", "Delete prefab and all of it's instances");
            button.setAttribute("onclick", "PrefabManager.deletePrefabWithID(" + i + ")");
            button.innerHTML = "<font color='red'>X</font>";
            td.appendChild(button);
        }
        this.prefabsDiv.appendChild(table);
    }






























    
    static notify(text, color) {
        if (this.displayingNotification == true) {
            clearTimeout(this.notificationDisplayCallback);
            clearTimeout(this.notificationAnimationListener);
            this.clearNotification();
            this._resetNotificationDisplay();
        }

        this.displayingNotification = true;
        
        if (color != undefined)
            text = "<font color=" + color + ">" + text + "</font>";

        this.writeToStatusbarRight(text);
        //console.log(text);
        this.notificationDisplayCallback = setTimeout(() => this.clearNotification(), Settings.notificationTimeout);
    }

    static clearNotification() {
        statusbarentryright.setAttribute("class", "statusbarentry fadeOut");
        this.notificationAnimationListener = setTimeout(() =>  this._resetNotificationDisplay(), 2000);
    }

    static _resetNotificationDisplay() {
        statusbarentryright.setAttribute("class", "statusbarentry");
        // TODO not sure if this is called too often
        // console.log("called")

        this.writeToStatusbarRight("");
        this.displayingNotification = false;
        this.notificationDisplayCallback = null;
        this.notificationAnimationListener = null;
    }

    static updateArtbounds() {
        this.artBoundsWidth.value = Preferences.artBounds.width;
        this.artBoundsHeight.value = Preferences.artBounds.height;
        Preferences.artBounds.adjustDisplayedResolution();
    }

    static setPolarGridCenterToPivot(checked) {
        GridManager.grid.placePolarGridCenterAt2DCursor = checked;

        if (GridManager.grid.placePolarGridCenterAt2DCursor == true)
            GridManager.grid.positionOffset = Globals.pivot.copy();
        else
            GridManager.grid.positionOffset = Vector2.zero;

        GUI.genereateGridSettings();
        Application.redrawAllViews();
    }

    // Toggles
    static lineHandlesTogglePressed() {
        Preferences.showLineHandles = !Preferences.showLineHandles;
        this.updateLineHandlesToggle();
    }

    static cutLinesTogglePressed() {
        Preferences.cutLines = !Preferences.cutLines;
        this.updateCutLinesToggle();
    }

    static gridSnapTogglePressed() {
        Preferences.snapToGrid = !Preferences.snapToGrid;
        this.updateGridSnapToggle();
    }

    static use2DCursorTogglePressed() {
        Preferences.usePivot = !Preferences.usePivot;
        this.updateUse2DCursorToggle()
    }


    static updateLineHandlesToggle() {
        if (Preferences.showLineHandles) {
            this.lineHandlesToggle.setAttribute("class", "pressed");
        }
        else {
            this.lineHandlesToggle.removeAttribute("class");
        }

        Application.redrawAllViews();
    }
    static updateCutLinesToggle() {
        if (Preferences.cutLines) {
            this.cutLinesToggle.setAttribute("class", "pressed");
        }
        else {
            this.cutLinesToggle.removeAttribute("class");
        }
    }
    static updateGridSnapToggle() {
        if (Preferences.snapToGrid) {
            this.gridSnapToggle.setAttribute("class", "pressed");
        }
        else {
            this.gridSnapToggle.removeAttribute("class");
        }
    }
    static updateUse2DCursorToggle() {
        if (Preferences.usePivot)
            this.use2DCursorToggle.setAttribute("class", "pressed");
        else
            this.use2DCursorToggle.removeAttribute("class");

        Application.redrawAllViews()
    }

    // vertices and edge selection
    static selectVerticesTogglePressed() {
        Preferences.selectVertices = !Preferences.selectVertices;
        this.updateSelectVerticesToggle()
    }
    static updateSelectVerticesToggle() {
        if (Preferences.selectVertices)
            this.selectVerticesToggle.setAttribute("class", "pressed");
        else
            this.selectVerticesToggle.removeAttribute("class");
    }
    static selectEdgesTogglePressed() {
        Preferences.selectEdges = !Preferences.selectEdges;
        this.updateSelectEdgesToggle()
    }
    static updateSelectEdgesToggle() {
        if (Preferences.selectEdges)
            this.selectEdgesToggle.setAttribute("class", "pressed");
        else
            this.selectEdgesToggle.removeAttribute("class");
    }
    // render preview
    static showRenderPreviewToggleTogglePressed() {
        Preferences.showRenderPreview = !Preferences.showRenderPreview;
        this.updateShowRenderPreviewToggleToggle();
    }

    static updateShowRenderPreviewToggleToggle() {
        if (Preferences.showRenderPreview){
            renderPreview.style.visibility = "visible";
            this.showRenderPreviewToggle.setAttribute("class", "pressed");
        }
        else {
            renderPreview.style.visibility = "hidden";
            showRenderPreviewToggle.removeAttribute("class");
        }
    }

    // Mirror buttons
    static mirrorXTogglePressed() {
        Globals.mirrorX = !Globals.mirrorX;
        this.updateMirrorXToggle()
    }
    static updateMirrorXToggle() {
        if (Globals.mirrorX)
            this.mirrorXToggle.setAttribute("class", "pressed");
        else
            this.mirrorXToggle.removeAttribute("class");
        Application.redrawAllViews();
    }

    static mirrorYTogglePressed() {
        Globals.mirrorY = !Globals.mirrorY;
        this.updateMirrorYToggle()
    }
    static updateMirrorYToggle() {
        if (Globals.mirrorY)
            this.mirrorYToggle.setAttribute("class", "pressed");
        else
            this.mirrorYToggle.removeAttribute("class");
        Application.redrawAllViews();
    }

    // dev mode toggle
    static devModeTogglePressed() {
        Preferences.developerMode = !Preferences.developerMode;
        this.updateDevModeToggle()
    }
    static updateDevModeToggle() {
        if (Preferences.developerMode) {
            this.devModeToggle.setAttribute("class", "pressed");
            this.stats.style.visibility = "visible";

            for (let beta of document.getElementsByClassName("beta"))
                beta.style.display = "block";
            for (let dev of document.getElementsByClassName("dev"))
                dev.style.display = "block";
        }
        else {
            this.devModeToggle.removeAttribute("class");
            this.stats.style.visibility = "hidden";

            for (let beta of document.getElementsByClassName("beta"))
                beta.style.display = "none";
            for (let dev of document.getElementsByClassName("dev"))
                dev.style.display = "none";
        }
        Application.layoutGUI();
    }

    static onlyTransformPrefabInstancePositionTogglePressed() {
        Preferences.onlyTransformPrefabInstancePosition = !Preferences.onlyTransformPrefabInstancePosition;
        this.updateOnlyTransformPrefabInstancePositionToggle();
    }

    static updateOnlyTransformPrefabInstancePositionToggle() {
        if (Preferences.onlyTransformPrefabInstancePosition)
            this.onlyTransformPrefabInstancePositionToggle.setAttribute("class", "pressed");
        else
            this.onlyTransformPrefabInstancePositionToggle.removeAttribute("class");
    }

    static updateRenderGuideLayers() {
        this.renderGuideLayers.checked = Preferences.renderGuideLayers;
    }
}