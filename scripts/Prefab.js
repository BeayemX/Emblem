﻿class Prefab{
    constructor() {
        this.data = new Data();
        this.name = "Prefab";
    }
    
    addLine(line) {
        this.data.addLine(line);
    }

    addInstance() {
        this.data.addInstance(instance);
    }

    copy() {
        let copy = new Prefab();

        for (let line of this.lines)
            copy.addLine(line.copy());

        return copy;
    }

    getAsLines() {
        let lines = [];

        for (let line of this.data.lines) 
            lines.push(line);

        for (let instance of this.data.instances) 
            lines = lines.concat(instance.getAsLines());

        return lines;
    }

    includesNested(prefab) {
        if (prefab == this)
            return true;

        for (let instance of this.data.instances) {
            if (instance.reference.includesNested(prefab)) 
                return true;
        }

        return false;
    }
}