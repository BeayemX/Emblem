﻿class SelectionData {
    constructor(selection, lines, partialLines, points, instances) {
        this.selection = selection;
        this.lines = lines == undefined ? [] : lines;
        this.partialLines = partialLines == undefined ? [] : partialLines;
        this.points = points == undefined ? [] : points;
        this.instances = instances == undefined ? [] : instances;
    }

    addPoint(point) {
        // point already included
        for (let p of this.points) {
            if (p === point)
                return;
        }

        // check if opposite is also selected
        let other = point.opposite;

        for (let p of this.points) {
        // opposite is included
            if (p === other) {
                this.lines.push(point.line);
                Utilities.deleteArrayEntry(this.partialLines, point.line)
                Utilities.deleteArrayEntry(this.points, other);
                return;
            }
        }

        // check if line belonging to line already included
        for (let line of this.lines) {
            if (line == point.line) {
                return;
            }
        }

        // add point
        this.points.push(point);
        this.partialLines.push(point.line)
        Utilities.deleteArrayEntry(Application.currentView.data.lines, point.line)
    }

    removePoint(point) {
        // opposite not selected
        for (let p of this.points) {
            if (p === point) {
                Utilities.deleteArrayEntry(this.points, point);
                Utilities.deleteArrayEntry(this.partialLines, point.line);
                // HACK should use addLine, but problem is, then everytime a line gets deselected
                // everything will be deselected if lineCutting is active.
                // Application.currentView.data.addLine(point.line);
                this.selection.view.data.lines.push(point.line);
                return;
            }
        }

        // opposite selected
        for (let l of this.lines) {
            if (l === point.line) {
                Utilities.deleteArrayEntry(this.lines, point.line);
                this.points.push(point.opposite);
                this.partialLines.push(point.line);
                return;
            }
        }
    }

    // TODO should check if point is already selected 
    //DONT USE
    addLine(line) {
        this.lines.push(line);
        Utilities.deleteArrayEntry(Application.currentView.data.lines, line);

        Application.file.updateStats();
        //this.changed();
    }

    addLinesUnsafe(lines) {
        this.lines = this.lines.concat(lines);
        Utilities.deleteArrayEntry(Application.currentView.data.lines, line);

        Application.file.updateStats();
        //this.changed();
    }

    addInstance(instance) {
        if (this.instances.includes(instance))
            return;

        this.instances.push(instance);
        this._instanceSelectionChanged();
    }

    removeInstance(instance) {
        if (!this.instances.includes(instance))
            return;

        Utilities.deleteArrayEntry(this.instances, instance);
        this._instanceSelectionChanged();
    }

    copy() {
        let newSelData = new SelectionData(this.selection);

        for (let line of this.lines)
            newSelData.addLine(line.copy());

        for (let p of this.points) {
            let line = new Line(
                p.position,
                p.opposite.position,
                p.line.color,
                p.line.thickness
                );
            newSelData.addPoint(line.start);
        }

        for (let inst of this.instances)
            newSelData.addInstance(inst.copy());

        return newSelData;
    }

    _instanceSelectionChanged() {
        // TODO use but sth is not working right. when selection in editor and selecting sth else in scene view... or sth like that..
        /* that was to load selected prefab into editor, but that way placing nested prefabs doesnt work
        if (this.selection.view == Application.prefabEditorView){
            return;
        }

        if (this.instances.length == 0) {
            Application.prefabEditorView.setPrefab(null);
            return;
        }

        let reference = this.instances[0].reference;

        for (var i = 0; i < this.instances.length; ++i) {
            if (this.instances[i].reference != reference) {
                // multiple different prefabs selected
                Application.prefabEditorView.setPrefab(null);
                return;
            }
        }

        // only one prefab reference among all selected instances
        Application.prefabEditorView.setPrefab(reference);
        //*/
    }

    changed() {
        //this.selection.changed(); // TOOD use?
        Application.selectionChanged(this.selection);
        Application.file.updateStats();
    }

    getAsLines() {
        let lines = this.lines.concat(this.partialLines);

        for (let instance of this.instances)
            lines = lines.concat(instance.getAsLines());

        return lines;
    }

    getAllPositions() {
        let instancesPositions = [];
        for (let instance of this.instances)
            instancesPositions.push(instance.transform);
        return Utilities.linesToLineEndings(this.lines).concat(this.points).concat(instancesPositions);
    }
}