﻿class IdleState extends State {
    constructor() {
        super();
    }
    enter() {

    }

    exit() {

    }

    toString() {
        return "IdleState";
    }


    keyDown(keycode) {
        switch (keycode) {
            case Key.A:
                Application.currentView.selection.selectAllToggle();
                Application.redrawAllViews();
                break;
            case Key.I:
                Application.currentView.selection.invert();
                Application.redrawAllViews();
                break;
            case Key.C:
                if (Input.isKeyDown(Key.Control))
                    Saver.copyLinesToClipboard();
                else if (!Input.isKeyDown(Key.Control) && !Input.isKeyDown(Key.Shift))
                    Application.currentView.logic.setState(new BrushSelectionState());
                else if (Input.isKeyDown(Key.Shift)) {
                    Application.currentView.camera.resetView();
                }
                break;
            case Key.V:
                //if (Input.isKeyDown(Key.Control)) 
                if (Saver.pasteLines())
                    Application.currentView.logic.setState(new MoveLinesState());
                break;
            case Key.X:
                if (!Application.currentView.selection.isEmpty()) {
                    if (Input.isKeyDown(Key.Control))
                        Saver.copyLinesToClipboard();

                    Application.currentView.selection.deleteSelection();
                    Application.redrawAllViews();
                }
                break;
            case Key.Delete:
            case Key.Backspace:
                if (!Application.currentView.selection.isEmpty()) {
                    Application.currentView.selection.deleteSelection();
                    Application.redrawAllViews();
                }
                break;

            case Key.ArrowLeft:
                this.arrowMovement(-1, 0, Input.isKeyDown(Key.Shift), Input.isKeyDown(Key.Control));
                break;
            case Key.ArrowUp:
                this.arrowMovement(0, -1, Input.isKeyDown(Key.Shift), Input.isKeyDown(Key.Control));
                break;
            case Key.ArrowRight:
                this.arrowMovement(1, 0, Input.isKeyDown(Key.Shift), Input.isKeyDown(Key.Control));
                break;
            case Key.ArrowDown:
                this.arrowMovement(0, 1, Input.isKeyDown(Key.Shift), Input.isKeyDown(Key.Control));
                break;
            case Key.M:
                Application.currentView.selection.merge();
                break;

            case Key.Z:
                ActionHistory.undo();
                break;

            case Key.Y:
                ActionHistory.redo();
                break;
            case Key.L:
                Application.currentView.selectLinked();
                break;

            case Key.S:
                if (Input.isKeyDown(Key.Shift)) {
                    Application.currentView.selection.snapToGrid();
                }
                break;
            case Key.Shift:
                Globals.drawPolyLine = true;
                break;

            case Key.P:
                if (Input.isKeyDown(Key.Control))
                    Exporter.renderPNG();
                break;
            case Key.F: // F // TODO improve. Application.currentView.camera.zoom to selection / Application.currentView.camera.zoom fit / etc ... 
                Utilities.fill(Application.currentView.selection.getAllSelectedPoints());
                break;
                
            case Key.Num0:
            case Key.Insert: // if numpad is disabled
                if (Application.currentView)
                    Application.currentView.zoomToBounds(-0.1);
                break;
            case Key.B: // B
                Application.currentView.logic.setState(new BoxSelectionState());

                // TODO HACK?
                if (Input.isKeyDown(Key.Control))
                    Application.currentView.logic.currentState.artBounds = true;
                Application.redrawAllViews();
                break;
            case Key.AlphaAdd: // +
                LineManipulator.scaleSelection(2);
                break;
            case Key.AlphaSubtract: // -
                LineManipulator.scaleSelection(0.5);
                break;

        }
    }
    keyUp(keycode) {

    }

    arrowMovement(x, y, shiftDown, ctrlDown) {
        if (!Application.currentView.selection.isEmpty()) {
            let stepSize = 10;
            if (shiftDown)
                stepSize = 1;
            if (ctrlDown)
                stepSize = 100;

            let delta = new Vector2(x * stepSize, y * stepSize);
            Application.currentView.selection.moveBy(delta);
            Application.redrawAllViews();
        }
    }
}