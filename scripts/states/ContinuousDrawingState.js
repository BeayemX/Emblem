﻿class ContinuousDrawingState extends State {
    enter() {
        this.lineStart;
        this.lineEnd;
        this.oldPos;

        this.used = false;
    }

    exit() {

    }

    update() {
        this.used = true;
        if (!Application.currentView.LMBDown)
            return;

        if (this.oldPos != undefined) {
            this.lineStart = this.oldPos.copy();
            this.lineEnd = Application.shouldSnap() ? Application.cursor.currentPosition : Application.cursor.position.copy();


            if (this.lineStart.x != this.lineEnd.x || this.lineStart.y != this.lineEnd.y)
                Application.currentView.data.addLine(
                    new Line(
                        this.lineStart.x,
                        this.lineStart.y,
                        this.lineEnd.x,
                        this.lineEnd.y
                        ));
        }

        if (Application.shouldSnap())
            this.oldPos = Application.cursor.currentPosition;
        else
            this.oldPos = Application.cursor.position.copy();
    }

    toString() {
        return "ContinuousDrawingState";
    }
}