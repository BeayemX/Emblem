﻿class DrawingState extends State {
    enter() {
        this.startPos = Application.cursor.currentPosition;
    }

    exit() {
        if (this.canceled)
            return;

        let start = this.startPos;
        let end = Application.cursor.currentPosition;

        if (start.x != end.x || start.y != end.y) {
            Application.currentView.data.addLine(new Line(start, end));
            Application.currentView.data.changed();
        }
    }

    cancel() {
        this.canceled = true;
        Application.currentView.logic.setState(new IdleState());
    }

    toString() {
        return "DrawingState";
    }
}