﻿class RenderBatcher {
    constructor() {
        this.layerBatches = []; // TODO use depth or sorting order instead of layers? e.g. could also be used for outlines or later on object sorting
        this.currentLayerBatch = null;
    }

    reset() {
        this.layerBatches = [];
        this.addNewLayer();
    }
    addNewLayer() {
        this.currentLayerBatch = new LayerBatch();
        this.layerBatches.push(this.currentLayerBatch);
    }

    addLine(line) {
        this.currentLayerBatch.addLine(line);
    }

    log() {
        let keys;

        for (let layerBatch of this.layerBatches) {
            console.log(layerBatch)

            for (let colorBatch of layerBatch.colorBatches) {
                keys = [];

                for (let key in colorBatch)
                    if (colorBatch.hasOwnProperty(key))
                        console.log(key)

                for (let key of keys)
                    console.log(key)
            }
        }
    }
}
class LayerBatch {
    constructor() {
        this.colorBatches = [];
    }

    addLine(line) {
        return;
        if (!this.colorBatches[line.color.toString()]) {
            this.colorBatches[line.color.toString()] = [];
            this.colorBatches[line.color.toString()][line.thickness] = [];
            console.log("if");
        }
        else if (!this.colorBatches[line.color.toString()][line.thickness]) {

            this.colorBatches[line.color.toString()][line.thickness] = [];
            console.log("else if");
        }
        else {
            console.log("else");
        }

        this.colorBatches[line.color.toString()][line.thickness].push(line);
    }
}