﻿// TODO should all objects also have a brush? instead of color and thickness member?
// how about lines regarding fillColor?
class Brush {
    constructor() {
        this.lineColor = Color.black();
        this.lineThickness = 1;

        this.fillColor = Color.transparent();
    }
}
