﻿class PanState extends State {
    enter() {

    }

    exit() {

    }

    update(screenPosDelta)
    {
        Application.currentView.camera.position = Application.currentView.camera.position.addVector(screenPosDelta.divide(Application.currentView.camera.zoom));
        Application.redrawAllViews();
    }

    toString() {
        return "PanState";
    }
}