﻿class BoxSelectionState extends State {
    enter() {

    }

    exit() {

    }

    start(selectType) {
        this.selectionType = selectType;
        this.startPos = Application.cursor.position.copy();
        this.currentPos = Application.cursor.position.copy();
    }

    execute() {
        let min = {
            x: Math.min(this.startPos.x, this.currentPos.x),
            y: Math.min(this.startPos.y, this.currentPos.y)
        };
        let max = {
            x: Math.max(this.startPos.x, this.currentPos.x),
            y: Math.max(this.startPos.y, this.currentPos.y)
        };

        // HACK make own state
        // Used for ArtBounds
        if (this.artBounds) {
            Preferences.artBounds.left = min.x;
            Preferences.artBounds.top = min.y;
            Preferences.artBounds.width = max.x - min.x;
            Preferences.artBounds.height = max.y - min.y;
            return;
        }

        // Used for selection
        let selectionType = this.selectionType;
        if (Input.isKeyDown(Key.Shift) && !this.initializedWithRMBDown)
            selectionType = !selectionType;

        let selectionBounds = new Bounds(min, max);
        let changePoints = [];

        if (Preferences.selectVertices && Preferences.selectEdges) {
            // TODO weird number but should be a third?
            let lines = selectionType ?
                Application.currentView.data.lines.concat(Application.currentView.selection.data.partialLines)
                : Application.currentView.selection.data.lines.concat(Application.currentView.selection.data.partialLines);

            let s, e, m, _m;
            for (let line of lines) {
                s = selectionBounds.contains(line.start);
                e = selectionBounds.contains(line.end);
                _m = line.start.position.addVector(line.end.position);
                _m = _m.divide(2);
                _m = new LineEnding(_m.x, _m.y, line);
                m = selectionBounds.contains(_m);

                if (s && !m && !e)
                    changePoints.push(line.start);
                else if (!s && !m && e)
                    changePoints.push(line.end);
                else if (m || selectionBounds.contains(line)) {
                    changePoints.push(line.start);
                    changePoints.push(line.end);
                }
            }
        } else if (Preferences.selectEdges) {
            let lines = selectionType ?
                Application.currentView.data.lines.concat(Application.currentView.selection.data.partialLines)
                : Application.currentView.selection.data.lines.concat(Application.currentView.selection.data.partialLines);
            for (let l of lines) {
                if (selectionBounds.contains(l)) {
                    changePoints.push(l.start);
                    changePoints.push(l.end);
                }
            }

        } else if (Preferences.selectVertices) {
            let points = selectionType ?
                Utilities.linesToLineEndings(Application.currentView.data.lines).concat(Application.currentView.selection.getUnselectedPointsOfPartialLines())
                : Application.currentView.selection.getAllSelectedPoints();

            for (let p of points)
                if (selectionBounds.contains(p))
                changePoints.push(p);
        }     

        if (selectionType)
            Application.currentView.selection.selectPoints(changePoints);
        else 
            Application.currentView.selection.deselectPoints(changePoints);

        // prefab instances
        let instancesToChangeSelection = [];

        if (selectionType) {
            for (let instance of Application.currentView.data.instances)
                if (instance.boxSelectionHit(selectionBounds))
                    instancesToChangeSelection.push(instance);

            for (let instance of instancesToChangeSelection) {
                Application.currentView.selection.data.addInstance(instance);
                Application.currentView.data.removeInstance(instance);
            }
        }
        else {
            for (let instance of Application.currentView.selection.data.instances)
                if (instance.boxSelectionHit(selectionBounds))
                    instancesToChangeSelection.push(instance);

            for (let instance of instancesToChangeSelection) {
                Application.currentView.data.addInstance(instance);
                Application.currentView.selection.data.removeInstance(instance);
            }
        }
    }

    cancel() {
        Application.currentView.logic.setState(new IdleState());
    }

    keyDown(keycode) {
        switch (keycode) {
            case Key.Enter:
            case Key.Escape:
            case Key.B:
                this.cancel();
                break;
        }
    }

    toString() {
        return "BoxSelectionState";
    }
}