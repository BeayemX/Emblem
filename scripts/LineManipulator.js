// SIFU TODO merge with Utilities?
// TODO if not other file name
class LineManipulator {

    static init() {
        console.log("LineManipulator created.");
    }

    static subdivide() {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        var selectedLines = selection.data.lines;
        var newLines = [];
        for (var i = 0; i < selectedLines.length; ++i) {
            let midPoint = selectedLines[i].end.position.addVector(selectedLines[i].start.position).multiply(0.5);

            newLines.push(new Line(selectedLines[i].start.position.x, selectedLines[i].start.position.y, midPoint.x, midPoint.y, selectedLines[i].color, selectedLines[i].thickness));
            newLines.push(new Line(midPoint.x, midPoint.y, selectedLines[i].end.position.x, selectedLines[i].end.position.y, selectedLines[i].color, selectedLines[i].thickness));
        }
        selection.data.lines = [];
        selection.data.lines = newLines;
        selection.data.changed();
        Application.redrawAllViews();
    }
    // TODO move to selection?
    static rotateSelectionBy(degrees, rotationCenter) {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        let selAsPoints = selection.getAllSelectedPoints();
        if (rotationCenter == undefined) {
            if (Preferences.usePivot)
                rotationCenter = Globals.pivot;
            else
                rotationCenter = Utilities.calculateCenterOfMass(selection.data.getAllPositions());
        }

        for (let p of selAsPoints)
            p.position = this.rotatePositionByAngle(p.position, rotationCenter, degrees);

        for (let instance of selection.data.instances) {
            instance.transform.position = this.rotatePositionByAngle(instance.transform.position, rotationCenter, degrees);
            if (!Preferences.onlyTransformPrefabInstancePosition)
                instance.transform.rotation += degrees;
        }

        Application.redrawAllViews();
    }

    static rotatePositionByAngle(position, rotationCenter, degrees) {
        let angle = degrees * (Math.PI / 180)

        let newPos = position.subtractVector(rotationCenter)

        // selected points

        //let a = Math.atan2(pos.y, pos.x);
        //a += radAngle;
        let x = newPos.x * Math.cos(angle) - newPos.y * Math.sin(angle);
        let y = newPos.x * Math.sin(angle) + newPos.y * Math.cos(angle)
        newPos.x = x;
        newPos.y = y;
        newPos = newPos.addVector(rotationCenter);

        return newPos;
    }

    // TODO move to selection?
    static scaleSelection(scaleVector) {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        // numeric values
        if (!isNaN(scaleVector))
            scaleVector = new Vector2(scaleVector, scaleVector);

        let center;

        if (Preferences.usePivot)
            center = Globals.pivot;
        else
            center = Utilities.calculateBBCenter(selection.data.getAllPositions());

        let selectionAsPoints = selection.getAllSelectedPoints();
        for (let p of selectionAsPoints)
            p.position = this.scalePoint(p.position, center, scaleVector);

        for (let instance of selection.data.instances) {
            instance.transform.position = this.scalePoint(instance.transform.position, center, scaleVector);
            if (!Preferences.onlyTransformPrefabInstancePosition)
            {
                instance.transform.scale = instance.transform.scale.rotateAround(Vector2.zero, -instance.transform.rotation);
                instance.transform.scale = this.scalePoint(instance.transform.scale, Vector2.zero, scaleVector);
                instance.transform.scale = instance.transform.scale.rotateAround(Vector2.zero, instance.transform.rotation);
            }
        }

        Application.redrawAllViews();
    }

    static scalePoint(position, center, scaleVector) {
        // numeric values
        if (!isNaN(scaleVector))
            scaleVector = new Vector2(scaleVector, scaleVector);

        return position
            .subtractVector(center)
            .multiplyVector(scaleVector)
            .addVector(center);
        //Application.redrawAllViews();
    }

    static growSelection() {
        Application.currentView.growSelection(true);
    }

    static selectLinked() {
        Application.currentView.selectLinked();
    }


    // TODO RENAME resetPositionForSelectedInstances() and move to selection?
    static resetPosition() {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        for (let instance of selection.data.instances)
            instance.transform.position = Vector2.zero;
    }
    static resetRotation() {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        for (let instance of selection.data.instances)
            instance.transform.rotation = 0;
    }
    static resetScale() {
        let selection = Application.prefabEditorView.prefab ? Application.currentView.selection : Application.sceneView.selection;
        for (let instance of selection.data.instances)
            instance.transform.scale = new Vector2(1, 1);
    }
}