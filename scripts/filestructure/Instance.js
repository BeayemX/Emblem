﻿class PrefabInstance extends EObject {
    constructor(prefabReference, position, rotation, scale) {
        super(position, rotation, scale);
        this.reference = prefabReference;
    }
    
    equals(other) {
        return this.reference == other.reference && this.transform.equals(other.transform);
    }

    copy() {
        return new PrefabInstance(this.reference, this.transform.position, this.transform.rotation, this.transform.scale);
    }

    selectionHit() {
        for (let line of this.getAsLines())
            if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range)
                return true;

        for (let instance of this.reference.data.instances)
        {
            for (let line of instance.getAsLines()) {
                line.start.position = this.objectSpaceForPosition(line.start.position);
                line.end.position = this.objectSpaceForPosition(line.end.position);

                if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range)
                    return true;
            }
        }

        return false;
    }

    boxSelectionHit(bounds) {
        for (let line of this.getAsLines())
            if (bounds.contains(line.start) || bounds.contains(line.end))
            return true;

        for (let instance of this.reference.data.instances)
        {
            for (let line of instance.getAsLines()) {
                line.start.position = this.objectSpaceForPosition(line.start.position);
                line.end.position = this.objectSpaceForPosition(line.end.position);

                if (bounds.contains(line.start) || bounds.contains(line.end))
                    return true;
            }
        }

        return false;
    }

    get bounds() {
        let min = new Vector2(Infinity, Infinity);
        let max = new Vector2(-Infinity, -Infinity);

        for (let line of this.lines) {
            min.x = Math.min(min.x, line.start.transform.position.x);
            min.y = Math.min(min.y, line.start.transform.position.y);
            max.x = Math.max(max.x, line.start.transform.position.x);
            max.y = Math.max(max.y, line.start.position.y);

            min.x = Math.min(min.x, line.end.transform.position.x);
            min.y = Math.min(min.y, line.end.transform.position.y);
            max.x = Math.max(max.x, line.end.transform.position.x);
            max.y = Math.max(max.y, line.end.transform.position.y);
        }

        return new Bounds(min.addVector(this.transform.position), max.addVector(this.transform.position));
    }

    getAsLines() {
        let lines = [];

        for (let line of this.reference.data.lines) {
            lines.push(new Line(
                this.objectSpaceForPosition(line.start.position),
                this.objectSpaceForPosition(line.end.position),
                line.color,
                line.thickness
            ));}

        for (let instance of this.reference.data.instances) {
            for (let line of instance.getAsLines()) {
                lines.push(new Line(
                    this.objectSpaceForPosition(line.start.position),
                    this.objectSpaceForPosition(line.end.position),
                    line.color,
                    line.thickness
                ));
            }
        }

        // if current prefab in editor, also use selection of edit view
        if (Application.prefabEditorView.prefab == this.reference) {
            for (let line of Application.prefabEditorView.getSelectionAsLines())
                lines.push(new Line(
                    this.objectSpaceForPosition(line.start.position),
                    this.objectSpaceForPosition(line.end.position),
                    line.color,
                    line.thickness
                ));
        }

        return lines;
    }

    // TODO implement me should return
    /*
    dissolve() {
        // return in this.objectSpaceForPosition?
        // what happens to scale and rotation?
        return this.data();
    } // */

}