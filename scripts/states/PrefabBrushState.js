﻿class PrefabBrushState extends State {
    constructor(prefab) {
        super();    
        this.currentPrefab = prefab;

    }

    enter() {
    }

    exit() {

    }

    // TODO also possibility to use instance.dissolve() (eg. while Control is pressed) to keep nested prefabs as instances
    place(dontPlacePrefabButDuplicateEachLineIndividually) {
        if (this.currentPrefab.includesNested(Application.prefabEditorView.prefab)
            && Application.currentView == Application.prefabEditorView)
            return;

        if (dontPlacePrefabButDuplicateEachLineIndividually) {
            // TODO addLines(lines)
            for (let line of this.currentPrefab.getAsLines())
               Application.currentView.data.addLine(line.copy().move(Application.cursor.currentPosition));
        } else {
            Application.currentView.data.addInstance(new PrefabInstance(this.currentPrefab, Application.cursor.currentPosition));
        }
        Application.currentView.data.changed();
    }

    cancel() {
        Application.logic.setState(new IdleState());
        Application.redrawAllViews();
        GUI.buildPrefabUI();
    }

    keyDown(keycode) {
        switch (keycode) {
            case Key.D:
                this.place();
                break;
        }
    }

    
    toString() {
        return "Prefab Brush State";
    }
}