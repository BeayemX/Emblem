﻿class Bounds {
    constructor(topLeft, bottomRight) {
        this.left = topLeft.x;
        this.top = topLeft.y;
        this.right = bottomRight.x;
        this.bottom = bottomRight.y;
    }

    contains(pos) {
        if (pos instanceof Vector2)
            return this.left <= pos.x && pos.x <= this.right &&
            this.top <= pos.y && pos.y <= this.bottom;
        else if (pos instanceof LineEnding) {
            return this.left - pos.line.thickness <= pos.position.x && pos.position.x <= this.right + pos.line.thickness &&
            this.top - pos.line.thickness <= pos.position.y && pos.position.y <= this.bottom + pos.line.thickness;
        }
        else if (pos instanceof Line) {
            return this.containsLine(pos.start.position, pos.end.position, pos.thickness);
        }

    }

    _computeOutCode(point, halfThickness) {
        let outCode = 0;
        if (point.x < this.left - halfThickness)
            outCode |= 1;
        else if (point.x > this.right + halfThickness)
            outCode |= 2;
        if (point.y < this.top - halfThickness)
            outCode |= 4;
        else if (point.y > this.bottom + halfThickness)
            outCode |= 8;
        return outCode;
    }

    containsLine(p0, p1, thickness) {
        let halfThickness = thickness * 0.5;
        let outcode0 = this._computeOutCode(p0, halfThickness);
        let outcode1 = this._computeOutCode(p1, halfThickness);
        let accept = false;
        while (true) {
            if ((outcode0 | outcode1) == 0)
                return true;
            else if ((outcode0 & outcode1) != 0)
                return false;
            else {
                p0 = p0.copy();
                p1 = p1.copy();
                let x, y;
                let outcodeOut = (outcode0 != 0) ? outcode0 : outcode1;

                if ((outcodeOut & 8) != 0) {
                    let bottom = this.bottom + halfThickness;
                    x = p0.x + (p1.x - p0.x) * (bottom - p0.y) / (p1.y - p0.y);
                    y = bottom;
                }
                else if ((outcodeOut & 4) != 0) {
                    let top = this.top - halfThickness;
                    x = p0.x + (p1.x - p0.x) * (top - p0.y) / (p1.y - p0.y);
                    y = top;
                }
                else if ((outcodeOut & 2) != 0) {
                    let right = this.right + halfThickness;
                    y = p0.y + (p1.y - p0.y) * (right - p0.x) / (p1.x - p0.x);
                    x = right;
                }
                else// if ((outcodeOut & 1) != 0)
                {
                    let left = this.left - halfThickness;
                    y = p0.y + (p1.y - p0.y) * (left - p0.x) / (p1.x - p0.x);
                    x = left;
                }

                if (outcodeOut == outcode0) {
                    p0.x = x;
                    p0.y = y;
                    outcode0 = this._computeOutCode(p0, halfThickness);
                }
                else {
                    p1.x = x;
                    p1.y = y;
                    outcode1 = this._computeOutCode(p1, halfThickness);
                }
            }
        }
        return true;
    }



    shrink(margin) {
        let b = new Bounds((new Vector2(this.left, this.top)).add(margin), (new Vector2(this.right, this.bottom)).subtract(margin));
        return b;
    }

    get width() {
        return this.right - this.left;
    }

    get height() {
        return this.bottom - this.top;
    }

    toString() {
        return "Bounds (" + this.left + "|" + this.top + ") , (" + this.right + "|" + this.bottom + ")";
    }
}