﻿class GridManager {

    static init() {
        console.log("GridManager created.");

        this.rectGrid = new RectangleGrid();
        this.triGrid = new TriangleGrid();
        this.polarGrid = new PolarGrid();

        waitingForStart.push(this);
    }
    static start() {
        this.updateGrid();
    }

    static setGridType(type) {
        Preferences.gridType = type;
        this.updateGrid();
    }
    static resetGrid() {
        if (Preferences.gridType == 0)
            this.rectGrid = new RectangleGrid();
        else if (Preferences.gridType == 1)
            this.triGrid = new TriangleGrid();
        else if (Preferences.gridType == 2)
            this.polarGrid = new PolarGrid();

        this.updateGrid();
    }

    static updateGrid() {
        if (Preferences.gridType == 0)
            GridManager.grid = this.rectGrid;
        else if (Preferences.gridType == 1)
            GridManager.grid = this.triGrid;
        else if (Preferences.gridType == 2)
            GridManager.grid = this.polarGrid;

        GUI.gridTypeSelection.value = Preferences.gridType;
        GUI.genereateGridSettings();
        Application.redrawAllViews();
    }
}