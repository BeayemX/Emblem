﻿class BrushSelectionState extends State {
    enter() {
        this.cursorRange = Application.cursor.range;
    }

    exit() {
        Application.cursor.range = this.cursorRange;
    }

    update() {
        if (Application.currentView.LMBDown == true || this.initializedWithRMBDown) {
            let newSelPoints = [];
            // TODO unify with normal mouse selection (and deselection) including this there are 3 similar calls... (two in this class select/deselect. one in View)
            if (Preferences.selectVertices && Preferences.selectEdges) {
                let s, e, m, _m;
                for (let line of Application.currentView.data.lines.concat(Application.currentView.selection.data.partialLines)) {
                    s = Application.cursor.intersect(line.start);
                    e = Application.cursor.intersect(line.end);
                    _m = line.start.position.addVector(line.end.position);
                    _m = _m.divide(2);
                    _m = new LineEnding(_m.x, _m.y, line);
                    m = Application.cursor.intersect(_m);

                    if (s && !m && !e)
                        newSelPoints.push(line.start);
                    else if (!s && !m && e)
                        newSelPoints.push(line.end);
                    else if (m) { // if one line ending and the middle of the line is within cursor range select whole line. otherwise weird selection happens when whole line is inside cursor range
                        newSelPoints.push(line.start);
                        newSelPoints.push(line.end);
                    } else if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range + (line.thickness * 0.5)) {
                        if (Application.cursor.withinTimesRange(line.start, 3))
                            newSelPoints.push(line.start);
                        else if (Application.cursor.withinTimesRange(line.end, 3))
                            newSelPoints.push(line.end);
                        else {
                            newSelPoints.push(line.start);
                            newSelPoints.push(line.end);
                        }
                    }
               }
            } else if (Preferences.selectEdges) {
                for (let line of Application.currentView.data.lines.concat(Application.currentView.selection.data.partialLines)) {
                    if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range + (line.thickness * 0.5)) {
                        newSelPoints.push(line.start);
                        newSelPoints.push(line.end);
                    }
                }
            } else if (Preferences.selectVertices) {
                for (let line of Application.currentView.data.lines) {
                    if (Application.cursor.intersect(line.start));
                        newSelPoints.push(line.start);
                    if (Application.cursor.intersect(line.end))
                        newSelPoints.push(line.end);
                }
                for (let line of Application.currentView.selection.data.partialLines) {
                    if (Application.cursor.intersect(line.start))
                        newSelPoints.push(line.start);
                    if (Application.cursor.intersect(line.end))
                        newSelPoints.push(line.end);
                }
            }

            Application.currentView.selection.selectPoints(newSelPoints);

            let instancesToChangeSelection = [];
            // prefab instances
            for (let instance of Application.currentView.data.instances)
                if (instance.selectionHit())
                    instancesToChangeSelection.push(instance);
            for (let instance of instancesToChangeSelection) {
                Application.currentView.selection.data.addInstance(instance);
                Application.currentView.data.removeInstance(instance);
            }

        } else if (Application.currentView.MMBDown == true) {
            let newUnselPoints = [];

            if (Preferences.selectVertices && Preferences.selectEdges) {
                let s, e, m, _m;
                for (let line of Application.currentView.selection.data.lines.concat(Application.currentView.selection.data.partialLines)) {
                    s = Application.cursor.intersect(line.start);
                    e = Application.cursor.intersect(line.end);
                    _m = line.start.position.addVector(line.end.position);
                    _m = _m.divide(2);
                    _m = new LineEnding(_m.x, _m.y, line);
                    m = Application.cursor.intersect(_m);

                    if (s && !m && !e)
                        newUnselPoints.push(line.start);
                    else if (!s && !m && e)
                        newUnselPoints.push(line.end);
                    else if (m) { // if one line ending and the middle of the line is within cursor range select whole line. otherwise weird selection happens when whole line is inside cursor range
                        newUnselPoints.push(line.start);
                        newUnselPoints.push(line.end);
                    } else if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range + (line.thickness * 0.5)) {
                        if (Application.cursor.withinTimesRange(line.start, 3))
                            newUnselPoints.push(line.start);
                        else if (Application.cursor.withinTimesRange(line.end, 3))
                            newUnselPoints.push(line.end);
                        else {
                            newUnselPoints.push(line.start);
                            newUnselPoints.push(line.end);
                        }
                    }
                }
            } else if (Preferences.selectEdges) {
                for (let line of Application.currentView.selection.data.lines.concat(Application.currentView.selection.data.partialLines)) {
                    if (line.distanceToPoint(Application.cursor.position) <= Application.cursor.range + (line.thickness * 0.5)) {
                        newUnselPoints.push(line.start);
                        newUnselPoints.push(line.end);
                    }
                }
            }
            else if (Preferences.selectVertices) {
                for (let line of Application.currentView.selection.data.lines) {
                    if (Application.cursor.intersect(line.start))
                        newUnselPoints.push(line.start);
                    if (Application.cursor.intersect(line.end))
                        newUnselPoints.push(line.end);
                }

                for (let line of Application.currentView.selection.data.partialLines) {
                    if (Application.cursor.intersect(line.start))
                        newUnselPoints.push(line.start);
                    if (Application.cursor.intersect(line.end))
                        newUnselPoints.push(line.end);
                }
            }

            Application.currentView.selection.deselectPoints(newUnselPoints);

            // prefab instances
            let instancesToChangeSelection = [];
            for (let instance of Application.currentView.selection.data.instances)
                if (instance.selectionHit())
                    instancesToChangeSelection.push(instance);

            for (let instance of instancesToChangeSelection) {
                Application.currentView.data.addInstance(instance);
                Application.currentView.selection.data.removeInstance(instance);
            }
        }
    }

    cancel() {
        Application.currentView.logic.setState(new IdleState());
    }


    keyDown(keycode) {
        switch (keycode) {
            case Key.Enter:
            case Key.Escape:
            case Key.C:
                this.cancel();
                break;
        }
    }

    keyUp(keycode) {
    }
}