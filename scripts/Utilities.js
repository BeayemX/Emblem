Utilities = new class {
    deleteArrayEntry(array, entry) {
        for (var i = 0; i < array.length; ++i) {
            if (array[i] == entry) {
                return array.splice(i, 1)[0];
            }
        }
        return null;
    }

    movePointsBy(points, delta) {
        for (let i = 0; i < points.length; ++i) {
            points[i].position = points[i].position.addVector(delta);
        }
    }
    
    calculateBBCenter(points) {
        let min = new Vector2(Infinity, Infinity);
        let max = new Vector2(-Infinity, -Infinity);
        
        for (let point of points) {
            min.x = Math.min(min.x, point.position.x);
            min.y = Math.min(min.y, point.position.y);
            max.x = Math.max(max.x, point.position.x);
            max.y = Math.max(max.y, point.position.y);
        }

        let center = new Vector2(
            (max.x + min.x) * 0.5,
            (max.y + min.y) * 0.5
            );

        return center;
    }

    calculateCenterOfMass(points) {
        let centerOfMass = new Vector2(0, 0);

        for (let point of points) {
            centerOfMass.x += point.position.x;
            centerOfMass.y += point.position.y;
        }
        centerOfMass = centerOfMass.divide(points.length);

        return centerOfMass;
    }

    snapPointsToGrid(points) {
        for (let p of points)
            p.position = GridManager.grid.getNearestPointFor(p.position);
    }

    cutWithSelection() {
        let selection = Application.currentView.selection;
        let selLines = selection.data.lines.concat(selection.data.partialLines);

        for (let line of selLines) {
            Utilities.cutLines(line, Application.currentView.data.lines, false);
        }
        Application.redrawAllViews();
    }

    cutLines(cutter, lines, useDrawnLineAsRealLine) {
        let intersections = [];
        intersections.push(cutter.start);
        this._addPointSorted(intersections, cutter.end);
        let n = lines.length;

        for (let i = n - 1; i >= 0; i--) {
            let points = this._intersect(cutter, lines[i]);
            if (points.length == 1) {
                if (!points[0].position.equals(lines[i].end.position) && !points[0].position.equals(lines[i].start.position)) {
                    lines.push(new Line(points[0].position.copy(), lines[i].end.position.copy(), lines[i].color, lines[i].thickness));
                    lines[i].end.position = points[0].position.copy();
                }
            }
            else if (points.length > 1)
                lines.splice(i, 1);

            for (let point of points)
                this._addPointSorted(intersections, point);
        }

        let origPts = 0;
        for (let i = 0; i < intersections.length - 1; i++) {
            if (intersections[i].position.equals(cutter.start.position) ||
                intersections[i].position.equals(cutter.end.position))
                origPts++;
            let o = origPts < 1 ? intersections[i].line : (origPts > 1 ? intersections[i + 1].line : cutter);
            if (useDrawnLineAsRealLine || o != cutter)
                lines.push(new Line(intersections[i].position.copy(), intersections[i + 1].position.copy(), o.color, o.thickness));
        }
    }

    _addPointSorted(points, point) {
        for (let i = 0; i < points.length; i++) {
            if (this.eq(point.position.y, points[i].position.y) && this.eq(point.position.x, points[i].position.x)) {
                if (points[i].line == null)
                    points[i].line = point.line;
                return;
            }
            if (point.position.x > points[i].position.x ||
                (this.eq(point.position.x, points[i].position.x) && point.position.y > points[i].position.y)) {
                points.splice(i, 0, point);
                return;
            }
        }
        points.push(point);
    }

    // TODO make part of line class?
    _intersect(line1, line2) {
        let points = [];
        let v1 = line1.end.position.subtractVector(line1.start.position);
        let v2 = line2.end.position.subtractVector(line2.start.position);
        let ls = -v1.y * (line1.start.position.x - line2.start.position.x) + v1.x * (line1.start.position.y - line2.start.position.y);
        let rs = -v2.x * v1.y + v1.x * v2.y;
        let lt = v2.x * (line1.start.position.y - line2.start.position.y) - v2.y * (line1.start.position.x - line2.start.position.x);
        let rt = -v2.x * v1.y + v1.x * v2.y;
        let s = ls / rs;
        let t = lt / rt;

        if (this.eq(ls, 0) && this.eq(rs, 0) && this.eq(lt, 0) && this.eq(rt, 0)) {
            let minX = Math.min(line1.start.position.x, line1.end.position.x, line2.start.position.x, line2.end.position.x);
            let minY = Math.min(line1.start.position.y, line1.end.position.y, line2.start.position.y, line2.end.position.y);
            let maxX = Math.max(line1.start.position.x, line1.end.position.x, line2.start.position.x, line2.end.position.x);
            let maxY = Math.max(line1.start.position.y, line1.end.position.y, line2.start.position.y, line2.end.position.y);
            if (Math.abs(maxX - minX) <= Math.abs(v1.x) + Math.abs(v2.x) &&
                Math.abs(maxY - minY) <= Math.abs(v1.y) + Math.abs(v2.y)) {

                points.push(line1.start);
                this._addPointSorted(points, line1.end);
                this._addPointSorted(points, line2.start);
                this._addPointSorted(points, line2.end);
            }

            return points;
        }
        if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            points.push(new LineEnding(line1.start.position.x + (t * v1.x), line1.start.position.y + (t * v1.y), null));

        return points;
    }

    linesToLineEndings(lines) {
        let points = [];
        for (let line of lines) {
            points.push(line.start);
            points.push(line.end);
        }
        return points;
    }

    mergePoints(points) {
        let center = this.calculateBBCenter(points);

        for (let p of points)
            p.position = center.copy();
    }

    // TODO make it possible to pass in, position as Vector2 and rotate as angle
    // FIXME problem with adding layers. can only work on sceneView
    makeSketchy(position, positionSteps, angle, angleStep) {
        let originalData = Application.currentView.selection.data.copy();
        Application.file.createNewLayer(true, Application.file.currentLayer.name + "_Sketch");

        if (position && angle) {
            for (var y = -position.y; y <= position.y; ++y) {
                for (var x = -position.x; x <= position.x; ++x) {
                    for (var i = -angle; i <= angle; ++i) {
                        let sel = originalData.copy();
                        Application.currentView.selection.data = sel;
                        Utilities.movePointsBy(Application.currentView.selection.getAllSelectedPoints(), new Vector2(x * positionSteps.x, y * positionSteps.y));
                        LineManipulator.rotateSelectionBy(i * angleStep);
                        Application.currentView.selection.clear();
                    }
                }
            }
        }
        else if (position) {
            for (var y = -position.y; y <= position.y; ++y) {
                for (var x = -position.x; x <= position.x; ++x) {
                    let sel = originalData.copy();
                    Application.currentView.selection.data = sel;
                    Utilities.movePointsBy(Application.currentView.selection.getAllSelectedPoints(), new Vector2(x * positionSteps.x, y * positionSteps.y));
                    Application.currentView.selection.clear();
                }
            }
        }
        else if (angle) {
            for (var i = -angle; i <= angle; ++i) {
                let sel = originalData.copy();
                Application.currentView.selection.data = sel;
                LineManipulator.rotateSelectionBy(i * angleStep);
                Application.currentView.selection.clear();
            }
        }

        Application.currentView.selection.clear();
    }

    // TODO when extruding line multiple times the line and points create the same outer lines...
    extrudeSelection() {
        let selection = Application.currentView.selection;

        if (Application.currentView.logic.currentState instanceof InputRecorderState)
            Application.currentView.logic.currentState.execute();

        let selData = new SelectionData(selection);

        for (let line of selection.data.lines) {
            let newLine = new Line(line.start.position, line.end.position, line.color, line.thickness);
            selData.addLine(newLine);

            newLine = new Line(line.start.position, line.start.position, line.color, line.thickness);
            selData.addPoint(newLine.end);

            newLine = new Line(line.end.position, line.end.position, line.color, line.thickness);
            selData.addPoint(newLine.end);
        }

        for (let point of selection.data.points) {
            let line = new Line(point.position, point.position, point.line.color, point.line.thickness);
            selData.addPoint(line.end);
        }

        selection.clear();
        selection.data = selData;

        Application.currentView.logic.setState(new MoveLinesState());
    }

    fill(points) {
        if (points.length > 20)
            if (!confirm("Are you sure you want to contine with the fill operation with " + points.length + " points. This could take a while to compute."))
                return;


        let newSelData = new SelectionData(Application.currentView.selection);
        for (let i = 0; i < points.length; ++i)
            for (let j = i + 1; j < points.length; ++j)
                if (i != j && !points[i].line.equals(points[j].line))
                    newSelData.addLine(new Line(points[i].position.copy(), points[j].position.copy(), points[i].line.color, points[i].line.thickness));

        Application.currentView.selection.clear();
        Application.currentView.selection.data = newSelData;
        Application.file.updateStats();
        Application.redrawAllViews();
    }

    makeCircle(center, radius, segments, overshoot) {
        let stepSize = 360 / segments;
        let rad = stepSize * (Math.PI / 180);
        for (let i = 0; i < segments; ++i) {
            let start = new Vector2(Math.cos(rad * i), Math.sin(rad * i));
            let end = new Vector2(Math.cos(rad * (i + 1)), Math.sin(rad * (i + 1)));

            start = start.multiply(radius);
            end = end.multiply(radius);

            if (overshoot) {
                let direction = end.subtractVector(start).normalized();
                start = start.subtractVector(direction.multiply(overshoot));
                end = end.addVector(direction.multiply(overshoot));
            }

            Application.file.currentLayer.data.lines.push(new Line(start, end, Application.currentBrush.lineColor, Application.currentBrush.lineThickness));
        }

        Application.file.updateStats();
        Application.redrawAllViews();
    }

    eq(a, b) {
        let margin = 0.1;
        let diff = Math.abs(a - b)
        return diff < margin;
    }
}
