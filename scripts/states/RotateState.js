﻿class RotateState extends InputRecorderState {
    constructor() {
        super();
    }
    enter() {
        // BUT maybe not needed if inherited from inputrecorder...
        // todo maybe pass this as argument to inputrecorder-constructor and call 'this'.methods()?
        this.startPos = Application.cursor.position.copy();
        this.currentPos = Application.cursor.position.copy();
        this.updateValue();
    }

    exit() {
    }

    updateValue(number) {
        let text = "Rotate: " + ((Math.floor(this.getAngle() * 1000) / 1000.0)) + "°";
        GUI.writeToStatusbarLeft(text);
        Application.redrawAllViews();
    }

    execute() {
        LineManipulator.rotateSelectionBy(this.getAngle());
    }

    cancel() {
        Application.currentView.logic.setState(new IdleState());
        Application.redrawAllViews();
    }

    getRotationFromMousePosition() {
        // TODO PERFORMANCE calculate center is calculated on every mouse update. maybe save somewhere?
        let rotationCenter;
        if (Preferences.usePivot)
            rotationCenter = Globals.pivot;
        else
            rotationCenter = Utilities.calculateCenterOfMass(Application.currentView.selection.data.getAllPositions());

        let p1 = Application.currentView.logic.currentState.startPos.subtractVector(rotationCenter);
        let p2 = Application.currentView.logic.currentState.currentPos.subtractVector(rotationCenter);

        let startAngle = Math.atan2(p1.y, p1.x); // this
        let currAngle = Math.atan2(p2.y, p2.x); // this

        let value = (currAngle - startAngle) * 180 / Math.PI;

        if (Input.isKeyDown(Key.Control)) {
            let snapFactor = 5;
            value = (Math.floor(value / snapFactor)) * snapFactor;
        } else if (Input.isKeyDown(Key.Shift)) {
            let snapFactor = 22.5;
            value = (Math.floor(value / snapFactor)) * snapFactor;
        }

        return value;
    }

    keyDown(keycode) {
        super.record(keycode);

        switch (keycode) {
            case Key.D:
                LineManipulator.rotateSelectionBy(this.getAngle());
                Application.currentView.selection.duplicate();
                LineManipulator.rotateSelectionBy(-this.getAngle());
                break;
            case Key.K:
                LineManipulator.rotateSelectionBy(this.getAngle());
                Utilities.cutWithSelection();
                LineManipulator.rotateSelectionBy(-this.getAngle());
                break;
        }
    }

    keyUp(keycode) {

    }

    getAngle() {
        let angle;

        if (isNaN(this.number))
            angle = Application.currentView.logic.currentState.getRotationFromMousePosition();
        else
            angle = this.number;

        return angle;
    }

    toString() {
        return "RotateState";
    }
}