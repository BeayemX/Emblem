﻿class MoveLinesState extends InputRecorderState {
    constructor() {
        super();
    }

    enter() {
        this.startPos = Application.cursor.position.copy();
        this.currentPos = Application.cursor.position.copy();
    }

    exit() {

    }

    // mouse moved --> called without parameters
    updateValue() {
        let delta = this.getDelta();
        let text = "Position delta: " + delta.toString();
        if (Application.currentView.logic.currentState.axisLock)
            text += " | Lock: " + Application.currentView.logic.currentState.axisLock;
        GUI.writeToStatusbarLeft(text);

        Application.redrawAllViews();
    }

    execute() {
        Application.currentView.selection.moveBy(this.getDelta());
    }

    cancel() {
        Application.currentView.logic.setState(new IdleState());
        Application.redrawAllViews();
    }

    getPositionDeltaFromMousePosition() {

        let delta = this.currentPos.subtractVector(this.startPos);

        if (Application.shouldSnap()) {
            let currGridSnapPos = GridManager.grid.getNearestPointFor(this.currentPos);
            let currGridStartSnapPos = GridManager.grid.getNearestPointFor(this.startPos);
            delta = currGridSnapPos.subtractVector(currGridStartSnapPos);

        }

        if (this.axisLock == "X")
            delta.y = 0;
        else if (this.axisLock == "Y")
            delta.x = 0;

        return delta;
    }

    keyDown(keycode) {
        super.record(keycode);

        switch (keycode) {
            case Key.D:
                Utilities.movePointsBy(Application.currentView.selection.data.getAllPositions(), this.getDelta());
                Application.currentView.selection.duplicate();
                Utilities.movePointsBy(Application.currentView.selection.data.getAllPositions(), this.getDelta().flipped());
                break;
            case Key.K:
                Utilities.movePointsBy(Application.currentView.selection.data.getAllPositions(), this.getDelta());
                Utilities.cutWithSelection();
                Utilities.movePointsBy(Application.currentView.selection.data.getAllPositions(), this.getDelta().flipped());
                break;
        }
    }

    keyUp(keycode) {

    }

    getDelta() {
        let delta;

        if (isNaN(this.number)) 
            delta = Application.currentView.logic.currentState.getPositionDeltaFromMousePosition();
        else 
            delta = new Vector2(this.number, this.number);

        if (this.axisLock == "X")
            delta.y = 0;
        else if (this.axisLock == "Y")
            delta.x = 0;

        return delta;
    }

    toString() {
        return "MoveLinesState";
    }
}