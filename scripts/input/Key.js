﻿Key = new class {
    constructor() {
        this.A = 65;
        this.B = 66;
        this.C = 67;
        this.D = 68;
        this.E = 69;
        this.F = 70;
        this.G = 71;
        this.H = 72;
        this.I = 73;
        this.J = 74;
        this.K = 75;
        this.L = 76;
        this.M = 77;
        this.N = 78;
        this.O = 79;
        this.P = 80;
        this.Q = 81;
        this.R = 82;
        this.S = 83;
        this.T = 84;
        this.U = 85;
        this.V = 86;
        this.W = 87;
        this.X = 88;
        this.Y = 89;
        this.Z = 90;

        this.Space = 32;
        this.Backspace = 8;
        this.Tab = 9;
        this.Enter = 13;
        this.Shift = 16
        this.Control = 17;
        this.Alt = 18;
        this.Escape = 27;
        this.Delete = 46;

        this.Comma = 188;
        this.Period = 190;

        this.Alpha0 = 48;
        this.Alpha1 = 49;
        this.Alpha2 = 50;
        this.Alpha3 = 51;
        this.Alpha4 = 52;
        this.Alpha5 = 53;
        this.Alpha6 = 54;
        this.Alpha7 = 55;
        this.Alpha8 = 56;
        this.Alpha9 = 57;
        this.AlphaAdd = 187;
        this.AlphaSubtract = 189;

        this.ArrowLeft = 37;
        this.ArrowRight = 39;
        this.ArrowUp = 38;
        this.ArrowDown = 40;

        this.Insert = 45;

        this.Num0 = 96;
        this.Num1 = 97;
        this.Num2 = 98;
        this.Num3 = 99;
        this.Num4 = 100;
        this.Num5 = 101;
        this.Num6 = 102;
        this.Num7 = 103;
        this.Num8 = 104;
        this.Num9 = 105;
        this.NumAdd = 107;
        this.NumSubtract = 109;
        this.NumMultiply = 106
        this.NumDivide = 111
        this.NumComma = 110

        this.F1 = 112
        this.F2 = 113
        this.F3 = 114
        this.F4 = 115
        this.F5 = 116
        this.F6 = 117
        this.F7 = 118
        this.F8 = 119
        this.F9 = 120
        this.F10 = 121
        this.F11 = 122
        this.F12 = 123
    }
}