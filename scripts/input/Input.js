class Input {
    static init() {
        console.log("Input created.");
        this.downKeys = [];
    }

    static keyDown(e) {
        if (this.downKeys[e.keyCode] == true) {
            e.preventDefault();
            return;
        }
        else
            this.downKeys[e.keyCode] = true;

        switch (e.keyCode) {
            case Key.S:
                if (Input.isKeyDown(Key.Control)) {
                    if (!Exporter.exportAsSVG())
                        Saver.autoSave();
                    Input.clearKeys();
                    e.preventDefault();
                }
                break;
            case Key.F12:
                if (!Preferences.developerMode == true) {
                    Exporter.renderPNG();
                }
                break;

            default:
                break;
        }

        if (Application.currentFocus instanceof View) {
            Application.currentView.keyDown(e.keyCode);
            Application.currentView.logic.currentState.keyDown(e.keyCode);
        }

        //if (Application.currentFocus)
        //  Application.currentFocus.keyDown(e.keyCode);
            
        // dont prevent default if inside view
        if (Application.currentFocus && !(
            e.keyCode == Key.F12
            || (e.keyCode == Key.L && Input.isKeyDown(Key.Control))
            || e.keyCode == Key.F6
            || e.keyCode == Key.F5
            || e.keyCode != Key.Tab
            )) {
            e.preventDefault();
            }

        // prevent default for these keys
        if (
            e.keyCode == Key.D && this.isKeyDown(Key.Control)
            || e.keyCode == Key.Alt
            || (e.keyCode == Key.R && Input.isKeyDown(Key.Control))
            )
            e.preventDefault();
    }

    static keyUp(e) {
        if (this.downKeys[e.keyCode] == false) {
            e.preventDefault();
            return;
        }
        else
            this.downKeys[e.keyCode] = false;

        switch (e.keyCode) {
            default:
                if (Application.currentFocus instanceof View)
                {
                    Application.currentView.keyUp(e.keyCode);
                    Application.currentView.logic.currentState.keyUp(e.keyCode);
                }
                //if (Application.currentFocus)
                //    Application.currentFocus.keyUp(e.keyCode);
                break;
        }

    }

    static isKeyDown(keycode) {
        return this.downKeys[keycode];
    }

    static clearKeys() {
        for (let keycode in Input.downKeys) {
            if (Input.downKeys[keycode] == true) {
                Application.currentView.logic.currentState.keyUp(+keycode);
                if (Application.currentFocus)
                    Application.currentFocus.keyUp(+keycode);
            }
        }
        Input.downKeys = [];
        Application.redrawAllViews();
    }
}