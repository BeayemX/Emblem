﻿class PrefabEditorRenderer extends Renderer {
    constructor(view) {
        super(view);
    }

    _displayPrefab() {
        if (this.view.prefab) {

            for (let line of this.view.prefab.data.lines)
                this._drawLine(line, line.color);

            /*
            for (let instance of this.view.prefab.data.instances) {
                for (let line of instance.getAsLines())
                    this._drawLine(line, Settings.prefabColor);
            }/*/
            this.drawInstances(this.view.prefab.data.instances, Settings.prefabColor);
            //*/

            for (let line of this.view.selection.data.lines)
                this._drawLine(line, Settings.selectionColor);


            for (let p of this.view.selection.data.points) {
                let color = this.generateGradient(p.position, p.opposite.position, Color.black());
                this.drawLineFromTo(p.position, p.opposite.position, p.line.thickness / this.view.camera.zoom, color, false, false);
            }


            for (let instance of this.view.selection.data.instances)
                for (let line of instance.getAsLines())
                    this._drawLine(line, Settings.prefabSelectionColor);
        } else {
            this._renderNoPrefabText();
        }
    }

    _redraw() {
        if (this.view.isRenderPreviewing) {
        } else {
            this._drawGrid();
            this.drawCursor();
            this.drawPreviewLine();

            if (this.view.logic.currentState instanceof BoxSelectionState) {
                // TODO if next line is removed, axis gets drawn in orange? somewhere this.view.context.stroke missing or sth like that? but i cant find it...
                this.drawCrosshair(true);
                this.drawBorderSelection();
            }
        }

        this._displayPrefab();

        if (this.view.logic.currentState instanceof MoveLinesState)
            this.drawMoveLinesPreview();
        else if (this.view.logic.currentState instanceof RotateState)
            this.drawRotateLinesPreview();
        else if (this.view.logic.currentState instanceof ScaleState)
            this.drawScaleLinesPreview();

        if (this.view.isRenderPreviewing) {

        }
        else {
            this.drawPivot();

            if (Application.logic.currentState instanceof PrefabBrushState)
                this.drawPrefabPreview();
        }

        if (this.view.logic.currentState instanceof MeasureState) {
            if (!this.view.isRenderPreviewing || Preferences.renderGuideLayers)
                if (this.view.logic.currentState.startPosRMB)
                    this.drawMeasure(this.view.logic.currentState.startPosRMB, this.view.logic.currentState.currentPosRMB);
        }

    }


    setQualitySetting(setting) {
        if (setting == "quality")
            this.view.context.lineCap = "round";
        else
            this.view.context.lineCap = "butt";
    }

    // TODO extract into abstract Renderer 
    // TODO make private
    _drawLine(line, color) {
        this.drawLineFromTo(line.start, line.end, line.thickness, color);
    }

    _renderNoPrefabText() {
        let origin = new Vector2(this.view.canvas.width * 0.5, this.view.canvas.height * 0.5);
        origin.y += -80;

        this.view.context.font = "16pt Oswald";
        this.view.context.fillStyle = 'black';
        this.view.context.textAlign = "center";
        this.view.context.textBaseline = "middle";
        this.view.context.fillText("Draw to create a new prefab.", origin.x, origin.y);
    }
}