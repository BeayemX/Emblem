﻿class Transform {
    constructor(position, rotation, scale) {
        this.position = position == undefined ? Vector2.zero : position;
        this.rotation = rotation == undefined ? 0 : rotation;
        this.scale = scale == undefined ? new Vector2(1, 1) : scale;
    }

    equals(other) {
        return this.position.equals(other.position)
            && this.rotation == other.rotation
            && this.scale.equals(other.scale);
    }

    transformSpaceForPosition(pos) {
        pos = pos.addVector(this.position);
        pos = pos.scaleFrom(this.position, this.scale);
        pos = pos.rotateAround(this.position, this.rotation);
        return pos;
    }
}