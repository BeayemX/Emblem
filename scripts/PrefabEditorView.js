﻿class PrefabEditorView extends View {
    constructor(container) {
        super(container);

        this.prefab = null;
        this.data = new Data(); // TODO null if this.prefab is null? but then throws error

        this.renderer = new PrefabEditorRenderer(this);
    }

    setPrefab(prefab) {
        this.prefab = prefab;
        this.data = (prefab) ? prefab.data : new Data(); // TODO null if this.prefab is null? but then throws error
        this.renderer.redraw();
    }

    _preMouseDown(e) {
        if (!this.prefab && e.button == 0)
            this.createNewPrefabFromNull();
    }

    createNewPrefabFromNull() {
        // TODO should be handled by PrefabManager or Prefab-constructor
        this.setPrefab(new Prefab());
        PrefabManager.prefabs.push(this.prefab);

        GUI.buildPrefabUI();
        this.renderer.redraw();
    }

    getSelectionAsLines() {
        let selData = this.selection.data;
        let lines = [];

        for (let line of selData.lines.concat(selData.partialLines)) 
            lines.push(line);

        for (let instance of selData.instances) 
            for (let line of instance.getAsLines())
                lines.push(line);

        return lines;
    }

    toString() {
        return "PrefabEditorView";
    }
}