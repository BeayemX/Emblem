﻿class Saver {
    static init() {
        console.log("Saver created. ");

        this.autosaveFileName = "AutoSave";
        this.clipboardFileName = "Clipboard";
    }

    static autoSave() {
        Application.clearAllSelections();
        localStorage.setItem(this.autosaveFileName, Exporter.generateSVGString());
    }

    static loadAutoSave() // SIFU TODO combine with loading from file
    {
        try {

            let autoSaveFile = localStorage.getItem(this.autosaveFileName);
            if (!autoSaveFile) {
                Saver.newFile();
                return;
            }

            this.createFileFromSVGString(autoSaveFile);
        }
        catch (err) {
            console.log(err)
            this.newFile();
            GUI.notify("Error loading previous session. Created new one.", 'green');
            //this.createFileFromSVGString('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1024" height="1024" viewBox="-512 -512 1024 1024">	<g name="New Layer" visible="true" hideInRenderPreview="false" >		<line x1="256" y1="0" x2="128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="0" y1="0" x2="-128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="0" y1="32" x2="0" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-128" y1="32" x2="-128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-128" y1="0" x2="-128" y2="-32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="128" y1="0" x2="0" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="128" y1="32" x2="128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="128" y1="0" x2="128" y2="-32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="256" y1="32" x2="256" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="256" y1="0" x2="256" y2="-32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="0" y1="0" x2="0" y2="-32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-256" y1="64" x2="-256" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-256" y1="32" x2="-256" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-128" y1="0" x2="-256" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-256" y1="0" x2="-256" y2="-32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-224" y1="32" x2="-256" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-224" y1="32" x2="-256" y2="64" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-256" y1="0" x2="-288" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-256" y1="64" x2="-288" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-96" y1="32" x2="-128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-96" y1="32" x2="-128" y2="64" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-128" y1="0" x2="-160" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="-128" y1="64" x2="-160" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="32" y1="32" x2="0" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="32" y1="32" x2="0" y2="64" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="0" y1="0" x2="-32" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="0" y1="64" x2="-32" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="160" y1="32" x2="128" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="160" y1="32" x2="128" y2="64" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="128" y1="0" x2="96" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="128" y1="64" x2="96" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="288" y1="32" x2="256" y2="0" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="288" y1="32" x2="256" y2="64" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="256" y1="0" x2="224" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> 		<line x1="256" y1="64" x2="224" y2="32" stroke="#000000" stroke-opacity="1" stroke-width="1" stroke-linecap="round"/> </svg>');
        }
    }

    static newFile() {
        ActionHistory.init();
        Application.file = new File(true);
        Application.sceneView.selection = new Selection(Application.sceneView);
        Application.prefabEditorView.prefab = null;
        Application.prefabEditorView.selection = new Selection(Application.prefabEditorView);

        Application.redrawAllViews();

        PrefabManager.init();

        Application.file.updateStats();
        GUI.objectHierarchyChanged();
    }

    static copyLinesToClipboard()
    {
        Application.clipBoard = Application.currentView.selection.data.copy();

        GUI.notify("Lines copied to clipboard!");
        return;
    }

    static pasteLines()
    {
        if (!Application.clipBoard)
            return false;

        // check if endless recursion would occur when pasting into editor
        if (Application.currentView == Application.prefabEditorView) {
            for (let instance of Application.clipBoard.instances) {
                if (Application.prefabEditorView.prefab.includesNested(instance.reference)) {
                    GUI.notify("Creating endless recursions isn't allowed!", 'red');
                    return;
                }
            }
        }
        let newPrefabAdded = false;
        for (let instance of Application.clipBoard.instances) {
            if (!PrefabManager.prefabs.includes(instance.reference)) {
                PrefabManager.addPrefab(instance.reference);
                newPrefabAdded = true;
            }
        }
        if (newPrefabAdded)
            GUI.buildPrefabUI();




        Application.currentView.selection.clear();
        Application.currentView.selection.data = Application.clipBoard.copy();

        // if copy pasted into different views
        Application.currentView.selection.data.selection = Application.currentView.selection;


        Application.selectionChanged(Application.currentView.selection);

        Application.redrawAllViews();
        Application.file.updateStats();

        GUI.notify("Lines pasted from clipboard!");
        return true;
    }

    static handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    static handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.

        for (var i = 0, f; f = files[i]; i++) {

            let reader = new FileReader();
            reader.readAsText(files[i]);
            reader.onload = (evt) => Saver.dndloadedSVG(evt);
        }
    }

    static dndloadedSVG(evt) {
        this.createFileFromSVGString(evt.target.result);
    }

    static createFileFromSVGString(svgString) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(svgString, "image/svg+xml");
        let svg = doc.getElementsByTagName('svg')[0];

        // TODO just copied from Saver.newFile(). should be unified
        ActionHistory.init();

        Application.file = new File(false);
        Application.sceneView.selection = new Selection(Application.sceneView);

        PrefabManager.init();
        // END copy paste from previous TODO
        let viewBox = svg.getAttribute("viewBox");

        let vB = viewBox.split(" ");
        Preferences.artBounds = new ArtBounds(vB[0], vB[1], vB[2], vB[3]);

        // iterate over whole document to create prefabs first
        for (let g of svg.childNodes) {
            if (g.nodeType != 1)
                continue;

            let id = g.getAttribute("prefabID");
            if (!id)
                continue;

            let prefab = new Prefab();
            prefab.name = g.getAttribute("name");

            let lines = [];
            for (let line of g.childNodes) {
                if (line.nodeType != 1)
                    continue;
                if (line.nodeName != "line")
                    continue;

                let newLine = new Line(
                    Number(line.getAttribute("x1")),
                    Number(line.getAttribute("y1")),
                    Number(line.getAttribute("x2")),
                    Number(line.getAttribute("y2")),
                    Color.hexToColor(line.getAttribute('stroke')),
                    Number(line.getAttribute('stroke-width'))
                    );

                    newLine.color.a = Number(line.getAttribute('stroke-opacity'));
                    lines.push(newLine);
            }

            prefab.data.lines = lines;
            PrefabManager.addPrefab(prefab);
        }
        // create nested-prefab-links
        for (let g of svg.childNodes) {
            if (g.nodeType != 1)
                continue;

            let id = g.getAttribute("prefabID");
            if (!id)
                continue;

            let instances = [];
            for (let instance of g.childNodes) {
                if (instance.nodeType != 1)
                    continue;

                let refID = instance.getAttribute("prefabReferenceID")
                if (!refID)
                    continue;

                instances.push(new PrefabInstance(
                    PrefabManager.prefabs[refID],
                    new Vector2(
                        Number(instance.getAttribute("positionX")),
                        Number(instance.getAttribute("positionY"))
                    ),
                    Number(instance.getAttribute("rotation")),
                    new Vector2(
                        Number(instance.getAttribute("scaleX")),
                        Number(instance.getAttribute("scaleY"))
                    )
                ));
            }
            PrefabManager.prefabs[id].data.instances = instances;
        }

        // create layers
        for (let g of svg.childNodes) {

            if (g.nodeType != 1)
                continue;
        // if prefab skip
            let id = g.getAttribute("prefabID");
            if (id)
                continue;

        // else create layer
            let layer = Application.file.createNewLayer(true);
            layer.name = g.getAttribute("name");
            layer.visible = g.getAttribute("visible") == "false" ? false : true;
            layer.hideInRenderPreview = g.getAttribute("hideInRenderPreview") == "true" ? true : false;

            let lines = [];
            for (let line of g.childNodes) {
                if (line.nodeType != 1)
                    continue;

        // if prefab-instance
                id = line.getAttribute("prefabReferenceID");
                if (id) {
                    let pos = new Vector2(
                        Number(line.getAttribute("positionX")),
                        Number(line.getAttribute("positionY")));
                    let rot = Number(line.getAttribute("rotation"));
                    let scal = new Vector2(
                        Number(line.getAttribute("scaleX")),
                        Number(line.getAttribute("scaleY")));
                    let instance = new PrefabInstance(PrefabManager.prefabs[id], pos, rot, scal);
                    layer.data.addInstance(instance);
                    continue;
                }
                // normal line
                let newLine = new Line(
                    Number(line.getAttribute("x1")),
                    Number(line.getAttribute("y1")),
                    Number(line.getAttribute("x2")),
                    Number(line.getAttribute("y2")),
                    Color.hexToColor(line.getAttribute('stroke')),
                    Number(line.getAttribute('stroke-width'))
                    );

                newLine.color.a = Number(line.getAttribute('stroke-opacity'));
                lines.push(newLine);

            }
        // not using addLine() because due to 'cutLines' it could lead to unwanted results
            layer.data.lines = lines;
        }

        GUI.buildPrefabUI();
        GUI.objectHierarchyChanged();
        Application.file.updateStats();
    }
}
