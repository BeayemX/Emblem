﻿class Camera {
    constructor(view) {
        this.view = view;

        this.zoom = 1;
        this.minZoom = 0.1;
        this.maxZoom = 1000;
        this.position = new Vector2(0, 0);

        // TODO better way to initialize?
        //this.multiplyZoomBy(1, true);
    }

    multiplyZoomBy(delta, keepCenter)
    {
        if (keepCenter)
            this.pushCameraCenter();

        this.setZoom(this.zoom * delta, false);

        if (keepCenter)
            this.popCameraCenter();
        Application.redrawAllViews();
    }

    zoomBy(delta, keepCenter) {
        if (keepCenter)
            this.pushCameraCenter();

        this.setZoom(this.zoom + delta, false);

        if (keepCenter)
            this.popCameraCenter();
        Application.redrawAllViews();
    }

    setZoom(val, keepCenter) {
        if (keepCenter)
            this.pushCameraCenter();

        this.zoom = Math.min(this.maxZoom, Math.max(this.minZoom, val));

        if (keepCenter)
            this.popCameraCenter();

        Application.redrawAllViews();
        GUI.writeToStats("Zoom", (this.zoom * 100).toFixed(2) + " %");
    }

    pushCameraCenter() {
        // TODO maybe there is a better option than saving center and comparing difference?
        this.center = new Vector2(this.view.canvas.width * 0.5, this.view.canvas.height * 0.5);
        this.worldCenter = this.screenSpaceToCanvasSpace(this.center);
    }

    popCameraCenter() {
        let newWorldCenter = this.screenSpaceToCanvasSpace(this.center);
        let diff = newWorldCenter.subtractVector(this.worldCenter);
        this.position = this.position.addVector(diff);

        this.center = undefined;
        this.worldCenter = undefined;
    }


    screenSpaceToCanvasSpace(vec2) {
        return vec2
            .divide(this.zoom)
            .subtractVector(this.position);
    }

    canvasSpaceToScreenSpace(vec2) {
        return vec2
            .addVector(this.position)
            .multiply(this.zoom)
        ;
    }

    getVisibleBounds() {
        return new Bounds(
            this.screenSpaceToCanvasSpace(new Vector2(0, 0)),
            this.screenSpaceToCanvasSpace(new Vector2(this.view.canvas.width, this.view.canvas.height))
            );
    }

    resetView() {
        Globals.pivot = Vector2.zero;

        this.setZoom(1, false);
        this.position = (new Vector2(this.view.canvas.width * 0.5, this.view.canvas.height * 0.5));

        this.view.renderer.redraw();
    }

}