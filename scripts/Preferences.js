﻿class Preferences {
    static init() {
        console.log("Preferences created.");
        this.cutLines = true;
        this.showGrid = true;
        this.showLineHandles = true;
        this.snapToGrid = true;
        this.gridType = 1;
        this.developerMode = false;
        this.usePivot = false;

        this.selectVertices = true;
        this.selectEdges = true;
        this.rmbSelectionType = "box"; // "box", "brush"

        this.showRenderPreview = false;

        this.artBounds = new ArtBounds(-512, -512, 1024, 1024);
        this.renderScaleFactor = 1;
        this.renderGuideLayers = false;

        this.qualitySetting = "quality"; // "mono", "speed", "quality"
        this.onlyTransformPrefabInstancePosition = false;

        // TODO gridPrefs? gridData class?
        this.gridSize;
        this.zoom;//?
        this.pan;//?

        this.loadPreferences();
    }

    static savePreferences() {
        localStorage.setItem("cutLines", JSON.stringify(this.cutLines));
        localStorage.setItem("showGrid", JSON.stringify(this.showGrid));
        localStorage.setItem("showLineHandles", JSON.stringify(this.showLineHandles));
        localStorage.setItem("snapToGrid", JSON.stringify(this.snapToGrid));
        localStorage.setItem("gridType", JSON.stringify(this.gridType));
        localStorage.setItem("developerMode", JSON.stringify(this.developerMode));
        localStorage.setItem("usePivot", JSON.stringify(this.usePivot));
        localStorage.setItem("qualitySetting", JSON.stringify(this.qualitySetting));
        localStorage.setItem("selectVertices", JSON.stringify(this.selectVertices));
        localStorage.setItem("selectEdges", JSON.stringify(this.selectEdges));
        localStorage.setItem("renderGuideLayers", JSON.stringify(this.renderGuideLayers));
        localStorage.setItem("onlyTransformPrefabInstancePosition", JSON.stringify(this.onlyTransformPrefabInstancePosition));
        localStorage.setItem("showRenderPreview", JSON.stringify(this.showRenderPreview));
        localStorage.setItem("rmbSelectionType", JSON.stringify(this.rmbSelectionType));
    }

    static loadPreferences() {
        let value;

        value = JSON.parse(localStorage.getItem("cutLines"));
        if (value != null) this.cutLines = value;

        value = JSON.parse(localStorage.getItem("showGrid"));
        if (value != null) this.showGrid = value;

        value = JSON.parse(localStorage.getItem("showLineHandles"));
        if (value != null) this.showLineHandles = value;

        value = JSON.parse(localStorage.getItem("snapToGrid"));
        if (value != null) this.snapToGrid = value;

        value = JSON.parse(localStorage.getItem("gridType"));
        if (value != null) this.gridType = value;

        value = JSON.parse(localStorage.getItem("developerMode"));
        if (value != null) this.developerMode = value;

        value = JSON.parse(localStorage.getItem("usePivot"));
        if (value != null) this.usePivot = value;

        value = JSON.parse(localStorage.getItem("qualitySetting"));
        if (value != null) this.qualitySetting = value;

        value = JSON.parse(localStorage.getItem("selectVertices"));
        if (value != null) this.selectVertices = value;

        value = JSON.parse(localStorage.getItem("selectEdges"));
        if (value != null) this.selectEdges = value;

        value = JSON.parse(localStorage.getItem("renderGuideLayers"));
        if (value != null) this.renderGuideLayers = value;

        value = JSON.parse(localStorage.getItem("onlyTransformPrefabInstancePosition"));
        if (value != null) this.onlyTransformPrefabInstancePosition = value;

        value = JSON.parse(localStorage.getItem("showRenderPreview"));
        if (value != null) this.showRenderPreview = value;

        value = JSON.parse(localStorage.getItem("rmbSelectionType"));
        if (value != null) this.rmbSelectionType = value;
    }

    static clear() {
        localStorage.removeItem("cutLines");
        localStorage.removeItem("showGrid");
        localStorage.removeItem("showLineHandles");
        localStorage.removeItem("snapToGrid");
        localStorage.removeItem("gridType");
        localStorage.removeItem("developerMode");
        localStorage.removeItem("usePivot");
        localStorage.removeItem("qualitySetting");
        localStorage.removeItem("selectVertices");
        localStorage.removeItem("selectEdges");
        localStorage.removeItem("renderGuideLayers");
        localStorage.removeItem("onlyTransformPrefabInstancePosition");
        localStorage.removeItem("showRenderPreview");
        localStorage.removeItem("rmbSelectionType");
        
        Preferences.init();
        window.location.reload();
        Application.redrawAllViews();
    }

    static setQualitySetting(quality) {
        this.qualitySetting = quality;
        Application.setQualitySetting(this.qualitySetting);
        Application.redrawAllViews();
    }

    static setRMBSelectionType(type) {
        Preferences.rmbSelectionType = type;
    }

    static adjustRenderGuideLayers(value) {
        this.renderGuideLayers = value;
    }
}

class ArtBounds{
    constructor(left, top, width, height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    get left() { return this._left; }
    get top() { return this._top; }
    get width() { return this._width; }
    get height() { return this._height; }

    set left(value) {
        this._left = Math.round(value);
        GUI.artBoundsLeft.value = this._left;
        this.adjustDisplayedResolution();
    }

    set top(value) {
        this._top = Math.round(value);
        GUI.artBoundsTop.value = this._top;
        this.adjustDisplayedResolution();
    }

    set width(value) {
        this._width = Math.max(1, Math.round(value));
        GUI.artBoundsWidth.value = this._width;
        this.adjustDisplayedResolution();
    }

    set height(value) {
        this._height = Math.max(1, Math.round(value));
        GUI.artBoundsHeight.value = this._height;
        this.adjustDisplayedResolution();
    }

    adjustDisplayedResolution() {
        GUI.renderResolution.innerHTML = this.width * Preferences.renderScaleFactor + " x " + this.height * Preferences.renderScaleFactor;

        // TODO there has to be a better way...
        // problem is loading order error 
        // Preferences.artBounds are not loaded yet when needed
        try {
            Application.adjustRenderPreview();
        }
        catch(e)
        {
            // console.log(e)
        }
    }

    get origin() {
        return new Vector2(this._left, this._top);
    }

    get size() {
        return new Vector2(this._width, this._height);
    }
}