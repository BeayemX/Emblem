﻿class SceneView extends View {
    constructor(container) {
        super(container);
        this.renderer = new SceneRenderer(this);
    }

    toString() {
        return "SceneView";
    }
}